package com.javadude.movies2

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.javadude.movies2.data.Screen
import com.javadude.movies2.data.ScreenHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
fun SimpleButton(
    text: String,
    onClick: () -> Unit
) {
    Button(onClick = onClick) {
        Text(text = text)
    }
}

/**
 * Sets up a Card() to have rounded corners, max width and some padding
 *
 * @param backgroundColor The background color for the card
 * @param onClick What to do when the body of the card is clicked
 * @param content A @Composable function that defines the content inside the card
 */
@ExperimentalMaterialApi
@Composable
fun RoundedCard(
    backgroundColor: Color = MaterialTheme.colors.surface,
    onClick: () -> Unit,
    content: @Composable () -> Unit
) = Card(
    elevation = 2.dp,
    backgroundColor = backgroundColor,
    shape = RoundedCornerShape(corner = CornerSize(16.dp)),
    onClick = onClick,
    content = content,
    modifier = Modifier
        .padding(8.dp)
        .fillMaxWidth()
)

/**
 * Sets up a text with "label" styling and padding for the app
 *
 * @param text The text to display
 * @param modifier Additional modifiers to pass in
 */
@Composable
fun Label(
    text: String,
    modifier: Modifier = Modifier
) = CommonText(
    text = text,
    style = MaterialTheme.typography.h6,
    startPadding = 8.dp,
    modifier = modifier
)

/**
 * Sets up a text with "display" styling and padding for the app
 *
 * @param text The text to display
 * @param modifier Additional modifiers to pass in
 */
@Composable
fun Display(
    text: String,
    modifier: Modifier = Modifier
) = CommonText(
    text = text,
    style = MaterialTheme.typography.h5,
    startPadding = 24.dp,
    modifier = modifier
)

/**
 * Common text setup used by other more specific text @Composables
 *
 * @param text The text to display
 * @param style The style to use on the text
 * @param startPadding How much space to add before the text
 * @param modifier Additional modifiers to pass in
 */
@Composable
private fun CommonText(
    text: String,
    style: TextStyle,
    startPadding: Dp,
    modifier: Modifier
) {
    Text(
        text = text,
        style = style,
        modifier = modifier
            .padding(
                start = startPadding,
                end = 8.dp,
                top = 8.dp,
                bottom = 8.dp
            )
            .fillMaxWidth()
    )
}

// LIST STUFF

/**
 * Basic definition of selection in a list
 */
@Immutable
sealed interface ListSelection {
    /**
     * Determines if the specified id is in this selection
     *
     * @param id The id to test
     */
    operator fun contains(id: String): Boolean
}

/**
 * Singleton object representing nothing being selected
 */
@Immutable
object NoListSelection: ListSelection {
    override fun contains(id: String) = false
        // Because we hold no selections, always returns false
}

/**
 * Represents a single-selected item in a list
 */
@Immutable
data class SingleListSelection(val id: String): ListSelection {
    override fun contains(id: String) = (this.id == id)
        // Because we hold exactly one selection, just test it
}

/**
 * Represents a multiple-selection in a list
 */
@Immutable
data class MultiListSelection(val ids: List<String>): ListSelection {
    override fun contains(id: String) = (id in ids)
        // Because we hold a list of selections, test in that list
}

/**
 * Holder for common data used when processing our common list. This makes it much easier for
 *   an application to set up its lists, reducing the number of parameters that must be passed
 *   around between @Composable functions.
 */
@Immutable
data class CoreInfo<T>(
    val isWide: Boolean,          // is the screen wide enough to support side-by-side interfaces?
    val items: List<T>,           // the items to display in a list
    val getId: (T) -> String,     // converts an item in the list to its id
    val getText: (T) -> String,   // converts an item in the list to the text used to display it
    val selection: ListSelection, // the current selection state
    val icon: ImageVector,        // the icon to display next to the item
    val iconDescription: String,  // the text to use for the icon in screen readers
    val multiSelectActions: @Composable (MultiListSelection) -> Unit,
        // common actions to define when multiple list items are selected
        // for example, a "delete" action that would appear on all screens
    val singleSelectActions: @Composable () -> Unit,
        // common actions to define when a single list item (or no item) is selected
)

/**
 * Displays a list of items on cards and manages selection
 *
 * @param coreInfo Common list data and functions
 * @param onSelectionChange Reports changes made by the user in the current selection
 * @param modifier Additional modifiers
 */
@ExperimentalMaterialApi
@Composable
fun <T> CommonList(
    coreInfo: CoreInfo<T>,
    onSelectionChange: (ListSelection) -> Unit,
    modifier: Modifier
) {
    // display a scrollable, dynamic list of the passed-in items
    LazyColumn(modifier = modifier) {
        items(
            items = coreInfo.items,
            key = { coreInfo.getId(it) }
                // use the item ids to optimize which parts of the UI tree are recreated
        ) { item ->
            val id = coreInfo.getId(item)

            val selected = id in coreInfo.selection // calls selection.contains(id)

            /**
             * Nested helper function to choose a color based on the selection state
             * @param colorIfSelected the color to use when selected == true
             * @param colorIfNotSelected the color to use when selected == false
             */
            fun selectionColor(
                colorIfSelected: Color,
                colorIfNotSelected: Color
            ) = if (selected) colorIfSelected else colorIfNotSelected

            // display each item on a card with rounded corners
            RoundedCard(
                backgroundColor =
                    selectionColor(MaterialTheme.colors.primary, MaterialTheme.colors.surface),
                onClick = {
                    // tell the caller that the selection has changed
                    // clicking on the card (outside the icon) always results in changing to a
                    //   single selection for the current item
                    onSelectionChange(SingleListSelection(coreInfo.getId(item)))
                },
            ) {
                // inside the card, define a Row with an Icon and text
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Image(
                        imageVector = coreInfo.icon,
                        colorFilter =
                            ColorFilter.tint(
                                selectionColor(MaterialTheme.colors.onPrimary, MaterialTheme.colors.onSurface)
                            ),
                        contentDescription = coreInfo.iconDescription,
                        modifier = Modifier
                            .padding(8.dp)
                            .size(48.dp)

                            // if the icon is clicked, we go to multi-select mode (if not already
                            //   in it) or toggle the selected state of the current item in the
                            //   current multi-selection
                            .clickable {
                                onSelectionChange(
                                    when (coreInfo.selection) {
                                        NoListSelection, is SingleListSelection ->
                                            MultiListSelection(listOf(id))
                                        is MultiListSelection ->
                                            if (coreInfo.selection.ids.contains(id)) {
                                                if (coreInfo.selection.ids.size == 1) {
                                                    NoListSelection
                                                } else {
                                                    MultiListSelection(coreInfo.selection.ids - id)
                                                }
                                            } else {
                                                MultiListSelection(coreInfo.selection.ids + id)
                                            }
                                    }
                                )
                            }
                    )
                    Text(
                        text = coreInfo.getText(item),
                        color = selectionColor(MaterialTheme.colors.onPrimary, MaterialTheme.colors.onSurface)
                    )
                }
            }
        }
    }
}

/**
 * Manages common editable text field function
 *
 * Note that we're working around a bug on pressing "back" that currently (as of Compose 1.0.2)
 *   requires the user to press back an extra time to remove focus on a text field before
 *   they can go back from the current screen
 *
 * @param label The text to display as a label
 * @param value The current value to display on the text field
 * @param onValueChange Called to tell the caller that the value has changed
 * @param onBack Needed for the workaround - called to override Back handling
 */
@ExperimentalComposeUiApi
@Composable
fun CommonTextField(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    onBack: () -> Unit
) {
    TextField(
        value = value,
        label = { Text(text = label)},
        onValueChange = onValueChange,
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            // as of Compose v1.1, the following onKeyEvent() call is no longer needed
            .onKeyEvent {
                if (it.key == Key.Back) {
                    onBack()
                    true
                } else {
                    false
                }
            }
    )
}

/**
 * Helper @Composable to manage displaying a List and detail content next to it when in wide
 *   screen, or only the list or detail in non-wide screen.
 *
 * This can be called from any other screen UI @Composable function to ensure consistent means
 *   of managing lists and side-by-side UIs
 *
 * @param coreInfo The data and functions passed from the activity for use in list setup
 * @param onSelectionChange Called when the user changes the selection in the list by tapping
 *   on icons or the body of the cards
 * @param showListWhenNotWide Ignored when in "wide" mode; otherwise, if true, shows only the list,
 *   if false shows only the detail
 * @param title The title to display on the top bar
 * @param detailPart A @Composable function to create the detail section (only called when it will
 *   be displayed)
 * @param extraMultiSelectActions Additional screen-specific actions to add to the toolbar when
 *   multiple items are selected in the list. The common multi-select actions are defined in the
 *   coreInfo property
 * @param extraSingleSelectActions Additional screen-specific actions to add to the toolbar when
 *   a single (or no) item is selected in the list. The common single-select actions are defined
 *   in the coreInfo property
 */
@ExperimentalMaterialApi
@Composable
fun <T> ListDetail(
    screenHelper: ScreenHelper,
    coreInfo: CoreInfo<T>,
    onSelectionChange: (ListSelection) -> Unit,
    showListWhenNotWide: Boolean,
    title: String,
    detailPart: @Composable (Modifier) -> Unit,
    extraMultiSelectActions: @Composable (MultiListSelection) -> Unit = {},
    extraSingleSelectActions: @Composable () -> Unit = {}
) =
    Screen(
        screenHelper = screenHelper,
        topBar = {
            SelectionTopAppBar(
                coreInfo = coreInfo,
                onSelectionChange = onSelectionChange,
                preferList = showListWhenNotWide,
                title = title,
                extraMultiSelectActions = extraMultiSelectActions,
                extraSingleSelectActions = extraSingleSelectActions
            )
        },
        content = {
            when {
                coreInfo.isWide ->
                    Row {
                        CommonList(
                            coreInfo = coreInfo,
                            onSelectionChange = onSelectionChange,
                            modifier = Modifier
                                .weight(40f)
                                .fillMaxHeight()
                        )
                        detailPart(
                            Modifier
                                .weight(60f)
                                .fillMaxHeight())
                    }
                showListWhenNotWide ->
                    CommonList(
                        coreInfo = coreInfo,
                        onSelectionChange = onSelectionChange,
                        modifier = Modifier.fillMaxSize()
                    )
                else -> detailPart(Modifier.fillMaxSize())
            }
        }
    )

/**
 * Common implementation of a top app bar that supports different modes based on the selection
 *   status.
 *
 * Normal mode (no selections or a single-selection) presents a title on the screen and
 *   single-selection actions.
 *
 * Multi-select mode presents a "back" icon on the toolbar, the count of the selected items,
 *   and multi-select actions.
 */
@Composable
fun SelectionTopAppBar(
    coreInfo: CoreInfo<*>,
    onSelectionChange: (ListSelection) -> Unit,
    preferList: Boolean,
    title: String,
    extraMultiSelectActions: @Composable (MultiListSelection) -> Unit = {},
    extraSingleSelectActions: @Composable () -> Unit = {},
) {
    if (coreInfo.selection is MultiListSelection) {
        TopAppBar(
            navigationIcon = {
                IconButton(icon = Icons.Filled.ArrowBack, iconDescription = stringResource(id = R.string.cancel_multi_select)) {
                    onSelectionChange(NoListSelection)
                }
            },
            title = { Text(text = coreInfo.selection.ids.size.toString()) },
            actions = {
                if (preferList || coreInfo.isWide) {
                    coreInfo.multiSelectActions(coreInfo.selection)
                }
                extraMultiSelectActions(coreInfo.selection)
            }
        )

    } else {
        TopAppBar(
            title = { Text(text = title) },
            actions = {
                if (preferList || coreInfo.isWide) {
                    coreInfo.singleSelectActions()
                }
                extraSingleSelectActions()
            }
        )
    }
}

/**
 * Creates a toolbar button
 *
 * @param icon The icon to display on the button
 * @param iconDescription A description of the icon (for screen readers)
 * @param onClick What to do when the button is clicked
 */
@Composable
fun IconButton(
    icon: ImageVector,
    iconDescription: String,
    onClick: () -> Unit
) = IconButton(onClick = onClick) {
    Icon(
        imageVector = icon,
        contentDescription = iconDescription
    )
}

@Composable
fun CoroutineButton(
    scope: CoroutineScope,
    text: String,
    onClick: suspend () -> Unit
) {
    Button(
        onClick = {
            scope.launch(Dispatchers.Default) {
                onClick()
            }
        },
        modifier = Modifier.padding(8.dp)
    ) {
        Text(text)
    }
}


/**
 * Extension on String that returns the current string, if not blank, or an alternate
 *
 * @receiver a String to test/return
 * @param alternate The alternate string to return if the receiver is blank
 * @return The string if non-blank, otherwise the alternate
 */
@Composable
fun String.notBlankOr(alternate: @Composable () -> String) =
    if (isNotBlank()) this else alternate()
