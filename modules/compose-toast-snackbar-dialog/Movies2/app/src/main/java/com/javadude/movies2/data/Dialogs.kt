package com.javadude.movies2.data

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import com.javadude.movies2.CoroutineButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import androidx.compose.material.AlertDialog as RealAlertDialog

data class AlertDialogData(
    val title: String,
    val text: String,
    val cancelOnClickOutside: Boolean,
    val showCancelButton: Boolean,
    val onCancel: suspend () -> Unit,
    val onOk: suspend () -> Unit,
)

@Composable
fun AlertDialog(
    screenHelper: ScreenHelper,
) {
    val alertDialogData = screenHelper.alertDialogData.collectAsState(initial = null)

    alertDialogData.value?.let { data ->
//        androidx.compose.material.AlertDialog( // could use fully-qualified name, or import alias
        RealAlertDialog( // import alias
            onDismissRequest = {
                if (data.cancelOnClickOutside) {
                    screenHelper.dismissDialog()
                }
            },
            title = { Text(text = data.title) },
            text = { Text(data.text) },
            confirmButton = {
                CoroutineButton(screenHelper.scope, "Ok") {
                    screenHelper.dismissDialog()
                    screenHelper.scope.launch(Dispatchers.Default) {
                        data.onOk()
                    }
                }
            },
            dismissButton =
                if (data.showCancelButton) {
                    {
                        CoroutineButton(screenHelper.scope, "Cancel") {
                            screenHelper.dismissDialog()
                            screenHelper.scope.launch(Dispatchers.Default) {
                                data.onCancel()
                            }
                        }
                    }
                } else {
                    null
                }
        )
    }
}
