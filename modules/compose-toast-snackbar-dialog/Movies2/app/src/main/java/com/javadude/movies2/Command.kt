package com.javadude.movies2

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

interface Command {
    val name: String
    suspend fun execute()
    suspend fun undo()
    suspend fun redo() = execute()
}

class UndoManager {
    private var undoStack = emptyList<Command>()
    private var redoStack = emptyList<Command>()
    private val mutex = Mutex()

    suspend fun execute(command: Command) {
        mutex.withLock {
            command.execute()
            undoStack = undoStack + command
        }
    }
    suspend fun undo() {
        undoStack.lastOrNull()?.let {
            mutex.withLock {
                undoStack = undoStack.dropLast(1)
                it.undo()
                redoStack = redoStack + it
            }
        }
    }
    suspend fun redo() {
        redoStack.lastOrNull()?.let {
            mutex.withLock {
                redoStack = redoStack.dropLast(1)
                it.redo()
                undoStack = undoStack + it
            }
        }
    }
}