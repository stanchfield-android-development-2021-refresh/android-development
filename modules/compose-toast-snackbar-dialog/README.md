# Module 8.1 Jetpack Compose Toast, Snackbar and Dialog

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 2.2: Android Databases Using Room](../modules/room)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
* [Module 5.2: Refactoring the Movies Example](../modules/movies-refactoring)
                
<!-- END COMMON HEADER -->

## Introduction

You'll often want to inform the user of the status of the application, or ask if the action they're taking is really what they want to do.

In this module, we'll look at three tehniques of giving feedback to the user: Toast, Snackbar and Dialog.

## Objectives

* Inform the user of actions that have taken place using Toast and Snackbar
* Allow the user to perform an action using a Snackbar
* Verify user intent using Dialogs

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:59:27

            
#### Jetpack Compose: Toast, Snackbar and Dialog Lecture (Fall 2021) (16:25)
 | [![Jetpack Compose: Toast, Snackbar and Dialog Lecture (Fall 2021)](https://img.youtube.com/vi/n5Mq-PwoXEM/mqdefault.jpg)](https://youtu.be/n5Mq-PwoXEM) |
 | -- |

                

#### Jetpack Compose: Toast, Snackbar and Dialog Example (Fall 2021) (1:43:02)
 | [![Jetpack Compose: Toast, Snackbar and Dialog Example (Fall 2021)](https://img.youtube.com/vi/xQeYd6fcqOk/mqdefault.jpg)](https://youtu.be/xQeYd6fcqOk) |
 | -- |

                

            