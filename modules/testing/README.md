# Module 12.1 Testing

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Automated tests can greatly improve the quality of your projects. You can run quick unit tests to check out non platform code, such as view models and algorithms, and instrumented tests that run on an emulator to test platform interaction such as user interfaces and databases.

In this module we'll add some tests to a simplified movies application.

> Note: Due to out-of-town trips, I decided to edit a recording of a live lecture that I gave Summer 2021. There are a few rough spots near the end of the first part of the example (live coding works well for me most of the time, but when it goes south, it does so spectacularly), but they're worked out in the second part of the example.

## Objectives

* Write unit and instrumented tests to automate checks that will help ensure your application works, and will continue to work

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 2:19:06

            
#### Testing: Lecture (Summer 2021) (20:31)
 | [![Testing: Lecture (Summer 2021)](https://img.youtube.com/vi/6MdN6GSjcSc/mqdefault.jpg)](https://youtu.be/6MdN6GSjcSc) |
 | -- |

                

#### Testing: Example, Part 1 (Summer 2021) (1:02:04)
 | [![Testing: Example, Part 1 (Summer 2021)](https://img.youtube.com/vi/k4aAgzQ4IyE/mqdefault.jpg)](https://youtu.be/k4aAgzQ4IyE) |
 | -- |

                

#### Testing: Example, Part 2 (Summer 2021) (56:31)
 | [![Testing: Example, Part 2 (Summer 2021)](https://img.youtube.com/vi/Pd5W-YQoDrA/mqdefault.jpg)](https://youtu.be/Pd5W-YQoDrA) |
 | -- |

                

            