package stanchfield.scott.composetest

import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow

open class MovieRepository(val db: MovieDatabase? = null) {
    open fun getMovies() = db?.movieDao()?.getMovies() ?: emptyFlow()
    open fun saveMovie(movie: Movie) { /* update db */ }
}

