package stanchfield.scott.composetest

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.UUID

@Entity
data class Movie(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var title: String = ""
)