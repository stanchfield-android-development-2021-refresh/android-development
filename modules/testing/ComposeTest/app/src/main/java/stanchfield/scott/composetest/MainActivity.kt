package stanchfield.scott.composetest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import stanchfield.scott.composetest.ui.theme.ComposeTestTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeTestTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
//                    Movies()
                }
            }
        }
    }
}



@Composable
fun Movies(
    viewModel: MovieViewModel
) {
    val movies by viewModel.movies.collectAsState(initial = emptyList())
    Movies(movies)
}

@Composable
fun Movies(
    movies: List<Movie>
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .testTag("moviesColumn")
    ) {
        Text(text = "Movies")
        movies.forEach {
            Text(text = it.title)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeTestTheme {
        Movies(listOf(Movie(title = "Romancing The Stone"), Movie(title = "Indiana Jones and the Idontwanna")))
    }
}