package stanchfield.scott.composetest

import androidx.lifecycle.ViewModel

class MovieViewModel(private val repository: MovieRepository): ViewModel() {
    val movies = repository.getMovies()
    fun saveMovie(movie: Movie) = repository.saveMovie(movie)
    fun createMovie(title: String) = Movie(title = title)
}