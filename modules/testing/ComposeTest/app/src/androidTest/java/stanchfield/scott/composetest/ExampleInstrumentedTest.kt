package stanchfield.scott.composetest

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.semantics.SemanticsProperties
import androidx.compose.ui.semantics.getOrNull
import androidx.compose.ui.semantics.heading
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.test.assertAny
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.filter
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.isHeading
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.text.style.TextAlign
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.cash.turbine.test
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import stanchfield.scott.composetest.ui.theme.ComposeTestTheme
import kotlin.time.ExperimentalTime

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

// NOTE: Instrumented test function names cannot contain spaces unless you're running the very
//    latest Android version. I've renamed the functions here to omit spaces.
//    You can still use spaces in test function names in _unit_ tests, though
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @get:Rule
    val composeRule = createComposeRule()

    @Test
    fun testSimpleComposeUi1() {
        composeRule.setContent {
            var text by remember { mutableStateOf("Hello") }
            ComposeTestTheme {
                Text(
                    text = text,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag("label")
//                      equivalent to
//                        .semantics {
//                            set(SemanticsProperties.TestTag, "label")
//                        }
                        .clickable {
                            text = "Goodbye"
                        }
                )
            }
        }

        val x = composeRule.onNodeWithTag("display").fetchSemanticsNode().config.getOrNull(SemanticsProperties.Text)

        composeRule.onNodeWithTag("label").assertTextEquals("Hello")
//        composeRule.onNodeWithTag("label").performClick()
//        composeRule.onNodeWithTag("label").assertTextEquals("Goodbye")

        composeRule.onNodeWithTag("label").run {
            assertTextEquals("Hello")
            performClick()
            assertTextEquals("Goodbye")
        }
//        with(composeRule.onNodeWithTag("label")) {
//            assertTextEquals("Hello")
//            performClick()
//            assertTextEquals("Goodbye")
//        }
    }

    @Test
    fun testSimpleComposeUi2() {
        composeRule.setContent {
            ComposeTestTheme {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag("stuff")
                ) {
                    Text(
                        text = "Name",
                        style = MaterialTheme.typography.h6,
                        modifier = Modifier
                            .fillMaxWidth()
                            .semantics {
                                heading()
                            }
                            .testTag("label")
                    )
                    Text(
                        text = "Scott",
                        style = MaterialTheme.typography.h4,
                        modifier = Modifier
                            .fillMaxWidth()
                            .testTag("user name")
                    )
                }
            }
        }

        composeRule.onNodeWithTag("label").assertTextEquals("Name")
        composeRule.onNodeWithTag("user name").assertTextEquals("Scott")
        composeRule.onNodeWithTag("stuff").onChildAt(0).assertTextEquals("Name")
        composeRule.onNodeWithTag("stuff").onChildAt(1).assertTextEquals("Scott")
        composeRule
            .onNodeWithTag("stuff")
            .onChildren()
            .filter(isHeading())
            .assertAny(hasText("Name"))
    }

    @Test
    fun checkBaseMovies() {
        val movies = listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart"))
        composeRule.setContent {
            ComposeTestTheme {
                Movies(movies)
            }
        }

        composeRule
            .onNodeWithTag("moviesColumn")
            .onChildAt(0)
            .assertTextEquals("Movies")

        movies.forEachIndexed { n, movie ->
            composeRule
                .onNodeWithTag("moviesColumn")
                .onChildAt(n+1)
                .assertTextEquals(movie.title)
        }

        with(composeRule.onNodeWithTag("moviesColumn")) {
            onChildAt(0).assertTextEquals("Movies")
            movies.forEachIndexed { n, movie ->
                onChildAt(n+1).assertTextEquals(movie.title)
            }
        }
        composeRule.onNodeWithTag("moviesColumn").run() {
            onChildAt(0).assertTextEquals("Movies")
            movies.forEachIndexed { n, movie ->
                onChildAt(n+1).assertTextEquals(movie.title)
            }
        }
    }

    @Test
    fun checkBaseMoviesUsingViewModel() {
        val movies = listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart"))
        val context = ApplicationProvider.getApplicationContext<Context>()
        val database = Room
            .inMemoryDatabaseBuilder(context, MovieDatabase::class.java)
            .addCallback(object: RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m1', 'Die Hard')")
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m2', 'Get Smart')")
                }
            })
            .build()
        val repository = MovieRepository(database)
        val viewModel = MovieViewModel(repository)

        composeRule.setContent {
            ComposeTestTheme {
                Movies(viewModel = viewModel)
            }
        }

        composeRule
            .onNodeWithTag("moviesColumn")
            .onChildAt(0)
            .assertTextEquals("Movies")

        movies.forEachIndexed { n, movie ->
            composeRule
                .onNodeWithTag("moviesColumn")
                .onChildAt(n+1)
                .assertTextEquals(movie.title)
        }

        with(composeRule.onNodeWithTag("moviesColumn")) {
            onChildAt(0).assertTextEquals("Movies")
            movies.forEachIndexed { n, movie ->
                onChildAt(n+1).assertTextEquals(movie.title)
            }
        }
        composeRule.onNodeWithTag("moviesColumn").run() {
            onChildAt(0).assertTextEquals("Movies")
            movies.forEachIndexed { n, movie ->
                onChildAt(n+1).assertTextEquals(movie.title)
            }
        }
    }

    @ExperimentalTime
    @Test
    fun testDatabase() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val database = Room
            .inMemoryDatabaseBuilder(context, MovieDatabase::class.java)
            .addCallback(object: RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m1', 'Die Hard')")
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m2', 'Get Smart')")
                }
            })
            .build()

        runBlocking {
            database.movieDao().getMovies().test {
                assertEquals(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun testRepository() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val database = Room
            .inMemoryDatabaseBuilder(context, MovieDatabase::class.java)
            .addCallback(object: RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m1', 'Die Hard')")
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m2', 'Get Smart')")
                }
            })
            .build()

        val repository = MovieRepository(database)

        runBlocking {
            repository.getMovies().test {
                assertEquals(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }
}