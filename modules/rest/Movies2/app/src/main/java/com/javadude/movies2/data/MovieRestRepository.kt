package com.javadude.movies2.data

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response


class MovieRestRepository(private val coroutineScope: CoroutineScope): MovieRepository {
    inner class ListFlowManager<T>(
        private val fetcher: suspend () -> Response<List<T>>
    ) {
        private val _flow = MutableStateFlow(emptyList<T>())
        init {
            fetch()
        }
        val flow: Flow<List<T>>
            get() = _flow

        fun fetch() =
            coroutineScope.launch(Dispatchers.IO) {
                _flow.value = fetcher().takeIf { it.isSuccessful }?.body() ?: emptyList()
            }
    }

    private val movieApiService = MovieApiService.create()

    private val moviesFlowManager = ListFlowManager { movieApiService.getMovies() }
    private val actorsFlowManager = ListFlowManager { movieApiService.getActors() }
    private var castFlowManager: ListFlowManager<ExpandedRole>? = null
    private var filmographyFlowManager: ListFlowManager<ExpandedAppearance>? = null

    override val moviesFlow = moviesFlowManager.flow
    override val actorsFlow = actorsFlowManager.flow

    override fun getCastFlow(movieId: String) =
        ListFlowManager { movieApiService.getCast(movieId) }.let {
            castFlowManager = it
            it.flow
        }

    override fun getFilmographyFlow(actorId: String) =
        ListFlowManager { movieApiService.getFilmography(actorId) }.let {
            filmographyFlowManager = it
            it.flow
        }

    override suspend fun getActorsById(ids: List<String>) = withContext(Dispatchers.IO) {
        ids.mapNotNull { movieApiService.getActor(it).takeIf { it.isSuccessful }?.body() }
    }
    override suspend fun getRolesByActorId(ids: List<String>) = withContext(Dispatchers.IO) {
        ids.mapNotNull { movieApiService.getRolesByActorId(it).takeIf { it.isSuccessful }?.body() }.flatten()
    }

    override suspend fun getMovie(id: String) = withContext(Dispatchers.IO) {
        movieApiService.getMovie(id).body()!!
    }

    override suspend fun getActor(id: String) = withContext(Dispatchers.IO) {
        movieApiService.getActor(id).body()!!
    }

    override suspend fun insert(vararg movies: Movie) {
        withContext(Dispatchers.IO) {
            movies.forEach {
                movieApiService.createMovie(it)
            }
            moviesFlowManager.fetch()
        }
    }

    override suspend fun insert(vararg actors: Actor) {
        withContext(Dispatchers.IO) {
            actors.forEach {
                movieApiService.createActor(it)
            }
            actorsFlowManager.fetch()
        }
    }

    override suspend fun insert(vararg roles: Role) {
        withContext(Dispatchers.IO) {
            roles.forEach {
                movieApiService.createRole(it)
            }
            filmographyFlowManager?.fetch()
            castFlowManager?.fetch()
        }
    }

    override suspend fun delete(vararg movies: Movie) {
        withContext(Dispatchers.IO) {
            movies.forEach {
                movieApiService.deleteMovie(it.id)
            }
            moviesFlowManager.fetch()
        }
    }

    override suspend fun delete(vararg actors: Actor) {
        withContext(Dispatchers.IO) {
            actors.forEach {
                movieApiService.deleteActor(it.id)
            }
            actorsFlowManager.fetch()
        }
    }

    override suspend fun update(vararg movies: Movie) {
        withContext(Dispatchers.IO) {
            movies.forEach {
                movieApiService.updateMovie(it.id, it)
            }
            moviesFlowManager.fetch()
        }
    }
    override suspend fun update(vararg actors: Actor) {
        withContext(Dispatchers.IO) {
            actors.forEach {
                movieApiService.updateActor(it.id, it)
            }
            actorsFlowManager.fetch()
        }
    }

    override suspend fun resetDatabase() {
        withContext(Dispatchers.IO) {
            movieApiService.resetDatabase()
        }
        moviesFlowManager.fetch()
        actorsFlowManager.fetch()
    }

    override suspend fun createMovie() = withContext(Dispatchers.IO) {
        Movie(title = "", description = "").apply { insert(this) }
    }

    override suspend fun createActor() = withContext(Dispatchers.IO) {
        Actor(name = "").apply { insert(this) }
    }

    override suspend fun deleteMoviesById(ids: List<String>) {
        withContext(Dispatchers.IO) {
            ids.forEach {
                movieApiService.deleteMovie(it)
            }
            moviesFlowManager.fetch()
        }
    }

    override suspend fun deleteActorsById(ids: List<String>) {
        withContext(Dispatchers.IO) {
            ids.forEach {
                movieApiService.deleteActor(it)
            }
            actorsFlowManager.fetch()
        }
    }

    override suspend fun deleteRole(actorId: String, movieId: String) {
        withContext(Dispatchers.IO) {
            movieApiService.deleteRole(actorId, movieId)
        }
        filmographyFlowManager?.fetch()
        castFlowManager?.fetch()
    }
}