package com.javadude.movies2

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.Movie

// -------------------------------------------------------------------------------------------------
// UI PART DEFINITIONS
//   At this level, these @Composable functions are responsible for displaying part of the user
//     interface, which could be next to another part on the screen
// -------------------------------------------------------------------------------------------------

/**
 * Displays details for a movie (its title, description and cast list)
 *
 * @param loadState The status of the movie being loaded
 * @param onSelectActor Notify the caller when an actor in the cast list has been selected
 * @param modifier Additional modifiers
 */
@ExperimentalMaterialApi
@Composable
fun MovieDisplayPart(
    loadState: MovieLoadState,
    onSelectActor: (String) -> Unit,
    onDeleteRole: (actorId: String, movieId: String) -> Unit,
    modifier: Modifier
) {
    when (loadState.loadStatus) {
        LoadStatus.NotSelected -> Text(stringResource(R.string.please_select_a_movie), modifier = modifier)
        LoadStatus.Loading -> Text(stringResource(R.string.loading), modifier = modifier)
        LoadStatus.Loaded -> loadState.movie?.let { movie ->
            Column(
                modifier = modifier.verticalScroll(rememberScrollState())
            ) {
                Label(text = stringResource(R.string.title))
                Display(text = movie.title)
                Label(text = stringResource(R.string.description))
                Display(text = movie.description)
                loadState.cast?.let {
                    Label(text = stringResource(R.string.cast))
                    it.forEach { role ->
                        RoundedCard(
                            onClick = { onSelectActor(role.actorId) }
                        ) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Icon(
                                    imageVector = Icons.Filled.Person,
                                    contentDescription = stringResource(id = R.string.actor),
                                    modifier = Modifier
                                        .padding(8.dp)
                                        .size(48.dp)
                                )
                                Text(
                                    text = "${role.character} - ${role.actor.name}",
                                    modifier = Modifier.weight(1f)
                                )
                                IconButton(
                                    icon = Icons.Filled.Delete,
                                    iconDescription = stringResource(id = R.string.delete_role),
                                    onClick = { onDeleteRole(role.actorId, movie.id) }
                                )
                            }
                        }
                    }
                }
            }
        } ?: throw IllegalStateException("If load state is Loaded, movie must be non-null")
    }
}

/**
 * Displays details for an actor (its name and filmography)
 *
 * @param loadState The status of the actor being loaded
 * @param onSelectMovie Notify the caller when a movie in the filmography has been selected
 * @param modifier Additional modifiers
 */
@ExperimentalMaterialApi
@Composable
fun ActorDisplayPart(
    loadState: ActorLoadState,
    onSelectMovie: (String) -> Unit,
    onDeleteRole: (actorId: String, movieId: String) -> Unit,
    modifier: Modifier
) {
    when (loadState.loadStatus) {
        LoadStatus.NotSelected -> Text(stringResource(R.string.please_select_an_actor), modifier = modifier)
        LoadStatus.Loading -> Text(stringResource(R.string.loading), modifier = modifier)
        LoadStatus.Loaded -> loadState.actor?.let { actor ->
            Column(
                modifier = modifier.verticalScroll(rememberScrollState())
            ) {
                Label(text = stringResource(R.string.name))
                Display(text = actor.name)
                loadState
                    .filmography
                    ?.sortedBy { it.movie.title }
                    ?.let {
                        Label(text = stringResource(R.string.filmography))
                        it.forEach { appearance ->
                            RoundedCard(
                                onClick = { onSelectMovie(appearance.movieId) },
                            ) {
                                Row(verticalAlignment = Alignment.CenterVertically) {
                                    Icon(
                                        imageVector = Icons.Filled.Person,
                                        contentDescription = stringResource(id = R.string.actor),
                                        modifier = Modifier
                                            .padding(8.dp)
                                            .size(48.dp)
                                    )
                                    Text(
                                        text = "${appearance.movie.title} - ${appearance.character}",
                                        modifier = Modifier.weight(1f)
                                    )
                                    IconButton(
                                        icon = Icons.Filled.Delete,
                                        iconDescription = stringResource(id = R.string.delete_role),
                                        onClick = { onDeleteRole(actor.id, appearance.movieId) }
                                    )
                                }
                            }
                        }
                    }
            }
        } ?: throw IllegalStateException("If load state is Loaded, actor must be non-null")
    }
}

/**
 * Edits movie details
 *
 * @param loadState The status of the actor being loaded
 * @param onMovieChange Notify the caller when the movie data has changed
 * @param onTitleChange Notify the caller when the title property has changed
 * @param onBack Notify the caller to work around the back problem in text fields (back handling overridden)
 * @param modifier Additional modifiers
 */
@ExperimentalComposeUiApi
@Composable
fun MovieEditPart(
    loadState: MovieLoadState,
    onMovieChange: (Movie) -> Unit,
    onTitleChange: (String) -> Unit,
    onBack: () -> Unit,
    modifier: Modifier
) {
    val titleLabel = stringResource(id = R.string.title)
    val descriptionLabel = stringResource(id = R.string.description)

    var title by remember(loadState) { mutableStateOf(loadState.movie?.title ?: "") }
    var description by remember(loadState) { mutableStateOf(loadState.movie?.description ?: "") }

    when (loadState.loadStatus) {
        LoadStatus.NotSelected -> Text(stringResource(R.string.please_select_a_movie), modifier = modifier)
        LoadStatus.Loading -> Text(stringResource(R.string.loading), modifier = modifier)
        LoadStatus.Loaded -> loadState.movie?.let { nonNullMovie ->
            Column(
                modifier = modifier.verticalScroll(rememberScrollState())
            ) {
                CommonTextField(
                    label = titleLabel,
                    value = title,
                    onValueChange = {
                        title = it
                        onTitleChange(it)
                        onMovieChange(Movie(id = nonNullMovie.id, title = it, description = description))
                    },
                    onBack = onBack
                )
                CommonTextField(
                    label = descriptionLabel,
                    value = description,
                    onValueChange = {
                        description = it
                        onMovieChange(Movie(id = nonNullMovie.id, title = title, description = it))
                    },
                    onBack = onBack
                )
            }
        } ?: throw IllegalStateException("If load state is Loaded, movie must be non-null")
    }
}

/**
 * Edits actor details
 *
 * @param loadState The status of the actor being loaded
 * @param onActorChange Notify the caller when the actor data has changed
 * @param onNameChange Notify the caller when the name property has changed
 * @param onBack Notify the caller to work around the back problem in text fields (back handling overridden)
 * @param modifier Additional modifiers
 */
@ExperimentalComposeUiApi
@Composable
fun ActorEditPart(
    loadState: ActorLoadState,
    onActorChange: (Actor) -> Unit,
    onNameChange: (String) -> Unit,
    onBack: () -> Unit,
    modifier: Modifier
) {
    val nameLabel = stringResource(id = R.string.name)
    var name by remember(loadState) { mutableStateOf(loadState.actor?.name ?: "") }

    when (loadState.loadStatus) {
        LoadStatus.NotSelected -> Text(stringResource(R.string.please_select_an_actor), modifier = modifier)
        LoadStatus.Loading -> Text(stringResource(R.string.loading), modifier = modifier)
        LoadStatus.Loaded -> loadState.actor?.let { nonNullActor ->
            Column(
                modifier = modifier.verticalScroll(rememberScrollState())
            ) {
                CommonTextField(
                    label = nameLabel,
                    value = name,
                    onValueChange = {
                        name = it
                        onNameChange(it)
                        onActorChange(Actor(id = nonNullActor.id, name = it))
                    },
                    onBack = onBack
                )
            }
        } ?: throw IllegalStateException("If load state is Loaded, actor must be non-null")
    }
}






// ---------------------------------------------------------------------------------------------
// The following are not actually used in this app, but are examples of some ways to reduce the
//    application code a bit more, through use of nested functions (to reduce having to write
//    passing common parameters every time) to use of reflection to automate some of the manual
//    code that would be written
// Note that the movie edit screen only has two fields, so doesn't get much benefit out of these
//    changes, but a screen with many more fields could benefit from less code to write and
//    better consistency in how all fields are handled
// ---------------------------------------------------------------------------------------------

// =============================================================================================
// ================ SIMPLE NESTED FUNCTION APPROACH ============================================
// =============================================================================================
// Here's a version using a nested function to:
//
//   * pass in values that are always the same (onBack, onMovieChange call)
//   * keep track of a local remembered string for the text field text
//   * update the local remembered movie instance (so the copy will always have the latest data)
//     otherwise, we keep copying the original movie passed in, only changing the current
//     field being edited
//   * reduce the code required to call onMovieChange using extension functions
@ExperimentalComposeUiApi
@Composable
fun MovieEditPart2(
    loadState: MovieLoadState,
    onMovieChange: (Movie) -> Unit,
    onTitleChange: (String) -> Unit,
    onBack: () -> Unit,
    modifier: Modifier
) {
    val titleLabel = stringResource(id = R.string.title)
    val descriptionLabel = stringResource(id = R.string.description)

    when (loadState.loadStatus) {
        LoadStatus.NotSelected -> Text(stringResource(R.string.please_select_a_movie), modifier = modifier)
        LoadStatus.Loading -> Text(stringResource(R.string.loading), modifier = modifier)
        LoadStatus.Loaded -> loadState.movie?.let { nonNullMovie ->
            // track the current movie, staring from what was loaded and and edits made
            var movie by remember(nonNullMovie) { mutableStateOf(nonNullMovie) }
            Column(
                modifier = modifier.verticalScroll(rememberScrollState())
            ) {

                @Composable
                fun TextField(
                    label: String,
                    getValue: Movie.() -> String,
                    copyWithValue: Movie.(String) -> Movie,
                    onValueChange: (String) -> Unit = {}
                ) {
                    var value by remember(nonNullMovie) { mutableStateOf(nonNullMovie.getValue()) }
                    CommonTextField(
                        label = label,
                        value = value,
                        onValueChange = {
                            value = it
                            onValueChange(it)
                            movie = movie.copyWithValue(it)
                            onMovieChange(movie)
                        },
                        onBack = onBack
                    )
                }

                TextField(
                    label = titleLabel,
                    getValue = { title },
                    copyWithValue = { copy(title=it) },
                    onValueChange = onTitleChange
                )
                TextField(
                    label = descriptionLabel,
                    getValue = { description },
                    copyWithValue = { copy(description=it) }
                )
            }
        } ?: throw IllegalStateException("If load state is Loaded, movie must be non-null")
    }
}
