# Module 10.1 RESTful Web Services

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
* [Module 8.1: Jetpack Compose Toast, Snackbar and Dialog](../modules/compose-toast-snackbar-dialog)
                
<!-- END COMMON HEADER -->

## Introduction

Android applications often communicate with servers to manage common data. In this module, we'll look at RESTful Web Services and see how your application can communicate with them to manage remote data.

## Objectives

* Understand the basic concepts of RESTful Web Services
* Create applications that can access remote data using REST

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 25:48

            
#### REST: Lecture (Fall 2021) (07:04)
 | [![REST: Lecture (Fall 2021)](https://img.youtube.com/vi/Fd0L5LbEwPU/mqdefault.jpg)](https://youtu.be/Fd0L5LbEwPU) |
 | -- |

                

#### REST: Example (Fall 2021) (18:44)
 | [![REST: Example (Fall 2021)](https://img.youtube.com/vi/Ya7sCOzl0ss/mqdefault.jpg)](https://youtu.be/Ya7sCOzl0ss) |
 | -- |

                

            