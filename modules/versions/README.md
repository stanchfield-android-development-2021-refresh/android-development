# Module 14.2 Android Versions

            
<!-- END COMMON HEADER -->

## Introduction

Android is an incredibly fast-moving platform, and designing applications that can run successfully on multiple versions of Android can be tricky.

In this module, we'll talk about some concerns and techniques for creating backward-compatible applications.

## Objectives

* Know where to look to determine which versions of Android for which you want to retain compatibility
* See how version checks and compatibility libraries can assist with backward compatibility

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 09:06

            
#### Android Versions: Lecture (Summer 2021) (09:06)
 | [![Android Versions: Lecture (Summer 2021)](https://img.youtube.com/vi/Y-f6X5iFxok/mqdefault.jpg)](https://youtu.be/Y-f6X5iFxok) |
 | -- |

                

            