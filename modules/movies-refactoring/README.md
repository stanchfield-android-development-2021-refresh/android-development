# Module 5.2 Refactoring the Movies Example

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 2.2: Android Databases Using Room](../modules/room)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
* [Module 4.1: Jetpack Compose Lists](../modules/compose-lists)
* [Module 5.1: Jetpack Compose Layouts](../modules/compose-layouts)
                
<!-- END COMMON HEADER -->

## Introduction

After looking at the movies application, we can refactor it to simplify and make it easier to add the Actor functionality.

We won't walk through actually performing the refactoring (it took a bit...), but the video for this module explains what changes were made and why.

## Objectives

* Consider some ways to refactor Jetpack Compose code

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 18:29

            
#### Example: Refactoring the Movies Application (Fall 2021) (18:29)
 | [![Example: Refactoring the Movies Application (Fall 2021)](https://img.youtube.com/vi/wFhPd-n4i24/mqdefault.jpg)](https://youtu.be/wFhPd-n4i24) |
 | -- |

                

            