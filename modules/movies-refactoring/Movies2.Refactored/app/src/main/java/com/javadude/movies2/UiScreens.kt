package com.javadude.movies2

import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.*
import androidx.compose.ui.res.stringResource
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.Movie

// -------------------------------------------------------------------------------------------------
// MAIN SCREEN UI DEFINITIONS
//   At this level, these @Composable functions are responsible to deciding whether to display
//     the movie/actor list, the movie/actor detail (for display/edit), or both
//   The ***Part functions are the actual UI implementation for that section of the UI
// -------------------------------------------------------------------------------------------------

/**
 * Displays a list of movies, possibly next to a "please select a movie" prompt if room
 *
 * @param coreInfo Common list data/functions
 * @param onSelectionChange Notify the caller that the selection has changed
 */
@Composable
fun MovieList(
    coreInfo: CoreInfo<Movie>,
    onSelectionChange: (ListSelection) -> Unit
) {
    ListDetail(
        coreInfo = coreInfo,
        showListWhenNotWide = true,
        title = stringResource(R.string.movies),
        onSelectionChange = onSelectionChange,
        detailPart = {
            Text(text = stringResource(id = R.string.please_select_a_movie), modifier = it)
        }
    )
}

/**
 * Displays a list of actors, possibly next to a "please select an actor" prompt if room
 *
 * @param coreInfo Common list data/functions
 * @param onSelectionChange Notify the caller that the selection has changed
 */
@Composable
fun ActorList(
    coreInfo: CoreInfo<Actor>,
    onSelectionChange: (ListSelection) -> Unit
) {
    ListDetail(
        coreInfo = coreInfo,
        showListWhenNotWide = true,
        title = stringResource(R.string.actors),
        onSelectionChange = onSelectionChange,
        detailPart = {
            Text(text = stringResource(id = R.string.please_select_an_actor), modifier = it)
        }
    )
}

/**
 * Displays data for a movie, possibly next to the movie list if room
 *
 * @param coreInfo Common list data/functions
 * @param onSelectionChange Notify the caller that the selection has changed
 * @param onSelectActor Notify that an actor inside the cast list was selected
 * @param onEditMovie Notify that the "edit" button on the toolbar was clicked
 * @param fetchers Helper object that holds common data fetching functions
 */
@Composable
fun MovieDisplay(
    coreInfo: CoreInfo<Movie>,
    onSelectionChange: (ListSelection) -> Unit,
    onSelectActor: (String) -> Unit,
    onEditMovie: (String) -> Unit,
    onDeleteRole: (actorId: String, movieId: String) -> Unit,
    fetchers: Fetchers,
) {
    val movieLoadState by rememberMovieLoadState(selection = coreInfo.selection, fetchers = fetchers)

    val title by remember(movieLoadState) { mutableStateOf(movieLoadState.movie?.title ?: "(Movie Loading)") }

    ListDetail(
        coreInfo = coreInfo,
        onSelectionChange = onSelectionChange,
        showListWhenNotWide = false,
        title = title,
        detailPart = {
            MovieDisplayPart(
                loadState = movieLoadState,
                onSelectActor = onSelectActor,
                onDeleteRole = onDeleteRole,
                modifier = it
            )
        },
        extraSingleSelectActions = {
            if (coreInfo.selection is SingleListSelection) {
                IconButton(icon = Icons.Filled.Edit, iconDescription = stringResource(id = R.string.edit_movie)) {
                    onEditMovie(coreInfo.selection.id)
                }
            }
        }
    )
}

/**
 * Displays data for an actor, possibly next to the actor list if room
 *
 * @param coreInfo Common list data/functions
 * @param onSelectionChange Notify the caller that the selection has changed
 * @param onSelectMovie Notify that a movie inside the filmography list was selected
 * @param onEditActor Notify that the "edit" button on the toolbar was clicked
 * @param fetchers Helper object that holds common data fetching functions
 */
@Composable
fun ActorDisplay(
    coreInfo: CoreInfo<Actor>,
    onSelectionChange: (ListSelection) -> Unit,
    onSelectMovie: (String) -> Unit,
    onEditActor: (String) -> Unit,
    onDeleteRole: (actorId: String, movieId: String) -> Unit,
    fetchers: Fetchers,
) {
    val actorLoadState by rememberActorLoadState(selection = coreInfo.selection, fetchers = fetchers)
    val name by remember(actorLoadState) { mutableStateOf(actorLoadState.actor?.name) }
    val nameToDisplay = name ?: stringResource(id = R.string.actor_loading)

    ListDetail(
        coreInfo = coreInfo,
        onSelectionChange = onSelectionChange,
        showListWhenNotWide = false,
        title = nameToDisplay,
        detailPart = {
            ActorDisplayPart(
                loadState = actorLoadState,
                onSelectMovie = onSelectMovie,
                onDeleteRole = onDeleteRole,
                modifier = it
            )
        },
        extraSingleSelectActions = {
            if (coreInfo.selection is SingleListSelection) {
                IconButton(icon = Icons.Filled.Edit, iconDescription = stringResource(id = R.string.edit_actor)) {
                    onEditActor(coreInfo.selection.id)
                }
            }
        }
    )
}

/**
 * Edits data for a movie, possibly next to the movie list if room
 *
 * @param coreInfo Common list data/functions
 * @param onSelectionChange Notify the caller that the selection has changed
 * @param onMovieChange Notify that the data for the movie has changed
 * @param fetchers Helper object that holds common data fetching functions
 * @param onBack Used by the bug workaround (see [CommonTextField]) to override back behavior
 */
@Composable
fun MovieEdit(
    coreInfo: CoreInfo<Movie>,
    onSelectionChange: (ListSelection) -> Unit,
    onMovieChange: (Movie) -> Unit,
    fetchers: Fetchers,
    onBack: () -> Unit,
) {
    val movieLoadState by rememberMovieLoadState(selection = coreInfo.selection, fetchers = fetchers)
    var title by remember(movieLoadState) { mutableStateOf(movieLoadState.movie?.title ?: "(Movie Loading)") }
    val toolbarTitle = title.notBlankOr { stringResource(R.string.no_title) }

    ListDetail(
        coreInfo = coreInfo,
        onSelectionChange = onSelectionChange,
        showListWhenNotWide = false,
        title = toolbarTitle,
        detailPart = {
            MovieEditPart2(
                loadState = movieLoadState,
                onMovieChange = onMovieChange,
                onTitleChange = { title = it},
                onBack = onBack,
                modifier = it,
            )
        }
    )
}

/**
 * Edits data for an actor, possibly next to the actor list if room
 *
 * @param coreInfo Common list data/functions
 * @param onSelectionChange Notify the caller that the selection has changed
 * @param onActorChange Notify that the data for an actor has changed
 * @param fetchers Helper object that holds common data fetching functions
 * @param onBack Used by the bug workaround (see [CommonTextField]) to override back behavior
 */
@Composable
fun ActorEdit(
    coreInfo: CoreInfo<Actor>,
    onSelectionChange: (ListSelection) -> Unit,
    onActorChange: (Actor) -> Unit,
    fetchers: Fetchers,
    onBack: () -> Unit,
) {
    val actorLoadState by rememberActorLoadState(selection = coreInfo.selection, fetchers = fetchers)
    var name by remember(actorLoadState) { mutableStateOf(actorLoadState.actor?.name) }
    val toolbarTitle = name?.let { it.notBlankOr { stringResource(R.string.no_title) } }
        ?: stringResource(id = R.string.actor_loading)

    ListDetail(
        coreInfo = coreInfo,
        onSelectionChange = onSelectionChange,
        showListWhenNotWide = false,
        title = toolbarTitle,
        detailPart = {
            ActorEditPart(
                loadState = actorLoadState,
                onActorChange = onActorChange,
                onNameChange = { name = it },
                onBack = onBack,
                modifier = it,
            )
        }
    )
}
