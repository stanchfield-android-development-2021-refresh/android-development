package com.javadude.movies2.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.RewriteQueriesToDropUnusedColumns
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
abstract class MovieDAO {
    // ASYNCHRONOUS
    @Query("SELECT * FROM Movie")
    abstract fun getMoviesFlow(): Flow<List<Movie>>

    // ASYNCHRONOUS
    @Query("SELECT * FROM Actor")
    abstract fun getActorsFlow(): Flow<List<Actor>>

    // SYNCHRONOUS
    @Query("SELECT * FROM Movie WHERE id = :id")
    abstract fun getMovie(id: String): Movie

    // SYNCHRONOUS
    @Query("SELECT * FROM Actor WHERE id = :id")
    abstract fun getActor(id: String): Actor

    @Transaction
    @RewriteQueriesToDropUnusedColumns
    @Query("SELECT * FROM Role WHERE movieId = :movieId ORDER BY orderInCredits")
    abstract fun getCastFlow(movieId: String): Flow<List<ExpandedRole>>

    @Transaction
    @RewriteQueriesToDropUnusedColumns
    @Query("SELECT * FROM Role WHERE actorId = :actorId")
    abstract fun getFilmographyFlow(actorId: String): Flow<List<ExpandedAppearance>>

    @Insert
    abstract fun insert(vararg movies: Movie)
    @Insert
    abstract fun insert(vararg actors: Actor)
    @Insert
    abstract fun insert(vararg roles: Role)
    @Update
    abstract fun update(vararg movies: Movie)
    @Update
    abstract fun update(vararg actors: Actor)
    @Update
    abstract fun update(vararg roles: Role)
    @Delete
    abstract fun delete(vararg movies: Movie)
    @Delete
    abstract fun delete(vararg actors: Actor)
    @Delete
    abstract fun delete(vararg roles: Role)

    @Query("DELETE FROM Movie WHERE id in (:ids)")
    abstract fun deleteMoviesById(ids: List<String>)

    @Query("DELETE FROM Actor WHERE id in (:ids)")
    abstract fun deleteActorsById(ids: List<String>)

    @Query("DELETE FROM Role WHERE actorId = :actorId AND movieId = :movieId")
    abstract fun deleteRole(actorId: String, movieId: String)

    @Query("DELETE FROM Movie")
    abstract fun clearMovies()

    @Query("DELETE FROM Actor")
    abstract fun clearActors()

    @Query("DELETE FROM Role")
    abstract fun clearRoles()

    @Transaction
    open fun resetDatabase() {
        clearMovies()
        clearActors()
        clearRoles()
        insert(
            Movie("m1", "The Transporter", "Jason Statham kicks a guy in the face"),
            Movie("m2", "Transporter 2", "Jason Statham kicks a bunch of guys in the face"),
            Movie("m3", "Hobbs and Shaw", "Cars, Explosions and Stuff"),
            Movie("m4", "Jumanji", "The Rock smolders"),
        )

        insert(
            Actor("a1", "Jason Statham"),
            Actor("a2", "The Rock"),
            Actor("a3", "Shu Qi"),
            Actor("a4", "Amber Valletta"),
            Actor("a5", "Kevin Hart"),
        )
        insert(
            Role("m1", "a1", "Frank Martin", 1),
            Role("m1", "a3", "Lai", 2),
            Role("m2", "a1", "Frank Martin", 1),
            Role("m2", "a4", "Audrey Billings", 2),
            Role("m3", "a2", "Hobbs", 1),
            Role("m3", "a1", "Shaw", 2),
            Role("m4", "a2", "Spencer", 1),
            Role("m4", "a5", "Fridge", 2),
        )
    }
}