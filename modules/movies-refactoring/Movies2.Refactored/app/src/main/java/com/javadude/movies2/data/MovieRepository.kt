package com.javadude.movies2.data

import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val moviesFlow: Flow<List<Movie>>
    val actorsFlow: Flow<List<Actor>>

    fun getCastFlow(movieId: String): Flow<List<ExpandedRole>>
    fun getFilmographyFlow(actorId: String): Flow<List<ExpandedAppearance>>
    suspend fun getMovie(id: String): Movie
    suspend fun getActor(id: String): Actor
    suspend fun insert(vararg movies: Movie)
    suspend fun insert(vararg actors: Actor)
    suspend fun delete(vararg movies: Movie)
    suspend fun delete(vararg actors: Actor)
    suspend fun update(vararg movies: Movie)
    suspend fun update(vararg actors: Actor)
    suspend fun createMovie(): Movie
    suspend fun createActor(): Actor
    suspend fun deleteMoviesById(ids: List<String>)
    suspend fun deleteActorsById(ids: List<String>)
    suspend fun deleteRole(actorId: String, movieId: String)
    suspend fun resetDatabase()
}