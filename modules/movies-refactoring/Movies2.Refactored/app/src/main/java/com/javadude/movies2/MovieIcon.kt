package com.javadude.movies2

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.materialIcon
import androidx.compose.material.icons.materialPath
import androidx.compose.ui.graphics.vector.ImageVector

// copied base ImageVector setup from androidx.compose.material.icons.filled.Person.kt
//    Copyright 2021 The Android Open Source Project
//    License: Apache 2

// converted from ic_baseline_movie_24.xml
// <vector xmlns:android="http://schemas.android.com/apk/res/android"
//    android:width="24dp"
//    android:height="24dp"
//    android:viewportWidth="24"
//    android:viewportHeight="24"
//    android:tint="?attr/colorControlNormal">
//  <path
//      android:fillColor="@android:color/white"
//      android:pathData="M18,4l2,4h-3l-2,-4h-2l2,4h-3l-2,-4H8l2,4H7L5,4H4c-1.1,0 -1.99,0.9 -1.99,2L2,18c0,1.1 0.9,2 2,2h16c1.1,0 2,-0.9 2,-2V4h-4z"/>
//</vector>
//
// I broke the pathData into specific commands. (I took a quick peek for converters and couldn't
//   find any, but there must be some out there - I found a mention of something used internally
//   but it seems to be long gone...)
// You don't need to do this conversion, but it's a little more efficient and makes our common
//   code simpler - it only needs to worry about ImageVectors
// This example only had the following commands (uppercase = absolute, lowercase = relative)
//    M - moveTo
//    H - horizontalLineTo
//    V - verticalLineTo
//    L - lineTo
//    C - curveTo
//    Z - close
// I'm sure we'll see conversion tools shortly (I'm tempted to write a quick one...)

// Note that this is an extension property on Icons.Filled so we access it the same way
//   as Icons.Filled.Person

public val Icons.Filled.Movie: ImageVector
    get() {
        if (_movie != null) {
            return _movie!!
        }
        _movie = materialIcon(name = "Filled.Movie") {
            materialPath {
                moveTo(18f,4f)
                lineToRelative(2f,4f)
                horizontalLineToRelative(-3f)
                lineToRelative(-2f,-4f)
                horizontalLineToRelative(-2f)
                lineToRelative(2f,4f)
                horizontalLineToRelative(-3f)
                lineToRelative(-2f,-4f)
                horizontalLineTo(8f)
                lineToRelative(2f,4f)
                horizontalLineTo(7f)
                lineTo(5f,4f)
                horizontalLineTo(4f)
                curveToRelative(-1.1f,0f, -1.99f,0.9f, -1.99f,2f)
                lineTo(2f,18f)
                curveToRelative(0f,1.1f, 0.9f,2f, 2f,2f)
                horizontalLineToRelative(16f)
                curveToRelative(1.1f,0f, 2f,-0.9f, 2f,-2f)
                verticalLineTo(4f)
                horizontalLineToRelative(-4f)
                close()
            }
        }
        return _movie!!
    }

private var _movie: ImageVector? = null