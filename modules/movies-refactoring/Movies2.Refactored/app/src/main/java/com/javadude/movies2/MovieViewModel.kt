package com.javadude.movies2

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.Movie
import com.javadude.movies2.data.MovieDatabaseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlin.reflect.KClass

sealed interface Screen {
    val selection: ListSelection
}
data class MovieListScreen(override val selection: ListSelection): Screen
data class MovieDisplayScreen(override val selection: ListSelection): Screen
data class MovieEditScreen(override val selection: ListSelection): Screen
data class ActorListScreen(override val selection: ListSelection): Screen
data class ActorDisplayScreen(override val selection: ListSelection): Screen
data class ActorEditScreen(override val selection: ListSelection): Screen

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = MovieDatabaseRepository(application)
    private val currentScreen_ = MutableStateFlow<Screen?>(MovieListScreen(NoListSelection))
    val currentScreen: Flow<Screen?> = currentScreen_

    private var backStack = listOf<Screen>(MovieListScreen(NoListSelection))
        set(value) {
            field = value
            currentScreen_.value = value.lastOrNull()
        }

    val movies = repository.moviesFlow
    val actors = repository.actorsFlow

    fun getCastFlow(id: String) = repository.getCastFlow(id)
    fun getFilmographyFlow(id: String) = repository.getFilmographyFlow(id)
    suspend fun getMovie(id: String) = repository.getMovie(id)
    suspend fun getActor(id: String) = repository.getActor(id)
    suspend fun update(movie: Movie) = repository.update(movie)
    suspend fun update(actor: Actor) = repository.update(actor)
    suspend fun deleteMovie(id: String) = repository.delete(repository.getMovie(id))
    suspend fun deleteActor(id: String) = repository.delete(repository.getActor(id))
    suspend fun resetDatabase() = repository.resetDatabase()
    suspend fun createMovie() = repository.createMovie()
    suspend fun createActor() = repository.createActor()
    suspend fun deleteMoviesById(ids: List<String>) = repository.deleteMoviesById(ids)
    suspend fun deleteActorsById(ids: List<String>) = repository.deleteActorsById(ids)
    suspend fun deleteRole(actorId: String, movieId: String) = repository.deleteRole(actorId, movieId)

    // stack manipulation
    fun replaceTopSelection(selection: ListSelection) {
        val newTop = when (backStack.last()) {
            is ActorDisplayScreen -> ActorDisplayScreen(selection)
            is ActorEditScreen -> ActorEditScreen(selection)
            is ActorListScreen -> ActorListScreen(selection)
            is MovieDisplayScreen -> MovieDisplayScreen(selection)
            is MovieEditScreen -> MovieEditScreen(selection)
            is MovieListScreen -> MovieListScreen(selection)
        }
        backStack = backStack.dropLast(1) + newTop
    }
    fun push(screen: Screen) {
        backStack = backStack + screen
    }
    inline fun <reified T:Any> popSkipping() {
        popSkipping(T::class)
    }
    @PublishedApi
    internal fun popSkipping(screenClassToSkip: KClass<*>) {
        backStack = backStack.dropLast(1).dropLastWhile {
            screenClassToSkip.isInstance(it)
        }
    }
    fun pop() {
        backStack = backStack.dropLast(1)
    }
    fun peekOneBack() =
        if (backStack.size > 1) {
            backStack[backStack.size-2]
        } else {
            null
        }
}