package com.javadude.movies2.data

import android.app.Application
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieDatabaseRepository(application: Application)
    : MovieRepository {

    private val db =
        Room.databaseBuilder(
            application,
            MovieDatabase::class.java,
            "MOVIES"
        ).build()

    override val moviesFlow = db.dao().getMoviesFlow()
    override val actorsFlow = db.dao().getActorsFlow()

    override fun getCastFlow(movieId: String)= db.dao().getCastFlow(movieId)

    override fun getFilmographyFlow(actorId: String) = db.dao().getFilmographyFlow(actorId)

    override suspend fun getMovie(id: String) = withContext(Dispatchers.IO) {
        db.dao().getMovie(id)
    }

    override suspend fun getActor(id: String) = withContext(Dispatchers.IO) {
        db.dao().getActor(id)
    }

    override suspend fun insert(vararg movies: Movie) = withContext(Dispatchers.IO) {
        db.dao().insert(*movies)
    }

    override suspend fun insert(vararg actors: Actor) = withContext(Dispatchers.IO) {
        db.dao().insert(*actors)
    }

    override suspend fun delete(vararg movies: Movie) = withContext(Dispatchers.IO) {
        db.dao().delete(*movies)
    }

    override suspend fun delete(vararg actors: Actor) = withContext(Dispatchers.IO) {
        db.dao().delete(*actors)
    }

    override suspend fun update(vararg movies: Movie) = withContext(Dispatchers.IO) {
        db.dao().update(*movies)
    }
    override suspend fun update(vararg actors: Actor) = withContext(Dispatchers.IO) {
        db.dao().update(*actors)
    }

    override suspend fun resetDatabase() = withContext(Dispatchers.IO) {
        db.dao().resetDatabase()
    }

    override suspend fun createMovie() = withContext(Dispatchers.IO) {
        Movie(title = "", description = "").apply { insert(this) }
    }

    override suspend fun createActor() = withContext(Dispatchers.IO) {
        Actor(name = "").apply { insert(this) }
    }

    override suspend fun deleteMoviesById(ids: List<String>) = withContext(Dispatchers.IO) {
        db.dao().deleteMoviesById(ids)
    }

    override suspend fun deleteActorsById(ids: List<String>) = withContext(Dispatchers.IO) {
        db.dao().deleteActorsById(ids)
    }

    override suspend fun deleteRole(actorId: String, movieId: String) = withContext(Dispatchers.IO) {
        db.dao().deleteRole(actorId, movieId)
    }
}