package com.javadude.activityexample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.javadude.activityexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)

        binding.button1.setOnClickListener {
            val intent = Intent(this, TargetActivity::class.java)
            startActivity(intent)
        }
        binding.button2.setOnClickListener {
            binding.status.text = getString(R.string.button_2_pressed)
        }

        setContentView(binding.root)
//
//        setContentView(R.layout.activity_main)
//
//        val button1 = findViewById<Button>(R.id.button1)
//        val button2 = findViewById<Button>(R.id.button2)
//        val hello = findViewById<TextView>(R.id.hello)
//        val status = findViewById<TextView>(R.id.status)
//
//        button1.setOnClickListener {
//            hello.text = getString(R.string.button_1_pressed)
//        }
//        button2.setOnClickListener {
//            status.text = getString(R.string.button_2_pressed)
//        }
    }
}