package com.javadude.composegauge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.javadude.composegauge.ui.theme.ComposeGaugeTheme
import kotlinx.coroutines.delay
import kotlin.math.min

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeGaugeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    var fillColor by remember { mutableStateOf(Color.Black) }
                    var value by remember { mutableStateOf(0f) }

                    var hour by remember { mutableStateOf(0) }
                    var minute by remember { mutableStateOf(0) }

                    LaunchedEffect(true) {
                        while(true) {
                            if ((minute + 1) > 59) {
                                hour++
                                minute = 0
                            } else {
                                minute++
                            }
                            delay(10)
                        }
                    }
//                    LaunchedEffect(true) {
//                        var colorNum = 0
//                        while(true) {
//                            value += 0.1f
//                            withContext(Dispatchers.Main) {
//                                value = value.coerceIn(0f, 1f)
//                                fillColor =
//                                    when (colorNum) {
//                                        0 -> Color.Blue
//                                        1 -> Color.Green
//                                        else -> Color.Red
//                                    }
//                            }
//                            colorNum = (colorNum+1) % 3
//                            delay(1000)
//                        }
//                    }

//                    Gauge(
//                        outlineColor = Color.Black,
//                        fillColor = fillColor,
//                        outlineWidth = 8.dp,
//                        value = value,
//                        modifier = Modifier.fillMaxSize()
//                    )

                    AnalogClock(
                        hour = hour,
                        minute = minute,
                        modifier = Modifier.fillMaxSize()
                    )
                }
            }
        }
    }
}

@Composable
fun AnalogClock(
    hour: Int,
    minute: Int,
    outlineColor: Color = Color.Black,
    outlineWidth: Dp = 8.dp,
    fillColor: Color = Color.Gray,
    hourHandColor: Color = Color.DarkGray,
    minuteHandColor: Color = Color.LightGray,
    hourMarkerColor: Color = Color.Black,
    modifier: Modifier
) {
    with(LocalDensity.current) {
        val outlineWidthPx = remember(outlineWidth) {
            outlineWidth.toPx()
        }
        Canvas(modifier = modifier) {
            val diameter = min(size.width, size.height) * 0.8f
            val radius = diameter/2

            drawCircle(
                color = fillColor,
                radius = radius,
            )
            drawCircle(
                color = outlineColor,
                radius = radius,
                style = Stroke(outlineWidthPx)
            )

            val hourMarkerLength = radius/10f
            repeat(12) {
                rotate(it/12f * 360) {
                    val start = center - Offset(0f, radius)
                    val end = start + Offset(0f, hourMarkerLength)
                    drawLine(
                        color = hourMarkerColor,
                        start = start,
                        end = end,
                        strokeWidth = outlineWidthPx
                    )
                }
            }

            val minuteRatio = minute / 60f
            val hourRatio = (hour+minuteRatio) / 12f

            rotate(minuteRatio * 360) {
                drawLine(
                    color = minuteHandColor,
                    start = center - Offset(0f, radius*0.9f),
                    end = center,
                    strokeWidth = outlineWidthPx
                )
            }
            rotate(hourRatio * 360) {
                drawLine(
                    color = hourHandColor,
                    start = center - Offset(0f, radius*0.6f),
                    end = center,
                    strokeWidth = outlineWidthPx
                )
            }
        }
    }
}



@Composable
fun Gauge(
    outlineColor: Color,
    fillColor: Color,
    outlineWidth: Dp,
    value: Float,
    modifier: Modifier
) {
    with(LocalDensity.current) {
        val outlineWidthPx = remember(outlineWidth) {
            outlineWidth.toPx()
        }
        Canvas(modifier = modifier.padding(8.dp)) {
            val fillHeight = size.height * value
            val offsetY = size.height - fillHeight
            drawRect(
                color = fillColor,
                topLeft = Offset(0f,offsetY),
                size = Size(size.width, fillHeight)
            )
            drawRect(
                color = outlineColor,
                topLeft = Offset(0f, 0f),
                size = Size(size.width, size.height),
                style = Stroke(outlineWidthPx)
            )
        }
    }
}