package com.javadude.composegraph

import androidx.compose.runtime.Immutable
import androidx.compose.ui.geometry.Offset

sealed interface ToolType
object DrawLine: ToolType
object Select: ToolType
sealed interface ShapeType: ToolType
object Circle: ShapeType
object Square: ShapeType
object Triangle: ShapeType

@Immutable
data class Shape(
    val shapeType: ShapeType,
    val offset: Offset
)