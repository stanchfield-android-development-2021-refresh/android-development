package com.javadude.composegraph

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

class GraphViewModel: ViewModel() {
    private val selectedTool_ = MutableStateFlow<ToolType>(Square)
    val selectedTool: Flow<ToolType>
        get() = selectedTool_

    private val shapes_ = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: Flow<List<Shape>>
        get() = shapes_

    fun select(tool: ToolType) {
        selectedTool_.value = tool
    }
    fun add(shape: Shape) {
        shapes_.value = shapes_.value + shape
    }
}