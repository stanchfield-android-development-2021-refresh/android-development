package com.javadude.movies2.data

import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val movies: Flow<List<Movie>>
    val actors: Flow<List<Actor>>

    suspend fun getCast(movieId: String): List<ExpandedRole>
    suspend fun getMovie(id: String): Movie
    suspend fun getActor(id: String): Actor
    suspend fun insert(vararg movies: Movie)
    suspend fun insert(vararg actors: Actor)
    suspend fun delete(vararg movies: Movie)
    suspend fun delete(vararg actors: Actor)
    suspend fun update(vararg movies: Movie)
    suspend fun update(vararg actors: Actor)
    suspend fun createMovie(): Movie
    suspend fun deleteMoviesById(ids: List<String>)
    suspend fun resetDatabase()
}