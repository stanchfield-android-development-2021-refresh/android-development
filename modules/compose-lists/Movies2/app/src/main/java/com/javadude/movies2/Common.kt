package com.javadude.movies2

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.javadude.movies2.data.Movie

@Composable
fun Label(
    text: String,
    modifier: Modifier = Modifier
) = CommonText(
    text = text,
    style = MaterialTheme.typography.h6,
    startPadding = 8.dp,
    modifier = modifier
)

@Composable
fun Display(
    text: String,
    modifier: Modifier = Modifier
) = CommonText(
    text = text,
    style = MaterialTheme.typography.h5,
    startPadding = 24.dp,
    modifier = modifier
)

@Composable
fun CommonText(
    text: String,
    style: TextStyle,
    startPadding: Dp,
    modifier: Modifier
) {
    Text(
        text = text,
        style = style,
        modifier = modifier
            .padding(
                start = startPadding,
                end = 8.dp,
                top = 8.dp,
                bottom = 8.dp
            )
            .fillMaxWidth()
    )
}

// LIST STUFF

sealed interface ListSelection {
    operator fun contains(id: String): Boolean
}
object NoListSelection: ListSelection {
    override fun contains(id: String) = false
}
data class SingleListSelection(val id: String): ListSelection {
    override fun contains(id: String) = (this.id == id)
}
data class MultiListSelection(val ids: List<String>): ListSelection {
    override fun contains(id: String) = (id in ids)
}

//@Composable
//fun SampleCall() {
//    val movies = emptyList<Movie>()
//    CommonList(
//        items = movies,
//        selection = ,
//        getId = { it.id },
//        iconId = ,
//        iconDescription = ,
//        onSelectionChange =
//    )
//}

// Tangent on operator overloading - stick to get/set/contains
//class Array2D {
//    operator fun get(x: Int, y: Int) = "Hello"
//    operator fun set(x: Int, y: Int, value: String) { }
//}
//
//fun arraySample() {
//    val array = Array2D()
//    array[4, 5] = "Bye"
//    println(array[1,2])
//}


@ExperimentalMaterialApi
@Composable
fun <T> CommonList(
    items: List<T>,
    selection: ListSelection,
//    getId: (T) -> String, // we'll use the extension function instead
    getId: T.() -> String,
    getText: T.() -> String,
    @DrawableRes iconId: Int,
    @StringRes iconDescription: Int,
    onSelectionChange: (ListSelection) -> Unit,
    modifier: Modifier
) {
    LazyColumn(modifier = modifier) {
        items(
            items = items,
            key = { it.getId() }
        ) { item ->
            val id = item.getId()

            val selected = id in selection // calls selection.contains(id)

            fun selectionColor(
                colorIfSelected: Color,
                colorIfNotSelected: Color
            ) = if (selected) colorIfSelected else colorIfNotSelected

            Card(
                elevation = 2.dp,
                backgroundColor =
                    selectionColor(MaterialTheme.colors.primary, MaterialTheme.colors.surface),

                shape = RoundedCornerShape(corner = CornerSize(16.dp)),
                onClick = {
                    onSelectionChange(SingleListSelection(item.getId()))
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Image(
                        painter = painterResource(id = iconId),
                        colorFilter =
                            ColorFilter.tint(
                                selectionColor(MaterialTheme.colors.onPrimary, MaterialTheme.colors.onSurface)
                            ),
                        contentDescription = stringResource(iconDescription),
                        modifier = Modifier
                            .padding(8.dp)
                            .size(48.dp)
                            .clickable {
                                onSelectionChange(
                                    when (selection) {
                                        NoListSelection, is SingleListSelection ->
                                            MultiListSelection(listOf(id))
                                        is MultiListSelection ->
                                            if (selection.ids.contains(id)) {
                                                if (selection.ids.size == 1) {
                                                    NoListSelection
                                                } else {
                                                    MultiListSelection(selection.ids - id)
                                                }
                                            } else {
                                                MultiListSelection(selection.ids + id)
                                            }
                                    }
                                )
                            }
                    )
                    Text(
                        text = item.getText(),
                        color = selectionColor(MaterialTheme.colors.onPrimary, MaterialTheme.colors.onSurface)
                    )
                }
            }
        }
    }
}

@ExperimentalComposeUiApi
@Composable
fun CommonTextField(
    @StringRes labelId: Int,
    value: String,
    onValueChange: (String) -> Unit,
    onBack: () -> Unit
) {
    TextField(
        value = value,
        label = { Text(text = stringResource(id = labelId))},
        onValueChange = onValueChange,
        modifier = Modifier.padding(8.dp)
            // as of Compose v1.1, the following onKeyEvent() call is no longer needed
            //   (I'm leaving it in to match the video)
            // workaround for bug: https://issuetracker.google.com/issues/192433071
            // for the moment, we'll just force it to consume the Back key in onKeyEvent
            //    and explicitly pop the stack in the navigator
            // a fix for the bug has been submitted but per the comments it sounds like it won't make the 1.0.0 release
            // NOTE: the "Back" key reference is currently experimental API and could change
            .onKeyEvent {
                if (it.key == Key.Back) {
                    onBack()
                    true
                } else {
                    false
                }
            }
    )
}