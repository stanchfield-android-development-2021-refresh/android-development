package com.javadude.movies2

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.ExpandedRole
import com.javadude.movies2.data.Movie
import kotlinx.coroutines.launch

// movie selection
//   tap to open it
//   tap icon or long press to start multi-selection
//      further taps add to/remove from selection

@ExperimentalMaterialApi
@Composable
fun MovieList(
    isWide: Boolean,
    movies: List<Movie>,
    selection: ListSelection,
    onSelectionChange: (ListSelection) -> Unit,
    onAddMovie: () -> Unit,
    onDeleteMovies: (MultiListSelection) -> Unit,
    onResetDb: () -> Unit,
) {
    Scaffold(
        topBar = {
            SelectionTopAppBar(
                selection = selection,
                title = stringResource(R.string.movies),
                onSelectionChange = onSelectionChange,
                multiSelectActions = { multiSelection ->
                    ToolbarButton(icon = Icons.Filled.Delete, iconDescription = R.string.delete_movies) {
                        onDeleteMovies(multiSelection)
                    }
                },
                singleSelectActions = {
                    ToolbarButton(icon = Icons.Filled.Add, iconDescription = R.string.add_movie) {
                        onAddMovie()
                    }
                    ToolbarButton(icon = Icons.Filled.Refresh, iconDescription = R.string.reset_db) {
                        onResetDb()
                    }
                }
            )
        },
        content = {
            if (isWide) {
                Row {
                    MovieListPart(
                        movies = movies,
                        selection = selection,
                        onSelectionChange = onSelectionChange,
                        modifier = Modifier
                            .weight(40f)
                            .fillMaxHeight()
                    )
                    Text(text = stringResource(id = R.string.please_select_a_movie), modifier = Modifier
                        .weight(60f)
                        .fillMaxHeight())
                }

            } else {
                MovieListPart(
                    movies = movies,
                    selection = selection,
                    onSelectionChange = onSelectionChange,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    )
}

@Composable
fun ToolbarButton(
    icon: ImageVector,
    @StringRes iconDescription: Int,
    onClick: () -> Unit
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = icon,
            contentDescription = stringResource(iconDescription)
        )
    }
}

@Composable
fun SelectionTopAppBar(
    selection: ListSelection,
    title: String,
    onSelectionChange: (ListSelection) -> Unit,
    multiSelectActions: @Composable (MultiListSelection) -> Unit,
    singleSelectActions: @Composable () -> Unit,
) {
    if (selection is MultiListSelection) {
        TopAppBar(
            navigationIcon = {
                ToolbarButton(icon = Icons.Filled.ArrowBack, iconDescription = R.string.cancel_multi_select) {
                    onSelectionChange(NoListSelection)
                }
            },
            title = { Text(text = selection.ids.size.toString()) },
            actions = {
                multiSelectActions(selection)
            }
        )

    } else {
        TopAppBar(
            title = { Text(text = title) },
            actions = {
                singleSelectActions()
            }
        )
    }
}
@ExperimentalMaterialApi
@Composable
fun MovieListPart(
    movies: List<Movie>,
    selection: ListSelection,
    onSelectionChange: (ListSelection) -> Unit,
    modifier: Modifier
) {
    CommonList(
        items = movies,
        selection = selection,
        getId = { id },
        getText = { title },
        iconId = R.drawable.ic_baseline_movie_24,
        iconDescription = R.string.movie_icon,
        onSelectionChange = onSelectionChange,
        modifier = modifier
    )
}

@ExperimentalMaterialApi
@Composable
fun MovieDisplay(
    isWide: Boolean,
    movies: List<Movie>,
    selection: ListSelection,
    onSelectionChange: (ListSelection) -> Unit,
    onEditMovie: (String) -> Unit,
    onAddMovie: () -> Unit,
    onDeleteMovies: (MultiListSelection) -> Unit,
    getMovie: suspend (String) -> Movie,
    getCast: suspend (String) -> List<ExpandedRole>
) {
    val movieId = remember(selection) {
        when (selection) {
            NoListSelection, is MultiListSelection -> null
            is SingleListSelection -> selection.id
        }
    }
    var loadState by remember { mutableStateOf<MovieLoadState>(MovieLoadState.NotSelected) }
    var movie by remember { mutableStateOf<Movie?>(null) }
    var cast by remember { mutableStateOf<List<ExpandedRole>?>(null) }
    val title by remember(movie) { mutableStateOf(movie?.title ?: "(Movie Loading)") }

    LaunchedEffect(key1 = movieId) {
        if (movieId == null) {
            loadState = MovieLoadState.NotSelected
            movie = null
            cast = null
        } else {
            loadState = MovieLoadState.Loading
            movie = getMovie(movieId)
            cast = getCast(movieId)
            loadState = MovieLoadState.Loaded
        }
    }

    Scaffold(
        topBar = {
            SelectionTopAppBar(
                selection = selection,
                title = title,
                onSelectionChange = onSelectionChange,
                multiSelectActions = { multiSelection ->
                    if (isWide) {
                        ToolbarButton(icon = Icons.Filled.Delete, iconDescription = R.string.delete_movies) {
                            onDeleteMovies(multiSelection)
                        }
                    }
                },
                singleSelectActions = {
                    if (isWide) {
                        ToolbarButton(icon = Icons.Filled.Add, iconDescription = R.string.add_movie) {
                            onAddMovie()
                        }
                    }
                    if (movieId != null) {
                        ToolbarButton(icon = Icons.Filled.Edit, iconDescription = R.string.edit_movie) {
                            onEditMovie(movieId)
                        }
                    }
                }
            )
        },
        content = {
            if (isWide) {
                Row {
                    MovieListPart(
                        movies = movies,
                        selection = selection,
                        onSelectionChange = onSelectionChange,
                        modifier = Modifier
                            .weight(40f)
                            .fillMaxHeight()
                    )
                    MovieDisplayPart(
                        loadState = loadState,
                        movie = movie,
                        cast = cast,
                        modifier = Modifier
                            .weight(60f)
                            .fillMaxHeight())
                }
            } else {
                MovieDisplayPart(
                    loadState = loadState,
                    movie = movie,
                    cast = cast,
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
    )
}

enum class MovieLoadState {
    NotSelected, Loading, Loaded
}

@Composable
fun MovieDisplayPart(
    loadState: MovieLoadState,
    movie: Movie?,
    cast: List<ExpandedRole>?,
    modifier: Modifier
) {
    when (loadState) {
        MovieLoadState.NotSelected -> Text(stringResource(R.string.please_select_a_movie), modifier = modifier)
        MovieLoadState.Loading -> Text(stringResource(R.string.loading), modifier = modifier)
        MovieLoadState.Loaded -> movie?.let {
            Column(
                modifier = modifier.verticalScroll(rememberScrollState())
            ) {
                Label(text = stringResource(R.string.title))
                Display(text = it.title)
                Label(text = stringResource(R.string.description))
                Display(text = it.description)
                cast?.let {
                    Label(text = stringResource(R.string.cast))
                    it.forEach { role ->
                        Display(text = "${role.character} - ${role.actor.name}")
                    }
                }
            }
        } ?: throw IllegalStateException("If load state is Loaded, movie must be non-null")
    }
}

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@Composable
fun MovieEdit(
    isWide: Boolean,
    movies: List<Movie>,
    selection: ListSelection,
    onSelectionChange: (ListSelection) -> Unit,
    onMovieChange: (Movie) -> Unit,
    getMovie: suspend (String) -> Movie,
    onAddMovie: () -> Unit,
    onBack: () -> Unit,
    onDeleteMovies: (MultiListSelection) -> Unit,
) {
    val movieId = remember(selection) {
        when (selection) {
            NoListSelection, is MultiListSelection -> null
            is SingleListSelection -> selection.id
        }
    }
    var loadState by remember { mutableStateOf<MovieLoadState>(MovieLoadState.NotSelected) }
    var movie by remember { mutableStateOf<Movie?>(null) }
    var title by remember(movie) { mutableStateOf(movie?.title ?: "") }

    LaunchedEffect(key1 = movieId) {
        if (movieId == null) {
            loadState = MovieLoadState.NotSelected
            movie = null
        } else {
            loadState = MovieLoadState.Loading
            movie = getMovie(movieId)
            loadState = MovieLoadState.Loaded
        }
    }

    Scaffold(
        topBar = {
            val toolbarTitle = title.notBlankOr { stringResource(R.string.no_title) }
            SelectionTopAppBar(
                selection = selection,
                title = toolbarTitle,
                onSelectionChange = onSelectionChange,
                multiSelectActions = { multiSelection ->
                    if (isWide) {
                        ToolbarButton(icon = Icons.Filled.Delete, iconDescription = R.string.delete_movies) {
                            onDeleteMovies(multiSelection)
                        }
                    }
                },
                singleSelectActions = {
                    if (isWide) {
                        ToolbarButton(icon = Icons.Filled.Add, iconDescription = R.string.add_movie) {
                            onAddMovie()
                        }
                    }
                }
            )
        },
        content = {
            if (isWide) {
                Row {
                    MovieListPart(
                        movies = movies,
                        selection = selection,
                        onSelectionChange = onSelectionChange,
                        modifier = Modifier
                            .weight(40f)
                            .fillMaxHeight()
                    )
                    MovieEditPart(
                        loadState = loadState,
                        movie = movie,
                        onMovieChange = onMovieChange,
                        modifier = Modifier
                            .fillMaxHeight()
                            .weight(60f),
                        onTitleChange = { title = it},
                        onBack = onBack
                    )
                }
            } else {
                MovieEditPart(
                    loadState = loadState,
                    movie = movie,
                    onMovieChange = onMovieChange,
                    modifier = Modifier.fillMaxSize(),
                    onTitleChange = { title = it},
                    onBack = onBack
                )
            }
        }
    )
}

@ExperimentalComposeUiApi
@Composable
fun MovieEditPart(
    loadState: MovieLoadState,
    movie: Movie?,
    onMovieChange: (Movie) -> Unit,
    onTitleChange: (String) -> Unit,
    onBack: () -> Unit,
    modifier: Modifier
) {
    var title by remember(movie) { mutableStateOf(movie?.title ?: "") }
    var description by remember(movie) { mutableStateOf(movie?.description ?: "") }

    when (loadState) {
        MovieLoadState.NotSelected -> Text(stringResource(R.string.please_select_a_movie), modifier = modifier)
        MovieLoadState.Loading -> Text(stringResource(R.string.loading), modifier = modifier)
        MovieLoadState.Loaded -> movie?.let { nonNullMovie ->
            Column(
                modifier = modifier.verticalScroll(rememberScrollState())
            ) {
                CommonTextField(
                    labelId = R.string.title,
                    value = title,
                    onValueChange = {
                        title = it
                        onTitleChange(it)
                        onMovieChange(Movie(id = nonNullMovie.id, title = it, description = description))
                    },
                    onBack = onBack
                )
                CommonTextField(
                    labelId = R.string.description,
                    value = description,
                    onValueChange = {
                        description = it
                        onMovieChange(Movie(id = nonNullMovie.id, title = title, description = it))
                    },
                    onBack = onBack
                )
            }
        } ?: throw IllegalStateException("If load state is Loaded, movie must be non-null")
    }

}

@Composable
fun String.notBlankOr(alternate: @Composable () -> String) =
    if (isNotBlank()) this else alternate()

@Composable
fun ActorList(
   actors: List<Actor>
) {

}

@Composable
fun ActorEdit(
   actor: Actor
) {

}

@Composable
fun ActorDisplay(
   actor: Actor
) {

}