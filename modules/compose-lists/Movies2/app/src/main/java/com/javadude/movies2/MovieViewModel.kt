package com.javadude.movies2

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.ExpandedRole
import com.javadude.movies2.data.Movie
import com.javadude.movies2.data.MovieDatabaseRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow

sealed interface Screen
data class MovieListScreen(val selection: ListSelection): Screen
data class MovieDisplayScreen(val selection: ListSelection): Screen
data class MovieEditScreen(val selection: ListSelection): Screen
data class ActorListScreen(val selection: ListSelection): Screen
data class ActorDisplayScreen(val selection: ListSelection): Screen
data class ActorEditScreen(val selection: ListSelection): Screen

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = MovieDatabaseRepository(application)
    private val currentScreen_ = MutableStateFlow<Screen?>(MovieListScreen(NoListSelection))
    val currentScreen: Flow<Screen?> = currentScreen_

    private var backStack = listOf<Screen>(MovieListScreen(NoListSelection))
        set(value) {
            field = value
            currentScreen_.value = value.lastOrNull()
        }

    val movies = repository.movies
    val actors = repository.actors

    suspend fun getMovie(id: String) = repository.getMovie(id)
    suspend fun getCast(id: String) = repository.getCast(id)
    suspend fun getActor(id: String) = repository.getActor(id)
    suspend fun update(movie: Movie) = repository.update(movie)
    suspend fun deleteMovie(id: String) = repository.delete(repository.getMovie(id))
    suspend fun deleteActor(id: String) = repository.delete(repository.getActor(id))
    suspend fun resetDatabase() = repository.resetDatabase()
    suspend fun createMovie() = repository.createMovie()
    suspend fun deleteMoviesById(ids: List<String>) = repository.deleteMoviesById(ids)

    // stack manipulation
    fun replaceTop(screen: Screen) {
        backStack = backStack.dropLast(1) + screen
    }
    fun push(screen: Screen) {
        backStack = backStack + screen
    }
    fun pop() {
        backStack = backStack.dropLast(1)
    }
    fun peekOneBack() =
        if (backStack.size > 1) {
            backStack[backStack.size-2]
        } else {
            null
        }
}