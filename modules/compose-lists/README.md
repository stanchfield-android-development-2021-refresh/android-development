# Module 4.1 Jetpack Compose Lists

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 2.2: Android Databases Using Room](../modules/room)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Lists are often a core component of a data-centric application. Interacting with lists can seem daunting, but we'll demystify that interaction in this module.

We'll build on our movie application, including

* Single and multi-selection in lists
* Factoring out more common function
* Showing alternate toolbars to present different options for single or multi-select
* Integrating list and detail displays for wider screens

> Note: Google fixed the "back" bug for TextFields in Jetpack Compose v1.1. You no longer need the extra code (though it doesn't seem to hurt if it remains)
> I've added a comment where it appears in the repository

## Objectives

* Create a list-based interface that allows a user to navigate by tapping on an item in a list
* React to different screen sizes and multi selection to improve the user's experience

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 2:32:28

            
#### Example: Compose - Lists (Fall 2021) (1:31:04)
 | [![Example: Compose - Lists (Fall 2021)](https://img.youtube.com/vi/ZnmxNQjMw0g/mqdefault.jpg)](https://youtu.be/ZnmxNQjMw0g) |
 | -- |

                

#### Example: Compose - Side-by-Side UIs (Fall 2021) (1:01:24)
 | [![Example: Compose - Side-by-Side UIs (Fall 2021)](https://img.youtube.com/vi/bviUBfjxcyo/mqdefault.jpg)](https://youtu.be/bviUBfjxcyo) |
 | -- |

                

            