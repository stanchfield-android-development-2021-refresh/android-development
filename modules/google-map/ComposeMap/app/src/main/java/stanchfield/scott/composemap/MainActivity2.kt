package stanchfield.scott.composemap

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.remember
import androidx.compose.ui.res.stringResource
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.model.LatLng
import com.google.maps.android.ktx.addMarker
import stanchfield.scott.composemap.ui.theme.ComposeMapTheme

class MainActivity2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val latitude = 39.163400392993395
        val longitude = -76.89197363588868

        setContent {
            ComposeMapTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Scaffold(
                        topBar = {
                            TopAppBar(
                                title = { Text(stringResource(id = R.string.app_name)) }
                            )
                        },
                        content = {
                            val cameraPosition = remember(latitude, longitude) {
                                LatLng(latitude, longitude)
                            }
                            ComposeMap(savedInstanceState = savedInstanceState) { googleMap ->
                                googleMap.addMarker { position(cameraPosition) }
                                googleMap.animateCamera(
                                    CameraUpdateFactory.newLatLngZoom(cameraPosition, 15f),
                                    1000, null
                                )
                            }
                        }
                    )
                }
            }
        }
    }
}
