package stanchfield.scott.composemap

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.model.BitmapDescriptor
import com.google.android.libraries.maps.model.LatLng
import com.google.android.libraries.maps.model.LatLngBounds
import com.google.android.libraries.maps.model.Marker
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.addPolyline
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import stanchfield.scott.composemap.ui.theme.ComposeMapTheme

class MainActivity3 : ComponentActivity() {
    private val viewModel by viewModels<CarViewModel>()
    private lateinit var googleMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var carIcon: BitmapDescriptor

    private val locationCallback = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            viewModel.updateCurrentLocation(locationResult.lastLocation)
        }
    }

    private var savedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.savedInstanceState = savedInstanceState

        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
            .addOnSuccessListener {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {
                    getLocationPermission.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
                } else {
                    startLocationAndMap()
                }
            }.addOnFailureListener(this) {
                Toast.makeText(this, "Google Play services required (or upgrade required)", Toast.LENGTH_SHORT).show()
                finish()
            }
    }

    private val getLocationPermission = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGranted ->
        if (isGranted.values.any { it }) {
            startLocationAndMap()
        } else {
            AlertDialog.Builder(this)
                .setTitle("Permissions Needed")
                .setMessage("We need coarse-location or fine-location permission to locate a car (fine location is highly recommended for accurate car locating). Please allow these permissions via App Info settings")
                .setCancelable(false)
                .setNegativeButton("Quit") { _,_ -> finish() }
                .setPositiveButton("App Info") { _,_ ->
                    startActivity(
                        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                            data = Uri.parse("package:$packageName")
                        }
                    )
                    finish()
                }
                .show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationAndMap() {
        val locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 5
            fastestInterval = 0
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())

        setContent {
            val currentLocation by viewModel.currentLocation.collectAsState(initial = null)
            val carLatLng by viewModel.carLatLng.collectAsState(initial = null)
            var carMarker by remember { mutableStateOf<Marker?>(null) }
            val scope = rememberCoroutineScope()

            fun setCarLocation() {
                carMarker?.remove()
                carMarker = viewModel.setCarLocation()?.let {
                    addCarMarker(it)
                }
            }
            fun clearCarLocation() {
                carMarker?.remove()
                carMarker = null
                viewModel.clearCarLocation()
            }

            ComposeMapTheme {
                Surface(color = MaterialTheme.colors.background) {
                    Scaffold(
                        topBar = {
                            ToolBar(
                                currentLocation = currentLocation,
                                carLatLng = carLatLng,
                                onSetCarLocation = ::setCarLocation,
                                onClearCarLocation = ::clearCarLocation,
                                onWalkToCar = {
                                    currentLocation?.let { curr ->
                                        carLatLng?.let { car ->
                                            val uri = Uri.parse("https://www.google.com/maps/dir/?api=1&origin=${curr.latitude},${curr.longitude}&destination=${car.latitude},${car.longitude}&travelmode=walking")
                                            startActivity(Intent(Intent.ACTION_VIEW, uri).apply {
                                                setPackage("com.google.android.apps.maps")
                                            })
                                        } ?: Toast.makeText(this, "Cannot navigate; no car location available", Toast.LENGTH_LONG).show()
                                    } ?: Toast.makeText(this, "Cannot navigate; no current location available", Toast.LENGTH_LONG).show()
                                }
                            )
                        },
                        content = {
                            ComposeMap(savedInstanceState = savedInstanceState) { googleMap ->
                                this.googleMap = googleMap
                                googleMap.isMyLocationEnabled = true
                                carIcon = loadBitmapDescriptor(R.drawable.ic_car)

                                carMarker = carLatLng?.let {
                                    googleMap.animateCamera(
                                        CameraUpdateFactory.newLatLngZoom(it, 15f),
                                        1000, null
                                    )
                                    addCarMarker(it)
                                } ?: run {
                                    var latLngBounds: LatLngBounds? = null

                                    fun LatLng.addToBounds() {
                                        latLngBounds = latLngBounds?.including(this) ?: LatLngBounds(this, this)
                                    }

                                    val point1 = LatLng(39.163742, -76.900235)
                                    val point2 = LatLng(39.163467, -76.897381)
                                    val point3 = LatLng(39.162610, -76.899500)
                                    point1.addToBounds()
                                    point2.addToBounds()
                                    point3.addToBounds()

                                    val polylineColor = ContextCompat.getColor(this, R.color.purple_700)
                                    val polyline = googleMap.addPolyline {
                                        color(polylineColor)
                                        add(point1, point2, point3)
                                    }

                                    scope.launch {
                                        delay(3000)
                                        polyline.remove()
                                    }

                                    val padding = resources.getDimension(R.dimen.bounds_padding)
                                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, padding.toInt()))

                                    null
                                }
                            }
                        }
                    )
                }

            }
        }
    }

    private fun Context.addCarMarker(latLng: LatLng) =
        googleMap.addMarker {
            position(latLng)
            anchor(0.5f, 0.5f)
            icon(carIcon)
            title(getString(R.string.parked_here))
            snippet("Lat/Lng: ${latLng.latitude}, ${latLng.longitude}")
        }
}

@Composable
private fun ToolBar(
    currentLocation: Location?,
    carLatLng: LatLng?,
    onSetCarLocation: () -> Unit,
    onClearCarLocation: () -> Unit,
    onWalkToCar: () -> Unit
) {
    TopAppBar(
        title = { Text(stringResource(id = R.string.car_finder)) },
        actions = {
            if (currentLocation != null) {
                IconButton(onClick = { onSetCarLocation() }) {
                    Icon(
                        imageVector = Icons.Filled.Star,
                        contentDescription = stringResource(id = R.string.remember_location)
                    )
                }
            }
            if (carLatLng != null) {
                IconButton(onClick = { onWalkToCar() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_baseline_directions_walk_24),
                        contentDescription = stringResource(id = R.string.walk)
                    )
                }
                IconButton(onClick = { onClearCarLocation() }) {
                    Icon(
                        imageVector = Icons.Filled.Delete,
                        contentDescription = stringResource(id = R.string.forget)
                    )
                }
            }
        }
    )
}
