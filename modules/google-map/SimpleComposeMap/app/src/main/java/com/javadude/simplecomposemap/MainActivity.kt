package com.javadude.simplecomposemap

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import com.javadude.simplecomposemap.ui.theme.SimpleComposeMapTheme
import kotlinx.coroutines.launch

const val latitude = 39.163400392993395
const val longitude = -76.89197363588868
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SimpleComposeMapTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    val mapView = remember {
                        MapView(this).apply {
                            id = R.id.google_map
                        }
                    }

                    DisposableEffect(lifecycle, mapView) {
                        val observer = LifecycleEventObserver { _, event ->
                            when (event) {
                                Lifecycle.Event.ON_CREATE -> mapView.onCreate(savedInstanceState)
                                Lifecycle.Event.ON_START -> mapView.onStart()
                                Lifecycle.Event.ON_RESUME -> mapView.onResume()
                                Lifecycle.Event.ON_PAUSE -> mapView.onPause()
                                Lifecycle.Event.ON_STOP -> mapView.onStop()
                                Lifecycle.Event.ON_DESTROY -> mapView.onDestroy()
                                else -> throw IllegalStateException("Unhandled lifecycle event")
                            }
                        }
                        lifecycle.addObserver(observer)
                        onDispose {
                            lifecycle.removeObserver(observer)
                        }
                    }

                    val scope = rememberCoroutineScope()

                    val cameraPosition = remember(latitude, longitude) {
                        LatLng(latitude, longitude)
                    }

                    AndroidView(
                        factory = { mapView },
                        update = {
                            scope.launch {
                                val googleMap = it.awaitMap()
                                googleMap.addMarker { position(cameraPosition) }
                                googleMap.animateCamera(
                                    CameraUpdateFactory.newLatLngZoom(cameraPosition, 15f),
                                    1000, null
                                )
                            }
                        }
                    )

                }
            }
        }
    }
}

