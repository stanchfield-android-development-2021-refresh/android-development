# Module 9.1 Google Maps

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Displaying a map can be a useful addition to many applications. Whether you're displaying the location of a business or the user's
current location, Google Maps makes it (fairly) easy!

In this module, we'll look at basic Google Maps setup in a Jetpack Compose application, then create a simple car-finder application that can rememer where you 
parked and help you navigate back to your car.

## Objectives

* Set up an API key on the Google Developer Console to gain access to online map data
* Create a simple Google Maps application that displays a location
* Bridge the old "views" UI system in Android with Jetpack Compose
* Create more complex applications using location services

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:17:46

            
#### Google Maps: Overview (Fall 2021) (09:09)
 | [![Google Maps: Overview (Fall 2021)](https://img.youtube.com/vi/VvNdc9eNcbc/mqdefault.jpg)](https://youtu.be/VvNdc9eNcbc) |
 | -- |

                

#### Google Maps: Basic Example (Fall 2021) (35:31)
 | [![Google Maps: Basic Example (Fall 2021)](https://img.youtube.com/vi/G_NDiQSHquo/mqdefault.jpg)](https://youtu.be/G_NDiQSHquo) |
 | -- |

                

#### Google Maps: Car Finder (Fall 2021) (33:06)
 | [![Google Maps: Car Finder (Fall 2021)](https://img.youtube.com/vi/4Pmu7yVKYqY/mqdefault.jpg)](https://youtu.be/4Pmu7yVKYqY) |
 | -- |

                

            