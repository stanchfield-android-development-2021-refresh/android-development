# Module 7.1 Jetpack Compose Graphics 2

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 2.2: Android Databases Using Room](../modules/room)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
* [Module 6.1: Jetpack Compose Graphics 1](../modules/compose-graphics-1)
                
<!-- END COMMON HEADER -->

## Introduction

We continue our Graph editor application, adding support for
* Blinking like-typed shapes when clicked
* Dragging shapes
* Drawing lines between objects (that stay connected when the shapes are moved)

> Note that I walk through several intentional errors during these videos and explain why they occur/how to fix them. The user tap/drag interaction can be trickier to get right than it seems, so I wanted to highlight common problems and show that sometimes an "obvious" design may not be correct...

## Objectives

* Interact with a custom Composable via taps and drags
* Understand how to successfully pass data and functions into a `pointerInput` block
* Think about how immutable data can sometimes cause non-obvious problems

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:57:28

            
#### Jetpack Compose Graphics: Blinking and Internal Compiler Errors (Fall 2021) (52:55)
 | [![Jetpack Compose Graphics: Blinking and Internal Compiler Errors (Fall 2021)](https://img.youtube.com/vi/4UZt1KOd8ow/mqdefault.jpg)](https://youtu.be/4UZt1KOd8ow) |
 | -- |

                

#### Jetpack Compose Graphics: Dragging and Connecting (Fall 2021) (1:04:33)
 | [![Jetpack Compose Graphics: Dragging and Connecting (Fall 2021)](https://img.youtube.com/vi/SqscYM8G8KY/mqdefault.jpg)](https://youtu.be/SqscYM8G8KY) |
 | -- |

                

            