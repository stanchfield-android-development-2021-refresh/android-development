# Module 3.1 Jetpack Compose Basics

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 2.2: Android Databases Using Room](../modules/room)
                
<!-- END COMMON HEADER -->

## Introduction

Jetpack Compose is an awesome new user-interface framework for Android, making it easy to create reactive user interfaces and separate your user-interface logic from business logic.

In this module, we'll look at the concepts behind Compose, some basic examples, and start creating our Movie application's user interface

## Objectives

* Understand the concepts behind Jetpack Compose
* Be able to create a simple, realistic user interface for a data-management application

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 2:51:56

            
#### Lecture: Compose Basics (Fall 2021) (26:16)
 | [![Lecture: Compose Basics (Fall 2021)](https://img.youtube.com/vi/DX9h4EdYtEs/mqdefault.jpg)](https://youtu.be/DX9h4EdYtEs) |
 | -- |

                

#### Example: Compose Basics (Fall 2021) (52:41)
 | [![Example: Compose Basics (Fall 2021)](https://img.youtube.com/vi/0l_MsqyGtA8/mqdefault.jpg)](https://youtu.be/0l_MsqyGtA8) |
 | -- |

                

#### Example: Movies App, Part 1 (Fall 2021) (1:32:59)
 | [![Example: Movies App, Part 1 (Fall 2021)](https://img.youtube.com/vi/qp0kprMzosE/mqdefault.jpg)](https://youtu.be/qp0kprMzosE) |
 | -- |

                

            