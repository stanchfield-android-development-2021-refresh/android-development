package com.javadude.movies1

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.javadude.movies1.data.Actor
import com.javadude.movies1.data.Movie

// movie selection
//   tap to open it
//   tap icon or long press to start multi-selection
//      further taps add to/remove from selection

@ExperimentalMaterialApi
@Composable
fun MovieList(
    movies: List<Movie>,
    onClick: (Movie) -> Unit,
    onAddMovie: () -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                        Text(stringResource(R.string.movies))
                },
                actions = {
                    IconButton(onClick = {
                        onAddMovie()
                    }) {
                        Icon(
                            imageVector = Icons.Filled.Add,
                            contentDescription = stringResource(R.string.add_movie)
                        )
                    }
                }
            )
        },
        content = {
            LazyColumn {
                items(
                    items = movies,
                    key = { it.id }
                ) { movie ->
                    Card(
                        elevation = 2.dp,
                        backgroundColor = MaterialTheme.colors.surface,
                        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
                        onClick = {
                            onClick(movie)
                        },
                        modifier = Modifier
                            .padding(8.dp)
                            .fillMaxWidth()
                    ) {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Image(
                                painter = painterResource(id = R.drawable.ic_baseline_movie_24),
                                colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                                contentDescription = stringResource(R.string.movie_icon),
                                modifier = Modifier
                                    .padding(8.dp)
                                    .size(48.dp)
//                           .clickable {
//                               TODO("deal with clicking icon")
//                           }
                            )
                            Text(
                                text = movie.title,
                                color = MaterialTheme.colors.onSurface
                            )
                        }
                    }
                }
            }
        }
    )
}

@Composable
fun MovieDisplay(
    movieId: String,
    onEditMovie: (String) -> Unit,
    getMovie: suspend (String) -> Movie
) {
    var movie by remember { mutableStateOf<Movie?>(null) }
    val title by remember(movie) { mutableStateOf(movie?.title ?: "(Movie Loading)") }

    LaunchedEffect(key1 = movieId) {
        movie = getMovie(movieId)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = title)
                },
                actions = {
                    IconButton(onClick = {
                        onEditMovie(movieId)
                    }) {
                        Icon(
                            imageVector = Icons.Filled.Edit,
                            contentDescription = stringResource(R.string.edit_movie)
                        )
                    }
                }
            )
        },
        content = {
            movie?.let {
                Column(
                    modifier = Modifier.verticalScroll(rememberScrollState())
                ) {
                    Label(text = stringResource(R.string.title))
                    Display(text = it.title)
                    Label(text = stringResource(R.string.description))
                    Display(text = it.description)
                }
            } ?: run {
                Text(stringResource(R.string.loading))
            }
        }
    )
}

@Composable
fun MovieEdit(
    movieId: String,
    onMovieChange: (Movie) -> Unit,
    getMovie: suspend (String) -> Movie
) {
    var movie by remember { mutableStateOf<Movie?>(null) }
    var title by remember(movie) { mutableStateOf(movie?.title ?: "") }
    var description by remember(movie) { mutableStateOf(movie?.description ?: "") }

    LaunchedEffect(key1 = movieId) {
        movie = getMovie(movieId)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    val toolbarTitle = title.notBlankOr { stringResource(R.string.no_title) }
                    Text(text = toolbarTitle)
                }
            )
        },
        content = {
            Column(
                modifier = Modifier.verticalScroll(rememberScrollState())
            ) {
                TextField(
                    value = title,
                    label = { Text(text = stringResource(id = R.string.title))},
                    onValueChange = {
                        title = it
                        onMovieChange(Movie(id = movieId, title = it, description = description))
                    },
                    modifier = Modifier.padding(8.dp)
                )
                TextField(
                    value = description,
                    label = { Text(text = stringResource(id = R.string.description))},
                    onValueChange = {
                        description = it
                        onMovieChange(Movie(id = movieId, title = title, description = it))
                    },
                    modifier = Modifier.padding(8.dp)
                )
            }
        }
    )
}

@Composable
fun String.notBlankOr(alternate: @Composable () -> String) =
    if (isNotBlank()) this else alternate()

@Composable
fun ActorList(
   actors: List<Actor>
) {

}

@Composable
fun ActorEdit(
   actor: Actor
) {

}

@Composable
fun ActorDisplay(
   actor: Actor
) {

}