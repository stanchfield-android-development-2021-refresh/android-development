package com.javadude.movies1

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import com.javadude.movies1.data.Movie
import com.javadude.movies1.ui.theme.Movies1Theme
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
// NOTE - DO NOT INCLUDE THIS IN YOUR SUBMISSION
//        IT MAY BE HELPFUL FOR SOME TESTING, THOUGH
//            LaunchedEffect(true) {
//                viewModel.resetDatabase()
//            }
            Movies1Theme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Ui(viewModel) {
                        finish()
                    }
                }
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit
) {
    // The Ui level is the only one to touch the view model directly
    // It pulls data from the view model and manages navigation (push/pop stack)
    // All lower levels only act on data/functions passed to them
    BackHandler {
        viewModel.pop()
    }
    val movies by viewModel.movies.collectAsState(initial = emptyList())
    val actors by viewModel.actors.collectAsState(initial = emptyList())
    val currentScreen by viewModel.currentScreen.collectAsState(initial = MovieListScreen)
    val scope = rememberCoroutineScope()

    when(val screen = currentScreen) {
        MovieListScreen ->
            MovieList(
                movies = movies,
                onClick = {
                    viewModel.push(MovieDisplayScreen(it.id))
                },
                onAddMovie = {
                    scope.launch {
                        val movie = viewModel.createMovie()
                        viewModel.push(MovieEditScreen(movie.id))
                    }
                }
            )
        is MovieDisplayScreen ->
            MovieDisplay(
                movieId = screen.movieId,
                onEditMovie = {
                    viewModel.push(MovieEditScreen(screen.movieId))
                },
                getMovie = { id -> viewModel.getMovie(id) }
            )
        is MovieEditScreen ->
            MovieEdit(
                movieId = screen.movieId,
                onMovieChange = { movie ->
                    scope.launch {
                        viewModel.update(movie)
                    }
                },
                getMovie = { id -> viewModel.getMovie(id) }
            )
        ActorListScreen ->
            ActorList(actors = actors)
        is ActorDisplayScreen -> Text(text = "")
//            ActorDisplay(
//                actor = screen.actor
//            )
        is ActorEditScreen -> Text(text = "")
//            ActorEdit(
//                actor = screen.actor
//            )
        null -> onExit()
    }
}

