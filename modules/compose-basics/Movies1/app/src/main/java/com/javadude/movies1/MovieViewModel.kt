package com.javadude.movies1

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.javadude.movies1.data.Actor
import com.javadude.movies1.data.ExpandedRole
import com.javadude.movies1.data.Movie
import com.javadude.movies1.data.MovieDatabaseRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow

sealed interface Screen
object MovieListScreen: Screen
class MovieDisplayScreen(val movieId: String): Screen
class MovieEditScreen(val movieId: String): Screen
object ActorListScreen: Screen
class ActorDisplayScreen(val actorId: String): Screen
class ActorEditScreen(val actorId: String): Screen

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = MovieDatabaseRepository(application)
    private val currentScreen_ = MutableStateFlow<Screen?>(MovieListScreen)
    val currentScreen: Flow<Screen?> = currentScreen_

    private var backStack = listOf<Screen>(MovieListScreen)
        set(value) {
            field = value
            currentScreen_.value = value.lastOrNull()
        }

    val movies = repository.movies
    val actors = repository.actors

    private val selectedMovie = MutableSharedFlow<Movie?>()
    @ExperimentalCoroutinesApi
    val cast = selectedMovie.flatMapLatest {
        it?.let { movie ->
            repository.getCast(movie.id)
        } ?: flow {
            emit(emptyList<ExpandedRole>())
        }
    }

    suspend fun selectMovie(id: String) = selectedMovie.emit(getMovie(id))
    suspend fun getMovie(id: String) = repository.getMovie(id)
    suspend fun getActor(id: String) = repository.getActor(id)
    suspend fun update(movie: Movie) = repository.update(movie)
    suspend fun deleteMovie(id: String) = repository.delete(repository.getMovie(id))
    suspend fun deleteActor(id: String) = repository.delete(repository.getActor(id))
    suspend fun resetDatabase() = repository.resetDatabase()
    suspend fun createMovie() = repository.createMovie()

    // stack maipulation
    fun push(screen: Screen) {
        backStack = backStack + screen
    }
    fun pop() {
        backStack = backStack.dropLast(1)
    }
}