# Module 5.1 Jetpack Compose Layouts

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 2.2: Android Databases Using Room](../modules/room)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
* [Module 4.1: Jetpack Compose Lists](../modules/compose-lists)
                
<!-- END COMMON HEADER -->

## Introduction

Arranging your Composables can be tricky to get right. Jetpack Compose's included layouts give you the basic support you need for most layouts, but you can gain more control and simpler specification with a custom layout. We'll also see how a "slotted" layout allows you to define a common overall structure where you can plug in each section of the layout.

In this module, we'll explore different ways to create a simple "Forms" interface, starting with basic Rows and Columns, trying out ConstraintLayout, then creating our own layout manager that will adapt to show a form differently based on available space.

## Objectives

* Choose an existing layout when it fits the need
* Avoid ConstraintLayout almost always
* Create custom layouts when something more complex is needed.
* Think "slots" when a common overall layout structure should be applied

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 2:17:34

            
#### Example: Basic and Custom Layouts (Fall 2021) (1:11:20)
 | [![Example: Basic and Custom Layouts (Fall 2021)](https://img.youtube.com/vi/V-rrPANU264/mqdefault.jpg)](https://youtu.be/V-rrPANU264) |
 | -- |

                

#### Lecture: Slot APIs (Fall 2021) (16:47)
 | [![Lecture: Slot APIs (Fall 2021)](https://img.youtube.com/vi/3XsCVqcdzVA/mqdefault.jpg)](https://youtu.be/3XsCVqcdzVA) |
 | -- |

                

#### Example: Slot APIs (Fall 2021) (10:27)
 | [![Example: Slot APIs (Fall 2021)](https://img.youtube.com/vi/uQIUpHP50Tc/mqdefault.jpg)](https://youtu.be/uQIUpHP50Tc) |
 | -- |

                

#### (Bonus, Not Required) More detail on Template Method, Strategy and Null Object Patterns (Fall 2021) (39:00)
 | [![(Bonus, Not Required) More detail on Template Method, Strategy and Null Object Patterns (Fall 2021)](https://img.youtube.com/vi/EWBCy0jWFvc/mqdefault.jpg)](https://youtu.be/EWBCy0jWFvc) |
 | -- |

                

            