package stanchfield.scott.files

import android.Manifest
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import stanchfield.scott.files.ui.UppercaseWriter
import stanchfield.scott.files.ui.theme.FilesTheme
import java.io.File
import java.io.InputStream
import java.io.InputStreamReader
import java.io.PrintWriter

// see https://developer.android.com/training/data-storage

// image Android1.png: <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
// image Android2.png:  <div>Icons made by <a href="https://icon54.com/" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
class MainActivity : ComponentActivity() {
    private var text by mutableStateOf("")
    private var imageUri by mutableStateOf<Uri?>(null)
    private var imageBitmap by mutableStateOf<ImageBitmap?>(null)

    private var treeUri: String?
        get() = PreferenceManager.getDefaultSharedPreferences(application).getString("tree_uri", null)
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(application).edit {
                putString("tree_uri", value)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val scope = rememberCoroutineScope()

            @Composable
            fun MyButton(
                text: String,
                onClick: suspend () -> Unit
            ) = Button(
                onClick = {
                    scope.launch {
                        onClick()
                    }
                },
                modifier = Modifier.padding(8.dp)
            ) {
                Text(text = text)
            }

            FilesTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Column(modifier = Modifier.fillMaxSize()) {
                        Row(modifier = Modifier.fillMaxWidth()) {
                            MyButton(text = "Write Files", onClick = ::writeFiles)
                            MyButton(text = "Read Files", onClick = ::readFiles)
                        }
                        Row(modifier = Modifier.fillMaxWidth()) {
                            MyButton(text = "Store Raw") {
                                copyRawToMediaStore()
                                dumpImageFiles()
                            }
                            MyButton(text = "Store Asset") {
                                copyAssetToMediaStore()
                                dumpImageFiles()
                            }
                            MyButton(text = "Dump Images", onClick = ::dumpImageFiles)
                        }
                        Row {
                            MyButton(text = "Android1.png") {
                                imageUri = findImage("Android1.png")
                            }
                            MyButton(text = "Android2.png") {
                                imageUri = findImage("Android2.png")
                            }
                        }
                        Row {
                            MyButton(text = "SAF Write", onClick = ::writeSAFFile)
                            MyButton(text = "SAF Read", onClick = ::readSAFFile)
                            MyButton(text = "SAF Tree", onClick = ::writeViaTree)
                        }
                        Text(
                            text = text,
                            modifier = Modifier
                                .fillMaxWidth()
                                .weight(1f)
                                .padding(8.dp)
                                .border(width = 2.dp, color = Color.Black, shape = RoundedCornerShape(8.dp))
                                .padding(8.dp)
                                .verticalScroll(rememberScrollState())
                        )
                        imageUri?.let { uri ->
                            LaunchedEffect(imageUri) {
                                checkPermissionAndOpenImage(uri)
                            }
                        }
                        imageBitmap?.let { bitmap ->
                            Image(bitmap = bitmap, contentDescription = "image")
                        }
                    }
                }
            }
        }
    }

    private suspend fun findImage(name: String): Uri? = withContext(Dispatchers.IO) {
        val collection =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
            } else {
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            }

        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DISPLAY_NAME
        )

        val selection = "${MediaStore.Images.Media.DISPLAY_NAME} = ?"
        val selectionArgs = arrayOf(name)

        val returnedCursor = contentResolver.query(collection, projection, selection, selectionArgs, null)
        returnedCursor?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
            if (cursor.moveToNext()) {
                val id = cursor.getLong(idColumn)
                ContentUris.withAppendedId(collection, id)
            } else {
                null
            }
        }
    }

    private suspend fun dumpImageFiles() {
        text = listImageFiles().joinToString("\n") { "${it.uri}\n\t${it.name}: ${it.size}" }
    }

    data class ImageInfo(val uri: Uri, val name: String, val size: Int)

    private suspend fun listImageFiles(): List<ImageInfo> = withContext(Dispatchers.IO) {
        // from https://developer.android.com/training/data-storage/shared/media
        // Need the READ_EXTERNAL_STORAGE permission if accessing images files that your app didn't create.

        val imagesList = mutableListOf<ImageInfo>()

        val collection =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
            } else {
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            }

        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        )

        val sortOrder = "${MediaStore.Images.Media.DISPLAY_NAME} ASC"

        contentResolver.query(collection, projection, null, null, sortOrder)?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
            val nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
            val sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE)

            while (cursor.moveToNext()) {
                val id = cursor.getLong(idColumn)
                val name = cursor.getString(nameColumn)
                val size = cursor.getInt(sizeColumn)

                val contentUri: Uri = ContentUris.withAppendedId(collection, id)
                imagesList += ImageInfo(contentUri, name, size)
            }
        } ?: throw RuntimeException("Could not get cursor")

        imagesList
    }



    private suspend fun readFiles() = withContext(Dispatchers.IO) {
        val subdir = File(filesDir, "mystuff")
        val file1 = File(subdir, "sample1.txt")
        text = "sample1.txt\n${file1.readText()}"

        delay(1500)

        val file2 = File(subdir, "sample2.txt")
        val stringBuilder = StringBuilder()
        file2.reader().use { reader ->
            val buffer = CharArray(1024)
            var count = 1024
            @Suppress("BlockingMethodInNonBlockingContext")
            while(count == 1024) {
                count = reader.read(buffer)
                stringBuilder.append(buffer, 0, count)
            }
        }
        text = "sample2.txt\n$stringBuilder"

        delay(1500)

        (3..4).forEach {
            val file = File(subdir, "sample$it.txt")
            stringBuilder.clear()
            file.bufferedReader().use { reader ->
                while (true) {
                    val line = reader.readLine()
                    if (line == null) {
                        break
                    } else {
                        stringBuilder.appendLine(line)
                    }
                }
            }
            text = "sample$it.txt\n$stringBuilder"

            delay(1500)
        }

        getExternalFilesDir(null)?.let { externalDir ->
            val subdir2 = File(externalDir, "externalStuff")
            val file5 = File(subdir2, "sample5.txt")
            text = "sample5.txt\n${file5.readText()}"
        }
    }

    private suspend fun writeFiles() = withContext(Dispatchers.IO) {
        val subdir = File(filesDir, "mystuff")
        subdir.mkdirs()

        val file1 = File(subdir, "sample1.txt")
        file1.writeText("""
            This is a sample file
                Another Line
            Yet Another Line
        """.trimIndent())

        val file2 = File(subdir, "sample2.txt")
        file2.writer().use { writer ->
            (1..10).forEach {
                writer.write(it.toString())
            }
        }

        val file3 = File(subdir, "sample3.txt")
        file3.printWriter().use { writer ->
            (1..10).forEach {
                writer.println(it)
            }
        }

        val file4 = File(subdir, "sample4.txt")
        UppercaseWriter(file4.bufferedWriter()).use { writer ->
            @Suppress("BlockingMethodInNonBlockingContext") // blocking IO is ok in Dispatchers.IO
            writer.write("""
                This is a sample file
                    Another lines
                Yet Another Line
            """.trimIndent())
        }

        getExternalFilesDir(null)?.let { externalDir ->
            val subdir2 = File(externalDir, "externalStuff")
            subdir2.mkdirs()
            val file5 = File(subdir2, "sample5.txt")
            file5.writeText("""
                External file
                    Stuff
                More Stuff
            """.trimIndent())
        } ?: throw RuntimeException("Cannot access application dir on external storage")
    }

    private suspend fun InputStream.copyToMediaStore(fileName: String) = withContext(Dispatchers.IO) {
        val imagesCollection =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
            } else {
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            }
        val newImageDetails = ContentValues().apply {
            put(MediaStore.Images.Media.DISPLAY_NAME, fileName)
            put(MediaStore.Images.Media.MIME_TYPE, "image/png")
        }
        val uri = contentResolver.insert(imagesCollection, newImageDetails) ?: throw RuntimeException("Could not insert data")
        @Suppress("BlockingMethodInNonBlockingContext")
        use { inputStream ->
            Log.d("!!!", uri.toString())
            contentResolver.openOutputStream(uri)?.use { outputStream ->
                inputStream.copyTo(outputStream)
            } ?: throw RuntimeException("Could not open output stream for $uri")
        }
    }

    private suspend fun copyRawToMediaStore() = withContext(Dispatchers.IO) {
        resources.openRawResource(R.raw.android1).use { inputStream ->
            inputStream.copyToMediaStore("Android1.png")
        }
    }

    private suspend fun copyAssetToMediaStore() = withContext(Dispatchers.IO) {
        @Suppress("BlockingMethodInNonBlockingContext")
        assets.open("images/android2.png").use { inputStream ->
            inputStream.copyToMediaStore("Android2.png")
        }
    }


    private suspend fun openImage() = withContext(Dispatchers.IO) {
        @Suppress("BlockingMethodInNonBlockingContext")
        imageUri?.let {
            contentResolver.openInputStream(it).use { stream ->
                imageBitmap = BitmapFactory.decodeStream(stream).asImageBitmap()
            }
        } ?: throw RuntimeException("No imageUriToOpen set")
    }


    private val requestReadPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
        if (isGranted) {
            lifecycleScope.launch {
                openImage()
            }
        } else {
            Toast.makeText(this, "You need read permission", Toast.LENGTH_LONG).show()
        }
    }

    private suspend fun checkPermissionAndOpenImage(uri: Uri) = withContext(Dispatchers.IO) {
        this@MainActivity.imageUri = uri
        if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            openImage()
        } else {
            requestReadPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    private fun writeSAFFile() = writeSAFDocumentLauncher.launch("simple-saf1.txt")
    private fun readSAFFile() = openSAFDocumentLauncher.launch(arrayOf("text/plain"))

    private val writeSAFDocumentLauncher = registerForActivityResult(
        object: ActivityResultContracts.CreateDocument() {
            override fun createIntent(context: Context, input: String): Intent {
                return super.createIntent(context, input).apply {
                    addCategory(Intent.CATEGORY_OPENABLE)
                    type = "text/plain"
                }
            }
        }
    ) { uri ->
        if (uri != null) {
            contentResolver.openOutputStream(uri)?.use { outputStream ->
                PrintWriter(outputStream).use { printWriter ->
                    printWriter.println(
                        """
                        This is a sample SAF file
                    """.trimIndent()
                    )
                }
            }
        }
    }

    private val openSAFDocumentLauncher = registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri ->
        if (uri != null) {
            contentResolver.openInputStream(uri)?.use { inputStream ->
                InputStreamReader(inputStream).use { reader ->
                    text = reader.readText()
                }
            }
        }
    }

    private suspend fun writeViaTree() = treeUri?.toUriOrNullIfNoPermission()?.createFileInTree() ?: openSAFDocumentTreeLauncher.launch(null)

    private fun String.toUriOrNullIfNoPermission() =
        if (contentResolver.persistedUriPermissions.any { it.uri.toString() == this && it.isReadPermission && it.isWritePermission })
            Uri.parse(this)
        else
            null

    // can save tree URI
    private val openSAFDocumentTreeLauncher = registerForActivityResult(ActivityResultContracts.OpenDocumentTree()) { uri ->
        if (uri != null) {
            treeUri = uri.toString()
            val takeFlags: Int =
                Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            contentResolver.takePersistableUriPermission(uri, takeFlags)
            lifecycleScope.launch {
                uri.createFileInTree()
            }
        }
    }

    private suspend fun Uri.createFileInTree() = withContext(Dispatchers.IO) {
        val dir = DocumentFile.fromTreeUri(this@MainActivity, this@createFileInTree) ?: throw RuntimeException("Error loading ${this@createFileInTree}")
        val stuffDir = dir.findFile("safStuff") ?: dir.createDirectory("safStuff") ?: throw RuntimeException("Error creating/finding safStuff dir in $dir")
        val file = stuffDir.createFile("text/plain", "sample-saf2.txt") ?: throw RuntimeException("could not create sample-saf2.txt")
        @Suppress("BlockingMethodInNonBlockingContext")
        contentResolver.openOutputStream(file.uri)?.use { outputStream ->
            PrintWriter(outputStream).use { pw ->
                pw.println("SAF file in directory")
                pw.println("Joy!!!")
            }
        } ?: throw RuntimeException("Could not open output stream for ${file.uri}")
        dumpSAFTree()
    }

    private suspend fun dumpSAFTree() = withContext(Dispatchers.IO) {
        val uri = Uri.parse(requireNotNull(treeUri))
        val dir = DocumentFile.fromTreeUri(this@MainActivity, uri) ?: throw RuntimeException("Error loading $uri")
        val stuffDir = dir.findFile("safStuff") ?: throw RuntimeException("Error finding safStuff dir in $dir")
        text = stuffDir.listFiles().joinToString("\n") { it.name ?: "(no name)" }
    }

}