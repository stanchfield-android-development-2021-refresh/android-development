package com.javadude.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.javadude.service.ui.theme.ServiceTheme

class MainActivity : ComponentActivity() {
    private var binder : RemoteService? = null // api to service
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            binder = null
        }
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            this@MainActivity.binder = RemoteService.Stub.asInterface(binder).apply {
                add(reporter) // start listening to service
            }
        }
    }

    private var progressState by mutableStateOf(0)
    private var peopleState by mutableStateOf("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ServiceTheme {
                Surface(color = MaterialTheme.colors.background) {
                    UI(peopleState, progressState) {
                        binder?.reset()
                    }
                }
            }
        }
    }


    // callback reporter to be passed to remote service
    private val reporter = object : RemoteServiceReporter.Stub() {
        override fun report(people: List<Person>, i: Int) {
            progressState = i
            peopleState = people.joinToString("\n") {
                "${it.name}: ${it.age}"
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // bind to the service
        // NOTE - DO NOT DO THIS IN onStart FOR YOUR ASSIGNMENT!
        //        YOU SHOULD BIND ONLY WHEN THE MAP IS READY!
        val intent = Intent(this, RemoteServiceImpl::class.java)
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        // tell the service to stop notifying us
        binder?.remove(reporter)
        unbindService(serviceConnection)
        super.onStop()
    }
}

@Composable
fun UI(
    people: String,
    progress: Int,
    onReset: () -> Unit
) {
    Column(modifier = Modifier.fillMaxSize()) {
        Button(onClick = onReset, modifier = Modifier.padding(8.dp)) {
            Text(text = stringResource(id = R.string.reset))
        }
        LinearProgressIndicator(progress = progress.toFloat()/100, modifier = Modifier.padding(8.dp).fillMaxWidth())
        Text(text = people, modifier = Modifier.padding(8.dp))
    }
}

