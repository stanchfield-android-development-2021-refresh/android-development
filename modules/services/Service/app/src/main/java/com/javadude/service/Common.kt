package com.javadude.service

import android.content.Intent
import android.content.IntentFilter

// constants
const val NOTIFICATION_CHANNEL_NAME = "com.javadude.music.player"
const val NOTIFICATION_ID = 42
const val EXTRA_ACTION = "action"
const val EXTRA_SONG = "song"
const val EXTRA_TITLE = "title"

// pending intent action ids
const val OPEN_ACTIVITY = 1
const val PLAY = 2
const val STOP = 3
const val PAUSE = 4

// Intent actions to be sent as broadcasts
const val ACTION_PLAY = "com.javadude.music.play"
const val ACTION_PAUSE = "com.javadude.music.pause"
const val ACTION_STOP = "com.javadude.music.stop"

// convert from the action code to action strings
fun Int.toAction() =
    when (this) {
        PLAY -> ACTION_PLAY
        PAUSE -> ACTION_PAUSE
        STOP -> ACTION_STOP
        else -> throw IllegalArgumentException()
    }

// intent filter for catching the play/pause/stop intents
val receiverIntentFilter =
    IntentFilter().apply {
        addCategory(Intent.CATEGORY_DEFAULT)
        addAction(ACTION_PLAY)
        addAction(ACTION_PAUSE)
        addAction(ACTION_STOP)
    }
