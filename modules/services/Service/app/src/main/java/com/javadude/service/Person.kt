package com.javadude.service

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

// Sample Parcelable Person
// the commented-out code is left here to demonstrate what the @Parcelize
//   annotation will generate
@Parcelize data class Person(
    val name : String,
    val age : Int
) : Parcelable
//{
//    companion object {
//        @JvmField
//        val CREATOR = object : Parcelable.Creator<Person> {
//            override fun createFromParcel(parcel: Parcel) =
//                Person(
//                    parcel.readString() ?: throw IllegalStateException(),
//                    parcel.readInt()
//                )
//
//            override fun newArray(size: Int) = Array<Person?>(size) { null }
//        }
//    }
//    override fun writeToParcel(parcel: Parcel, flags: Int) {
//        parcel.writeString(name)
//        parcel.writeInt(age)
//    }
//
//    override fun describeContents() = 0
//
//}
