package com.javadude.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat

// implementation of a started foreground service

sealed interface State
object Stopped: State
object Paused: State
object Playing: State

// NOTE: A good overview of MediaPlayer is at
//          https://blog.mindorks.com/using-mediaplayer-to-play-an-audio-file-in-android
// NOTE: A much more detailed media service setup is described at
//         https://developer.android.com/guide/topics/media-apps/audio-app/building-a-mediabrowserservice
//       The goal of this example is to demonstrate basic use of a service with controls on a notification

class RemoteMusicService : Service() {
    private var state: State = Stopped
    private var mediaPlayer: MediaPlayer? = null
    private var currentTitle: String = ""
    private var currentSong: Int = -1

    // receiver that listens for the notification's play/pause/stop broadcasts
    //   and runs the requested action
    private val broadcastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            handleIntent(intent)
        }
    }

    private fun MediaPlayer.cleanup() {
        if (isPlaying) {
            stop()
        }
        reset()
        release()
    }

    override fun onCreate() {
        super.onCreate()

        // register channel (required if at least Oreo)
        // Notification channels allow the use to adjust notification settings for
        //    groups of notifications
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(
                NotificationChannel(
                    NOTIFICATION_CHANNEL_NAME,
                    getString(R.string.javadude_music_player),
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            )
        }

        // listen for broadcasts from the notification play/pause/stop buttons
        registerReceiver(broadcastReceiver, receiverIntentFilter)
    }

    // when we receive an intent (which may start this service), perform the action
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        handleIntent(intent)
        return START_REDELIVER_INTENT // if process killed, redeliver the last intent
    }

    // perform the requested action, whether from onStartCommand or the broadcast receiver
    // (needs some tweaks to properly handle if the process is killed and intent re-delivered -
    //   the current title and song won't be preserved right now - should store in properties
    //   and the state management below could be off - for example, if the last intent was "pause"
    //   and we were re-created with start state stopped... left as an exercise for the interested
    //   reader, muihahahahaha!)
    private fun handleIntent(intent: Intent) {
        // if there is no action, it was sent from the activity calling startService;
        //   convert the action code to an action string
        when (intent.action ?: intent.getIntExtra(EXTRA_ACTION, PLAY).toAction()) {
            ACTION_PLAY -> {
                when (state) {
                    Paused -> {
                        // continue current song
                        mediaPlayer?.run {
                            seekTo(currentPosition)
                            start()
                        }
                    }
                    Playing -> { } // already playing; ignore
                    Stopped -> {
                        // start playing a new song (get data from the intent)
                        currentTitle = intent.getStringExtra(EXTRA_TITLE) ?: "No title provided"
                        currentSong = intent.getIntExtra(EXTRA_SONG, -1)
                        if (currentSong == -1) {
                            throw IllegalArgumentException()
                        }
                        mediaPlayer = MediaPlayer.create(this@RemoteMusicService, currentSong).apply { start() }
                    }
                }
                state = Playing
            }
            ACTION_PAUSE -> {
                mediaPlayer?.pause()
                state = Paused
            }
            ACTION_STOP -> {
                // if stopping, kill the service (which will remove the notification)
                state = Stopped
                stopSelf()
            }
        }

        // always send/update a foreground service notification
        updateNotification(currentTitle)
    }

    // set up the intent flags based on android version.
    // as of Marshmallow, we can set immutability (whether other apps can mod the intent)
    // as of Oreo, (im)mutability setting is required
    private fun intentFlags() =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        } else {
            PendingIntent.FLAG_UPDATE_CURRENT
        }

    // helper function to set up an action if a predicate is true
    private fun NotificationCompat.Builder.addActionIf(
        predicate: Boolean,
        actionId: Int,
        @DrawableRes icon: Int,
        @StringRes title: Int
    ) = apply {
        if (predicate) {
            addAction(
                icon,
                getString(title),
                // set up the action to send a broadcast
                PendingIntent.getBroadcast(
                    this@RemoteMusicService, actionId,
                    Intent(actionId.toAction())
                        .setPackage("com.javadude.service") // only send to this app
                        .putExtra(EXTRA_SONG, currentSong)
                        .putExtra(EXTRA_TITLE, currentTitle),
                    intentFlags()
                )
            )
        }
    }

    // set up a foreground-service notification that has music controls
    private fun updateNotification(title: String) {
        val openApp = PendingIntent.getActivity(this, OPEN_ACTIVITY,
            Intent(this, MusicActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            , intentFlags())

        val notification =
            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_NAME)
                .setContentTitle(getString(R.string.music_player))
                .setContentIntent(openApp)
                .setSmallIcon(R.drawable.ic_baseline_music_note_24)
                .setSilent(true)
                .addActionIf(state == Paused || state == Stopped, PLAY,
                    R.drawable.ic_baseline_play_arrow_24,
                    R.string.play
                )
                .addActionIf(state == Playing, PAUSE, R.drawable.ic_baseline_pause_24, R.string.pause)
                .addActionIf(state == Paused || state == Playing, STOP, R.drawable.ic_baseline_stop_24, R.string.stop)
                .setContentText(getString(R.string.song, title))
                .setStyle(
                    when (state) {
                        Playing, Paused ->
                            androidx.media.app.NotificationCompat.MediaStyle()
                                .setShowActionsInCompactView(0, 1)
                        Stopped ->
                            androidx.media.app.NotificationCompat.MediaStyle()
                                .setShowActionsInCompactView(0)
                    }
                )
                .build()

        startForeground(NOTIFICATION_ID, notification)
    }

    override fun onDestroy() {
        unregisterReceiver(broadcastReceiver)
        mediaPlayer?.cleanup()
        super.onDestroy()
    }

    // not a bound service; don't return a binder
    override fun onBind(intent: Intent?): IBinder? = null
}