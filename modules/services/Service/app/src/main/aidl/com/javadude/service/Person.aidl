package com.javadude.service;

// AIDL that declares (to the AIDL compiler) that we have a class named
//    com.javadude.services2.Person that's Parcelable
parcelable Person;