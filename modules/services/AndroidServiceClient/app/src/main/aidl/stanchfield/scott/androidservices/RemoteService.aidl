package stanchfield.scott.androidservices;
import stanchfield.scott.androidservices.RemoteServiceReporter;

// AIDL that defines our API with the remote service
// Here we allow a "reset" request as well as adding/removing a callback
interface RemoteService {
	void reset();
	void add(RemoteServiceReporter reporter);
	void remove(RemoteServiceReporter reporter);
}