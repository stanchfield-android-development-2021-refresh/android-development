# Module 10.2 Android Services

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

You're creating a music player and would like the music to continue even if the user is looking at a different application.

Or your application manages data or an external sensor that other applications need to access.

Android Services can help!

In this module, we'll create Android services that bridge the gap between applications and can keep running even if the current application is not visible.

The sample applications included in this directory are:

* Service - general service examples - all in same application
* AndroidService - separate application only hosting a remote service
* AndroidClient - separate application that talks with the remote service

> Note: Music in example downloaded from
>    https://www.free-stock-music.com/alexander-nakarada-one-bard-band.html
>
> LICENSE
>
> One Bard Band by Alexander Nakarada | https://www.serpentsoundstudios.com
> 
> Music promoted by https://www.free-stock-music.com
> 
> Attribution 4.0 International (CC BY 4.0): https://creativecommons.org/licenses/by/4.0/


## Objectives

* Understand the difference between services and coroutines/threads
* Create applications that expose an API for other applications to consume
* Create long-running services that are independent of attached activities

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:08:48

            
#### Services: Lecture (Fall 2021) (28:14)
 | [![Services: Lecture (Fall 2021)](https://img.youtube.com/vi/0mn-AJ-fcmw/mqdefault.jpg)](https://youtu.be/0mn-AJ-fcmw) |
 | -- |

                

#### Services: Example (Fall 2021) (35:42)
 | [![Services: Example (Fall 2021)](https://img.youtube.com/vi/4br0VOPIzu8/mqdefault.jpg)](https://youtu.be/4br0VOPIzu8) |
 | -- |

                

#### Services: Remote Example (Fall 2021) (04:52)
 | [![Services: Remote Example (Fall 2021)](https://img.youtube.com/vi/w_Bxk6cBAdE/mqdefault.jpg)](https://youtu.be/w_Bxk6cBAdE) |
 | -- |

                

            