package com.javadude.speech

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.lifecycle.lifecycleScope
import com.javadude.speech.language.Game
import com.javadude.speech.ui.theme.SpeechTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Locale

private const val UTTERANCE_ID = "command"

class MainActivity3 : ComponentActivity() {
    private lateinit var game: Game
    private lateinit var textToSpeech: TextToSpeech
    private var text by mutableStateOf("")
    private var status by mutableStateOf("")

    private var speechRecognizer: SpeechRecognizer? = null
    private val speechRecognizerIntent =
        Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        textToSpeech = TextToSpeech(this) {
            textToSpeech.language = Locale.US
            textToSpeech.setOnUtteranceProgressListener(
                object : UtteranceProgressListener() {
                    override fun onStart(utteranceId: String) {
                        status = getString(R.string.shush)
                    }
                    override fun onDone(utteranceId: String) {
                        status = ""
                        tryToStartSpeechRecognition()
                    }
                    override fun onError(utteranceId: String) {
                        status = getString(R.string.error_speaking)
                    }
                }
            )

            game = Game(
                onQuit = { finish() },
                reporter = { message ->
                    text = "${game.getLocationInfo()}\n\n$message"
                    textToSpeech.speak(message, TextToSpeech.QUEUE_ADD, null, UTTERANCE_ID)
                }
            )
            setContent {
                SpeechTheme {
                    Surface(color = MaterialTheme.colors.background) {
                        Ui(status, text)
                    }
                }
            }
            game.look()
        }
    }

    private fun tryToStartSpeechRecognition() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            runOnUiThread {
                AlertDialog.Builder(this)
                    .setMessage("This application uses voice commands. Please allow audio recording on the next popup to use it")
                    .setPositiveButton("Ok") {_,_ -> getRecordingPermission.launch(Manifest.permission.RECORD_AUDIO)}
                    .setNegativeButton("Cancel") {_,_ -> finish()}
                    .show()
            }
        } else {
            startListening()
        }
    }

    private val getRecordingPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
        if (isGranted) {
            startListening()
        } else {
            Toast.makeText(this, "We need recording permission to use auto-voice recognition in this app.", Toast.LENGTH_LONG).show()
        }
    }

    private fun startListening() {
        lifecycleScope.launch(Dispatchers.Main) {
            if (speechRecognizer == null) {
                if (!SpeechRecognizer.isRecognitionAvailable(this@MainActivity3)) {
                    Toast.makeText(this@MainActivity3, "Speech is not available", Toast.LENGTH_SHORT).show()
                } else {
                    speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this@MainActivity3).apply {
                        setRecognitionListener(
                            object : NullRecognitionListener() {
                                override fun onReadyForSpeech(params: Bundle) {
                                    status = "Please speak a command"
                                }

                                override fun onResults(results: Bundle) {
                                    val resultArray = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                                    val command = resultArray!![0]
                                    game.parse(command)
                                }

                                override fun onError(error: Int) {
                                    status = "Recognition error: $error"
                                    // NOTE: there have been many issues reported with the speech recognizer,
                                    //       most often giving error 7 or 8. General advice has been
                                    //       to stop and restart recognition when this happens
                                    //       There are a few libraries that try to make all of this simpler.
                                    //       For example: https://github.com/vikramezhil/DroidSpeech2.0
                                    //       I just wanted to show the basics.
                                    if (error == 8 || error == 7) {
                                        lifecycleScope.launch(Dispatchers.Main) {
                                            speechRecognizer?.stopListening()
                                            speechRecognizer?.startListening(speechRecognizerIntent)
                                        }
                                    }
                                }
                            }
                        )
                    }
                }
            }
            speechRecognizer?.startListening(speechRecognizerIntent)
        }
    }

    override fun onPause() {
        speechRecognizer?.stopListening()
        speechRecognizer?.cancel()
        super.onPause()
    }
}

@Composable
fun Ui(
    status: String,
    text: String,
) {
    Scaffold(
        topBar = { TopAppBar(title = { Text("Auto Speech Adventure!") }) },
        content = {
            Column(modifier = Modifier.padding(8.dp)) {
                MyText(text = status)
                MyText(text = text)
            }
        }
    )
}
