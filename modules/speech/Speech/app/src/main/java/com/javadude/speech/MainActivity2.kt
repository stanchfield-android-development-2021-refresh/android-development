package com.javadude.speech

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContract
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mic
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import com.javadude.speech.language.Game
import com.javadude.speech.ui.theme.SpeechTheme
import java.util.Locale

private const val UTTERANCE_ID = "command"

@ExperimentalComposeUiApi
class MainActivity2 : ComponentActivity() {
    private lateinit var game: Game
    private lateinit var textToSpeech: TextToSpeech
    private var text by mutableStateOf("")
    private var status by mutableStateOf("")

    private val recognizeSpeechViaIntent = registerForActivityResult(GetSpeech) {
        when(it) {
            is Command -> game.parse(it.text)
            is SpeechError -> Toast.makeText(this, "Error running speech", Toast.LENGTH_SHORT).show()
            SpeechCanceled -> status = "Speech canceled"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        textToSpeech = TextToSpeech(this) {
            textToSpeech.language = Locale.US
            textToSpeech.setOnUtteranceProgressListener(
                object : UtteranceProgressListener() {
                    override fun onStart(utteranceId: String) {
                        status = getString(R.string.shush)
                    }
                    override fun onDone(utteranceId: String) {
                        status = ""
                    }
                    override fun onError(utteranceId: String) {
                        status = getString(R.string.error_speaking)
                    }
                }
            )

            game = Game(
                onQuit = { finish() },
                reporter = { message ->
                    text = "${game.getLocationInfo()}\n\n$message"
                    textToSpeech.speak(message, TextToSpeech.QUEUE_ADD, null, UTTERANCE_ID)
                }
            )
            setContent {
                SpeechTheme {
                    Surface(color = MaterialTheme.colors.background) {
                        Ui(status, text,
                            onMicrophoneTap = {
                                recognizeSpeechViaIntent.launch(true)
                            },
                            onCommand = { command ->
                                game.parse(command)
                            },
                        )
                    }
                }
            }
            game.look()
        }

    }
}

@ExperimentalComposeUiApi
@Composable
fun Ui(
    status: String,
    text: String,
    onMicrophoneTap: () -> Unit,
    onCommand: (String) -> Unit
) {
    var command by remember { mutableStateOf("") }
    Scaffold(
        topBar = { TopAppBar(title = { Text("Speech Adventure!") }) },
        content = {
            Column(modifier = Modifier.padding(8.dp)) {
                OutlinedTextField(
                    label = { Text(stringResource(id = R.string.enter_command)) },
                    value = command,
                    trailingIcon = {
                        Icon(
                            imageVector = Icons.Filled.Mic,
                            contentDescription = stringResource(id = R.string.record_button),
                            modifier = Modifier
                                .padding(8.dp)
                                .height(48.dp)
                                .width(48.dp)
                                .clickable { onMicrophoneTap() }
                        )
                    },
                    textStyle = MaterialTheme.typography.h4,
                    keyboardActions = KeyboardActions {
                        onCommand(command)
                        command = ""
                    },
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Go,
                    ),
                    onValueChange = { command = it },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                        // as of Compose v1.1, the following onKeyEvent() call is no longer needed
                        .onKeyEvent {
                            if (it.key == Key.Enter) {
                                onCommand(command.trim())
                                command = ""
                                true
                            } else {
                                false
                            }
                        }
                )
                MyText(text = status)
                MyText(text = text)
            }
        }
    )
}


// support for intent-based speech recognition
// does not require RECORD_AUDIO permission, but feels more invasive
sealed interface SpeechResult
object SpeechCanceled: SpeechResult
object SpeechError: SpeechResult
data class Command(val text: String): SpeechResult

object GetSpeech : ActivityResultContract<Boolean, SpeechResult>() {
    override fun createIntent(context: Context, input: Boolean) =
        Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            putExtra(RecognizerIntent.EXTRA_PROMPT, "Tell me what you want to do")
        }

    override fun parseResult(resultCode: Int, intent: Intent?) =
        when(resultCode) {
            Activity.RESULT_OK -> {
                val results = intent?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                Command(results!![0].lowercase())
            }
            Activity.RESULT_CANCELED -> SpeechCanceled
            else -> SpeechError
        }
}
