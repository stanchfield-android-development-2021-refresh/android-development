package com.javadude.speech

import com.javadude.speech.language.CommandException
import com.javadude.speech.language.CommandsLexer
import com.javadude.speech.language.CommandsParser
import com.javadude.speech.language.Game
import org.antlr.v4.runtime.ANTLRErrorListener
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.Parser
import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.Recognizer
import org.antlr.v4.runtime.atn.ATNConfigSet
import org.antlr.v4.runtime.dfa.DFA
import java.util.BitSet

fun Game.parse(command: String) {
    val inputStream = CharStreams.fromString(command.lowercase())
    val lexer = CommandsLexer(inputStream)
    val tokenStream = CommonTokenStream(lexer)
    val parser = CommandsParser(tokenStream)
    try {
        lexer.addErrorListener(antlrErrorListener)
        parser.addErrorListener(antlrErrorListener)
        this.command = command
        parser.command(this)
    } catch (e: CommandException) {
        report(e.message ?: "Command $command not recognized; try again")
    } catch (e: Throwable) {
        report("Command $command not recognized; try again")
    }
}

// normally the error listener would be better fleshed out, but this works for this simple example
private val antlrErrorListener = object: ANTLRErrorListener {
    override fun syntaxError(
        recognizer: Recognizer<*, *>?,
        offendingSymbol: Any?,
        line: Int,
        charPositionInLine: Int,
        msg: String?,
        e: RecognitionException?
    ) {
        throw RuntimeException()
    }

    override fun reportContextSensitivity(
        recognizer: Parser?,
        dfa: DFA?,
        startIndex: Int,
        stopIndex: Int,
        prediction: Int,
        configs: ATNConfigSet?
    ) {
        throw RuntimeException()
    }

    override fun reportAttemptingFullContext(
        recognizer: Parser?,
        dfa: DFA?,
        startIndex: Int,
        stopIndex: Int,
        conflictingAlts: BitSet?,
        configs: ATNConfigSet?
    ) {
        throw RuntimeException()
    }

    override fun reportAmbiguity(
        recognizer: Parser?,
        dfa: DFA?,
        startIndex: Int,
        stopIndex: Int,
        exact: Boolean,
        ambigAlts: BitSet?,
        configs: ATNConfigSet?
    ) {
        throw RuntimeException()
    }
}
