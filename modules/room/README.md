# Module 2.2 Android Databases Using Room

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
                
<!-- END COMMON HEADER -->

## Introduction

Android Jetpack provides the Room component to ease database access. In this module, we'll create a Room database to represent a Movie application.

## Objectives

* Create entities using Room
* Access database data through those entities
* Structure your application for unidirectional data flow

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:50:06

            
#### Lecture: Room (Fall 2021) (24:21)
 | [![Lecture: Room (Fall 2021)](https://img.youtube.com/vi/m7B0XDcvRjE/mqdefault.jpg)](https://youtu.be/m7B0XDcvRjE) |
 | -- |

                

#### Example: Room Movie Database (Fall 2021) (1:25:45)
 | [![Example: Room Movie Database (Fall 2021)](https://img.youtube.com/vi/i7Ix7p3AdPc/mqdefault.jpg)](https://youtu.be/i7Ix7p3AdPc) |
 | -- |

                

            