package com.javadude.room.data

import androidx.room.Relation

// PLAIN kotlin class, NOT @Entity
data class ExpandedRole(
    val actorId: String,
    @Relation(
        parentColumn = "actorId",
        entityColumn = "id"
    )
    val actor: Actor,
    val character: String,
    val orderInCredits: Int
)