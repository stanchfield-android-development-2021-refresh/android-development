package com.javadude.room

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.javadude.room.ui.theme.RoomTheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RoomTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Ui(viewModel)
                }
            }
        }
    }
}

@Composable
fun LabeledText(
    label: String,
    value: String
) {
    Column {
        Text(
            text = label,
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            text = value,
            style = MaterialTheme.typography.h6,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, top = 8.dp, end = 8.dp, bottom = 8.dp)
                .border(
                    color = MaterialTheme.colors.primary,
                    width = 2.dp
                )
                .padding(8.dp)
        )
    }
}

@Composable
fun Ui(
    viewModel: MovieViewModel
) {
    val scope = rememberCoroutineScope()
    // ...
    val movies by viewModel.movies.collectAsState(initial = emptyList())
    val actors by viewModel.actors.collectAsState(initial = emptyList())
    val cast by viewModel.cast.collectAsState(initial = emptyList())

    @Composable
    fun ColumnScope.CommonButton(
        text: String,
        onClick: suspend () -> Unit
    ) {
        Button(
            onClick = {
                scope.launch { // NOT GOOD - uses external state!
                    onClick()
                }
            },
            content = {
                Text(text = text)
            } ,
            modifier = Modifier
                .padding(8.dp)
                .align(Alignment.CenterHorizontally)
        )
    }


    Row(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .weight(1f)
                .verticalScroll(rememberScrollState())
        ) {
            LabeledText(
                label = stringResource(id = R.string.movies),
                value = movies.joinToString("\n") { it.title }
            )
            LabeledText(
                label = stringResource(id = R.string.actors),
                value = actors.joinToString("\n") { it.name }
            )
            LabeledText(
                label = stringResource(id = R.string.cast),
                value = cast.joinToString("\n") { "${it.character} - ${it.actor.name}" }
            )
        }
        Column(
            modifier = Modifier
                .wrapContentWidth()
                .verticalScroll(rememberScrollState())
        ) {
            CommonButton(text = stringResource(R.string.reset_sample_db)) {
                viewModel.resetDatabase()
            }
            CommonButton(text = stringResource(R.string.select_movie_1)) { viewModel.selectMovie("m1") }
            CommonButton(text = stringResource(R.string.select_movie_2)) { viewModel.selectMovie("m2") }
            CommonButton(text = stringResource(R.string.select_movie_3)) { viewModel.selectMovie("m3") }
            CommonButton(text = stringResource(R.string.delete_movie_1)) { viewModel.deleteMovie("m1") }
            CommonButton(text = stringResource(R.string.delete_movie_2)) { viewModel.deleteMovie("m2") }
            CommonButton(text = stringResource(R.string.delete_actor_1)) { viewModel.deleteActor("a1") }
            CommonButton(text = stringResource(R.string.delete_actor_2)) { viewModel.deleteActor("a2") }
        }
    }
}


//@Composable
//fun Greeting(name: String) {
//    Text(text = "Hello $name!")
//}
//
//@Preview(showBackground = true)
//@Composable
//fun DefaultPreview() {
//    RoomTheme {
//        Greeting("Scott")
//    }
//}