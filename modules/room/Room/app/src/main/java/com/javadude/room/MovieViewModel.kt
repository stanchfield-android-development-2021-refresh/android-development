package com.javadude.room

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.javadude.room.data.ExpandedRole
import com.javadude.room.data.Movie
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = MovieDatabaseRepository(application)

    val movies = repository.movies
    val actors = repository.actors

    private val selectedMovie = MutableSharedFlow<Movie?>()
    @ExperimentalCoroutinesApi
    val cast = selectedMovie.flatMapLatest {
        it?.let { movie ->
            repository.getCast(movie.id)
        } ?: flow {
            emit(emptyList<ExpandedRole>())
        }
    }

    suspend fun selectMovie(id: String) = selectedMovie.emit(getMovie(id))
    suspend fun getMovie(id: String) = repository.getMovie(id)
    suspend fun getActor(id: String) = repository.getActor(id)
    suspend fun deleteMovie(id: String) = repository.delete(repository.getMovie(id))
    suspend fun deleteActor(id: String) = repository.delete(repository.getActor(id))
    suspend fun resetDatabase() = repository.resetDatabase()
}