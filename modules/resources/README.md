# Module 14.4 Further Resources

            
<!-- END COMMON HEADER -->

## Introduction

You've reached the end of the course! Congratulations! In this module, I'll point you to a few other resources that may be helpful and you work with Android in the future.

## Objectives

* Learn about a few places that can be helpful resources for future development.

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 03:25

            
#### Further Resources: Lecture (Summer 2021) (03:25)
 | [![Further Resources: Lecture (Summer 2021)](https://img.youtube.com/vi/TmagDwUR594/mqdefault.jpg)](https://youtu.be/TmagDwUR594) |
 | -- |

                

            