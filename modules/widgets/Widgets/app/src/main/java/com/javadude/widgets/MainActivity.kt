package com.javadude.widgets

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.widgets.ui.theme.WidgetsTheme

const val EXTRA_ITEM_ID = "EXTRA_ITEM_ID"
const val EXTRA_ITEM_NAME = "EXTRA_ITEM_NAME"

@ExperimentalComposeUiApi
class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<SampleViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.updateSelectedName(intent.getStringExtra(EXTRA_ITEM_NAME))

        setContent {
            WidgetsTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    val selectedName by viewModel.selectedName.collectAsState(initial = "")
                    val people by viewModel.people.collectAsState(initial = emptyList())
                    var name by remember { mutableStateOf("") }

                    Scaffold(
                        topBar = {
                                 TopAppBar(
                                     title = { Text("Widgets") },
                                     actions = {
                                         IconButton(icon = Icons.Filled.Add, iconDescription = stringResource(id = R.string.add)) {
                                             viewModel.addPerson(name)
                                         }
                                     }
                                 )
                        },
                        content = {
                            Column {
                                CommonTextField(
                                    label = stringResource(id = R.string.name),
                                    value = name,
                                    onValueChange = { name = it },
                                    onBack = { finish() }
                                )
                                Divider()
                                LazyColumn {
                                    items(people) { person ->
                                        if (selectedName == person.name) {
                                            Row(verticalAlignment = Alignment.CenterVertically) {
                                                Icon(imageVector = Icons.Filled.Star, contentDescription = stringResource(id = R.string.selected_person))
                                                Display(text = person.name)
                                            }
                                        } else {
                                            Display(
                                                text = person.name,
                                                modifier = Modifier.clickable {
                                                    viewModel.updateSelectedName(person.name)
                                                }
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    )
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        viewModel.updateSelectedName(intent.getStringExtra(EXTRA_ITEM_NAME))
    }
}

