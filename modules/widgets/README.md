# Module 11.2 Widgets

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Users can personalize their home screens by adding "widgets", small user interfaces that interact with installed applications.

In this module, we'll create a simple application that manages a list of names and displays those names on a homescreen widget.

> Note that the videos for this module use the old Android Views approach for the activity's user interface. I have updated the example code to use
> Jetpack compose for this (and Flows rather than LiveData), but unfortunately, Compose cannot yet be used to create homescreen widgets.
> When creating a widget user interface, you'll still need to use xml-defined layouts as described in the video and communicate with them using the "remote views" API.

> As an interesting aside... Note how much simpler the list of names UI is to implement in Jetpack Compose than the old RecyclerView!

## Objectives

* Create a homescreen widget that displays useful information for an application
* Jump to the application when the widget is clicked
* Pass data (in this case a selected name) to the application so we can display the correct data.

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 42:32

            
#### Widgets: Lecture (Spring 2016) (10:32)
 | [![Widgets: Lecture (Spring 2016)](https://img.youtube.com/vi/DY2Ij0lhDsE/mqdefault.jpg)](https://youtu.be/DY2Ij0lhDsE) |
 | -- |

                

#### Widgets: Example (Spring 2016) (32:00)
 | [![Widgets: Example (Spring 2016)](https://img.youtube.com/vi/YtMB1HufaFY/mqdefault.jpg)](https://youtu.be/YtMB1HufaFY) |
 | -- |

                

            