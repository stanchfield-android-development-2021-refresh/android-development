package stanchfield.scott.sensors

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import stanchfield.scott.sensors.ui.theme.SensorsTheme
import kotlin.math.min

@Immutable
data class PuckInfo(
    val color: Color,
    val cx: Float,
    val cy: Float,
    val radius: Float,
    val dx: Float = 0f,
    val dy: Float = 0f,
    val ax: Float = 0f,
    val ay: Float = 0f
)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val defaultDisplay = windowManager.defaultDisplay
        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY)

        setContent {
            SensorsTheme {
                with(LocalDensity.current) {
                    Column {
                        var text by remember { mutableStateOf("") }
                        Text(text = text, modifier = Modifier.fillMaxWidth())
                        BoxWithConstraints(
                            modifier = Modifier
                                .padding(8.dp)
                                .border(width=2.dp, color=Color.Black)
                                .fillMaxSize()
                        ) {
                            val width = maxWidth.toPx()
                            val height = maxHeight.toPx()

                            val radius = min(width, height) / 10
                            var puckInfo by remember {
                                mutableStateOf(
                                    PuckInfo(
                                        cx = radius * 3,
                                        cy = radius * 4,
                                        radius = radius,
                                        color = Color.Red
                                    )
                                )
                            }
                            DisposableEffect(key1 = Unit) {
                                val listener = object : SensorEventListener {
                                    override fun onSensorChanged(event: SensorEvent) {
                                        val (xValue, yValue) = when (defaultDisplay.rotation) {
                                            android.view.Surface.ROTATION_0 ->   Pair(-event.values[0],  event.values[1])
                                            android.view.Surface.ROTATION_90 ->  Pair(event.values[1], event.values[0])
                                            android.view.Surface.ROTATION_180 -> Pair(event.values[0], -event.values[1])
                                            android.view.Surface.ROTATION_270 -> Pair(-event.values[1],  -event.values[0])
                                            else -> throw IllegalArgumentException()
                                        }
                                        text = "rotation: ${defaultDisplay.rotation}, x=$xValue, y=$yValue"
                                        puckInfo = puckInfo.copy(ax = xValue*5, ay = yValue*5)
                                    }
                                    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
                                }
                                sensorManager.registerListener(listener, gravitySensor, SensorManager.SENSOR_DELAY_GAME)
                                onDispose {
                                    sensorManager.unregisterListener(listener)
                                }
                            }

                            LaunchedEffect(Unit) {
                                while (true) {
                                    delay(50)
                                    puckInfo = puckInfo.move(width, height)
                                }
                            }
                            Surface(color = MaterialTheme.colors.background) {
                                Puck(puckInfo = puckInfo, modifier = Modifier.fillMaxSize())
                            }
                        }
                    }
                }
            }
        }
    }
}

private fun PuckInfo.move(width: Float, height: Float): PuckInfo {
    var newdx = dx + ax
    var newdy = dy + ay
    var newcx = cx + newdx
    var newcy = cy + newdy

    if (newcx !in radius..width-radius) {
        newdx = -newdx / 2
        newcx = newcx.coerceIn(radius, width - radius)
    }
    if (newcy !in radius..height-radius) {
        newdy = -newdy / 2
        newcy = newcy.coerceIn(radius, height - radius)
    }
    return copy(
        dx = newdx,
        dy = newdy,
        cx = newcx,
        cy = newcy,
    )
}

@Composable
fun Puck(
    puckInfo: PuckInfo,
    modifier: Modifier
) {
    Canvas(modifier = modifier) {
        drawCircle(
            color = puckInfo.color,
            center = Offset(puckInfo.cx, puckInfo.cy),
            radius = puckInfo.radius
        )
    }
}
