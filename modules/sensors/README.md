# Module 14.1 Sensors

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Most Android devices come with sensors for reporting movement, temperature, direction, location, etc. In this module, we'll talk about some sensor concepts and implement a simple moving puck on the screen using accelerometer input that translates tilt of the phone into movement of the puck.

## Objectives

* Understand how to obtain a sensor from the sensor manager
* Create applications that use sensors like the accelerometer to provide input

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 23:03

            
#### Sensors: Lecture and Examples (Summer 2021) (23:03)
 | [![Sensors: Lecture and Examples (Summer 2021)](https://img.youtube.com/vi/lHtOM9lsAmg/mqdefault.jpg)](https://youtu.be/lHtOM9lsAmg) |
 | -- |

                

            