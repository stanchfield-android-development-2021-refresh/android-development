# Module 1.3 Sample Code

            
<!-- END COMMON HEADER -->

## Introduction

In this module, we'll take a look at how the sample code for the course is organized and how you can set up your homework projects to take advantage of its common dependency setup.

## Objectives

* Explore the sample code repository
* Set up a sample project using the sample-code dependencies set up file

## Notes

To set up your projects using the sample-code dependencies file. When copying files, it's easiest to copy them from your local copy of the repository rather than clicking these links to download (the links are only here so you can look at the files online if you would like)

* Copy [modules/libs.versions.toml](../libs.versions.toml) to the root of your project. Do not modify this file.

* Copy [modules/sample-code/SampleProject/build.gradle](SampleProject/build.gradle) to the root of your project. Do not modify this file.

* Copy [modules/sample-code/SampleProject/modules.gradle](SampleProject/modules.gradle) to the root of your project. Do not modify this file unless you're adding new modules to the project.

* Copy [modules/sample-code/SampleProject/app/build.gradle](SampleProject/app/build.gradle) to your app directory in your project. You will need to modify the applicationId to be your application id. For example:

  ```
  android {
     ...

     defaultConfig {
        applicationId "lastName.firstName.hw2" // Change this line

    ...
  ```

* Copy [modules/sample-code/SampleProject/app/build-dependencies.gradle](SampleProject/app/build-dependencies.gradle) to your app directory in your project. You may need to adjust this based on which dependencies you need. I recommend you look at the build-dependencies.gradle in the specific example app you're using as a guide (you may want to copy that one)

* Copy [modules/sample-code/SampleProject/settings.gradle](SampleProject/settings.gradle) to the root of your project. You will need to modify the location of the libs.versions.toml file and the rootProject.name to match your project name. For example:
  ```
  ...
     versionCatalogs {
            libs {
                from(files('libs.versions.toml')) // remove the "../.."
            }
        }
  ...
  rootProject.name = "HW1"
  ```

* Copy [modules/sample-code/SampleProject/gradle/wrapper/gradle-wrapper.properties](SampleProject/gradle/wrapper/gradle-wrapper.properties) to your project's gradle/wrapper directory. Do not modify this file.

* Create a modules.gradle file in the top level of your project. It should contain
  ```
  include ':app'
  ```
  

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 15:35

            
#### Example: Sample Code (Fall 2021) (15:35)
 | [![Example: Sample Code (Fall 2021)](https://img.youtube.com/vi/eOMvW9DxoIo/mqdefault.jpg)](https://youtu.be/eOMvW9DxoIo) |
 | -- |

                

            