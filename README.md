### On this page

* [Android Development](#android-development)
   * [Version Information](#version-information)
   * [Course Outline](#course-outline)

# Android Development

Content and Sample Code for Android Development Course (Fall 2021 Refresh)

## Sample Code

Please see the [sample-code](modules/sample-code) module for details on the sample code provided in this repository, and how to use the sample dependencies files in your own projects.

> Note - Library versions in the sample source code are managed in the libs.versions.toml in the modules directory of the repository. I recommend you use this file to ensure you're using the same versions of the libraries as I demonstrate in class. I may update this file during the class based on updated dependencies, SDKs or language releases.

## Version Information

Keep in mind that versions you'll see in the videos are based on when the video was recorded (and I can't re-record every term). Be sure to use the versions listed below for your assignments.

Note that the [Sample Code Module](modules/sample-code) contains build files that you can copy that use these versions. This is the easiest way to make sure you're using the required versions, but you can update your build files directly if you prefer.

DO NOT UPDATE FROM THESE VERSIONS UNLESS INSTRUCTED TO DO SO


| Tool/Library        | Version                                                                      |
| ------------------- | ---------------------------------------------------------------------------- |
| Android Studio      | Chipmunk (2021.2.1 Patch 1) <br/>https://developer.android.com/studio#downloads<br/>(recommend downloading a zip file instead of an installer - keep the zip file) |
| Android Min SDK     | 21                                                                           |
| Android Compile SDK | 32                                                                           |
| Android Target SDK  | 32                                                                           |
| Jetpack Compose     | 1.1.1                                                                        |
| Kotlin              | 1.6.10                                                                       |

## Course Outline

<!-- START OUTLINE -->
### Week 1 &nbsp;&nbsp;&nbsp; 2022/01/26 to 2022/02/01 &nbsp;&nbsp;&nbsp; (Video Runtime: 2:31:29)
   * [Module 1.1 - Course Logistics](modules/course-logistics) (Video Runtime: 29:51)
   * [Module 1.2 - Development Environment](modules/development-environment) (Video Runtime: 34:49)
   * [Module 1.3 - Sample Code](modules/sample-code) (Video Runtime: 15:35)
   * [Module 1.4 - Activities](modules/activities) (Video Runtime: 1:11:14)
   * START [Assignment 1: Development Environment](assignments/development-environment)
      * DUE 2022-02-01 by 11:59PM Eastern
   * SUBMIT [Assignment 1: Development Environment](assignments/development-environment)
      * DUE 2022-02-01 by 11:59PM Eastern
-------
### Week 2 &nbsp;&nbsp;&nbsp; 2022/02/02 to 2022/02/08 &nbsp;&nbsp;&nbsp; (Video Runtime: 3:03:11)
   * [Module 2.1 - Kotlin Primer](modules/kotlin-primer) (Video Runtime: 1:13:05)
   * [Module 2.2 - Android Databases Using Room](modules/room) (Video Runtime: 1:50:06)
   * START [Assignment 2: Contact Manager Database](assignments/contact-database)
      * DUE 2022-02-15 by 11:59PM Eastern
-------
### Week 3 &nbsp;&nbsp;&nbsp; 2022/02/09 to 2022/02/15 &nbsp;&nbsp;&nbsp; (Video Runtime: 2:51:56)
   * [Module 3.1 - Jetpack Compose Basics](modules/compose-basics) (Video Runtime: 2:51:56)
   * SUBMIT [Assignment 2: Contact Manager Database](assignments/contact-database)
      * DUE 2022-02-15 by 11:59PM Eastern
-------
### Week 4 &nbsp;&nbsp;&nbsp; 2022/02/16 to 2022/02/22 &nbsp;&nbsp;&nbsp; (Video Runtime: 2:32:28)
   * [Module 4.1 - Jetpack Compose Lists](modules/compose-lists) (Video Runtime: 2:32:28)
   * START [Assignment 3: Contact Manager User Interface](assignments/contact-ui)
      * DUE 2022-03-08 by 11:59PM Eastern
-------
### Week 5 &nbsp;&nbsp;&nbsp; 2022/02/23 to 2022/03/01 &nbsp;&nbsp;&nbsp; (Video Runtime: 2:36:03)
   * [Module 5.1 - Jetpack Compose Layouts](modules/compose-layouts) (Video Runtime: 2:17:34)
   * [Module 5.2 - Refactoring the Movies Example](modules/movies-refactoring) (Video Runtime: 18:29)
-------
### Week 6 &nbsp;&nbsp;&nbsp; 2022/03/02 to 2022/03/08 &nbsp;&nbsp;&nbsp; (Video Runtime: 1:47:55)
   * [Module 6.1 - Jetpack Compose Graphics 1](modules/compose-graphics-1) (Video Runtime: 1:47:55)
   * SUBMIT [Assignment 3: Contact Manager User Interface](assignments/contact-ui)
      * DUE 2022-03-08 by 11:59PM Eastern
-------
### Week 7 &nbsp;&nbsp;&nbsp; 2022/03/09 to 2022/03/15 &nbsp;&nbsp;&nbsp; (Video Runtime: 1:57:28)
   * [Module 7.1 - Jetpack Compose Graphics 2](modules/compose-graphics-2) (Video Runtime: 1:57:28)
   * START [Assignment 4: Gem Matcher Game](assignments/gem-matcher)
      * DUE 2022-04-05 by 11:59PM Eastern
-------
### Week 8 &nbsp;&nbsp;&nbsp; 2022/03/16 to 2022/03/22 &nbsp;&nbsp;&nbsp; (Video Runtime: 1:59:27)
   * [Module 8.1 - Jetpack Compose Toast, Snackbar and Dialog](modules/compose-toast-snackbar-dialog) (Video Runtime: 1:59:27)
-------
### Thanksgiving Break
-------
### Week 9 &nbsp;&nbsp;&nbsp; 2022/03/30 to 2022/04/05 &nbsp;&nbsp;&nbsp; (Video Runtime: 2:09:09)
   * [Module 9.1 - Google Maps](modules/google-map) (Video Runtime: 1:17:46)
   * [Module 9.2 - Speech](modules/speech) (Video Runtime: 51:23)
   * SUBMIT [Assignment 4: Gem Matcher Game](assignments/gem-matcher)
      * DUE 2022-04-05 by 11:59PM Eastern
-------
### Week 10 &nbsp;&nbsp;&nbsp; 2022/04/06 to 2022/04/12 &nbsp;&nbsp;&nbsp; (Video Runtime: 1:34:36)
   * [Module 10.1 - RESTful Web Services](modules/rest) (Video Runtime: 25:48)
   * [Module 10.2 - Android Services](modules/services) (Video Runtime: 1:08:48)
   * START [Assignment 5: Maps, Rest and Services](assignments/maps-rest-services)
      * DUE 2022-04-19 by 11:59PM Eastern
-------
### Week 11 &nbsp;&nbsp;&nbsp; 2022/04/13 to 2022/04/19 &nbsp;&nbsp;&nbsp; (Video Runtime: 1:33:25)
   * [Module 11.1 - Near-Field Communication (NFC)](modules/nfc) (Video Runtime: 50:53)
   * [Module 11.2 - Widgets](modules/widgets) (Video Runtime: 42:32)
   * SUBMIT [Assignment 5: Maps, Rest and Services](assignments/maps-rest-services)
      * DUE 2022-04-19 by 11:59PM Eastern
-------
### Week 12 &nbsp;&nbsp;&nbsp; 2022/04/20 to 2022/04/26 &nbsp;&nbsp;&nbsp; (Video Runtime: 2:19:06)
   * [Module 12.1 - Testing](modules/testing) (Video Runtime: 2:19:06)
   * START [Assignment 6: Testing](assignments/testing)
      * DUE 2022-05-03 by 11:59PM Eastern
-------
### Week 13 &nbsp;&nbsp;&nbsp; 2022/04/27 to 2022/05/03 &nbsp;&nbsp;&nbsp; (Video Runtime: 1:59:57)
   * [Module 13.1 - Files](modules/files) (Video Runtime: 1:59:57)
   * SUBMIT [Assignment 6: Testing](assignments/testing)
      * DUE 2022-05-03 by 11:59PM Eastern
-------
### Week 14 &nbsp;&nbsp;&nbsp; 2022/05/04 to 2022/05/10 &nbsp;&nbsp;&nbsp; (Video Runtime: 1:03:34)
   * [Module 14.1 - Sensors](modules/sensors) (Video Runtime: 23:03)
   * [Module 14.2 - Android Versions](modules/versions) (Video Runtime: 09:06)
   * [Module 14.3 - Publishing Apps](modules/publishing) (Video Runtime: 28:00)
   * [Module 14.4 - Further Resources](modules/resources) (Video Runtime: 03:25)
-------
Total Video Time: 29:59:44
<!-- END OUTLINE -->