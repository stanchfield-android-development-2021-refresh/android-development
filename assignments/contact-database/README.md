# Assignment 2 - Contact Manager Database

## Setup

NOTE: Be sure to name this project HW2 and all packages starting with lastname.firstname.hw2.

You must implement this assignment in Kotlin.

> Note: Be sure the version numbers in your build files match those in the example.

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

Do not attempt to copy the example project and just tweak it. Android Studio can give you unending pain if you try to rename it, and the application you are creating will be significantly simpler than the movie example.

Start a new project and copy over the pieces you need. I would recommend that you copy the the two build.gradles (top-level and the one in app) from the Room example into your project. 
You will need to fix the application id.

## Introduction
In this assignment, you'll create the database for a simple contacts application.

> Note that the relationship between a Contact and an Address is one-to-many. The Movie application example demonstrates one-to-many and many-to-many relationships; you will NOT need an extra table to track the relationsip. Look at how the Ratings and Movies are related.

## Database 

Create a ContactEntity class using Room annotations containing

* id (String)
* First and Last Name (separate Strings)
* Phone numbers (Strings): Home, Work, Mobile
* Email Address (String)
        
Create an AddressEntity class using Room annotations containing

* id (String)
* type (String) (for "home" address, "work" address, etc)
* street (String)
* city (String)
* state (String)
* zip (String)
* contactId (String) to track the owning contact for this address (one contact to many addresses)
        
Create a ContactDAO and a ContactDatabase using Room.

The DAO(s) must include functions to

* insert contacts and addresses

* update contacts and addresses

* delete contacts and addresses

* queries that return Flows for 
    * all contacts
    * all addresses

* queries that directly return data for
    * a contact by its id
    * an address by its id
    * a contact and all of its addresses (similar to RatingsWithMovies)

Create a ContactRepository interface and a ContactDatabaseRepository that expose all functions from the Dao. Note that
the flows must be exposed as vals in the repository, as demonstrated in the Movie example.

Create a ContactViewModel that provides

* A private property containing an instance of the repository

* public immutable Flow properties for a list of all contacts and a list of all addresses

* A public MutableState property (with a private setter) for a contact with a list of addresses

* A function to select a contact, which will set the mutable state

* Functions to insert, update and delete contacts and addresses

When creating the database, create three dummy contacts, each with two addresses 

## User Interface

Create a simple test user interface similar to the Movie Database example I demonstrated in class

You should be able to

* See a list of all contacts

   * list only the contact name in the list

   * when you click a contact in the list, display a screen with the contact and a list of its addresses

      * show all details of the contact

      * just show the type and street of the address

      * when you click an address, display all of its details it on a screen of its own

* See a list of all addresses

   * just show the type and street of the address

   * when you click an address, display all of its details it on a screen of its own

<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folders and from the `app` folder
1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw2`
1. Go to the Canvas course website
1. Click on the Assignment link on the left navigation bar
1. Click on the Assignment 2 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!