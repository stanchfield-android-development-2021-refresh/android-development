# Assignment 1 - Development Environment

## Setup

NOTE: Be sure to name this project HW1 and all packages starting with lastname.firstname.hw1.

You must implement this assignment in Kotlin.

> Note: Be sure the version numbers in your build files match those in the main [README](../../README.md) and [Sample Code Module](../../modules/sample-code).

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

## Introduction
In this assignment, we'll set up your development environment.

## Setup

NOTE: Be sure to name this project HW1 and its packages starting with lastname.firstname.hw1.

This assignment is a 'checkmark' assignment; it does not directly count into your grade score, but if you do not submit a working 'hello world' your overall grade will be reduced by a '-' (A becomes A-, A- becomes B+, etc). The intent here is to ensure that everyone's development environment works before HW2 is assigned. Because the assignment is so simple, it shouldn't carry as much positive weight as the other assignments. Because it is so important to do, it should carry a high penalty if not done.

## Requirements

You must implement this assignment using Kotlin.

Follow the instructions in the class video to create a 'hello, world' application, export it, and send it to me.

Note - you do not need to change any code for this assignment; just create and run a project to make sure your development environment is working, then submit the project.

Be sure to use the versions of Android Studio and the Android SDK/tools stated in class!

Note: DO NOT update your versions of Android Studio or the Android SDK/tools during the semester! I want to be sure that everyone is running the same versions of the tools so the grader will see your submissions the same way that you submitted them.

<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folders and from the `app` folder
1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw1`
1. Go to the Blackboard course website
1. Click on the Assignment Submission link on the left navigation bar
1. Click on the Assignment 1 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!