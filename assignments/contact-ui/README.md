# Assignment 3 - Contact Manager User Interface

## Setup

NOTE: Be sure to name this project HW3 and all packages starting with lastname.firstname.hw3.

You must implement this assignment in Kotlin.

> Note: Be sure the version numbers in your build files match those in the main [README](../../README.md) and [Sample Code Module](../../modules/sample-code).

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

Do not attempt to copy the example project and just tweak it. Android Studio can give you unending pain if you try to rename it, and the application you are creating will be significantly simpler than the movie example.

Start a new project and copy over the pieces you need. I would recommend that you copy the build.gradle files from the compose movies with database example into your project. 
You will need to fix the application id. 

## Introduction
In this assignment, you'll create a UI for a simple contacts application.

## Data

Copy the data from your Contact Manager Database project into the new HW3 project. Note that you will need to rename the package statements in the copied files.
        
## User Interface

Note - Any screen that is not a LazyColumn must be scrollable as demonstrated in class. (Use `Modifier.verticalScroll(rememberScrollState())`)

Your application should:

* Use a stack to keep track of the user's current screen

* Have a single activity that manages all screens

* Use Jetpack Compose to define the user interfaces

* Contain the following screens. Note that you do not need to try to come up with common Composable functions for things like lists or scaffolds; you can implement the lists/scaffolds directly wherever you need them.

    * Contact List

        * If there are no contacts, display a Text stating "No contacts found"

        * Have a toolbar button that resets the database (clearing it and adding the contacts, like in the movies example). This button should use the "auto renew" icon.

           * You can add dependency 

             ```
             implementation "androidx.compose.material:material-icons-extended:$compose_version"
             ```

             to your app's build.gradle, which gives access to many more icons, and use Icons.Filled.Autorenew

             (See https://fonts.google.com/icons for a list of icons, though I've seen some that appear to be missing)
    
        * Displays a LazyColumn of all contacts in the database, sorted by last name then first name
        
        * Each contact should be displayed on a Card
        
        * The contact card should include
        
            * A "person" icon
            
            * The contact's lastname, firstname
            
            * The contact's mobile phone number
            
            * The layout of the information on the card is up to you
            
        * Support multiple-selection as demonstrated in class

           * When contacts are multi-selected, the toolbar should contain

              * Navigation "back" arrow that will clear multi-select
              * A count of the number of selected items
              * A delete action that will delete the addresses
        
        * Clicking on the person icon will toggle the item in multi-select mode
        
        * Clicking anywhere else on an item card will go to the contact display

        * (You do not need to implement long-press; just clicks)
        
        * The toolbar should display "Contacts" as the title and have an add button that creates a new contact
        
        * Pressing the create contact button will go to the Contact Edit screen with a new item. It will not create any addresses   
    
    * Contact Display
    
        * Should use a scrollable Column for the overall layout

        * Displays all data for a contact
        
        * Displays all information for all addresses for that contact
        
        * The layout of the information is up to you
        
        * Has an app bar with an edit button that takes you to the Contact Edit screen. The title on the app bar should be the name of the contact or "&lt;no name&gt;" if the name is blank
        
        * Must be scrollable
    
    * Contact Edit
    
        * Should use a scrollable Column for the overall layout

        * Has an app bar where the title on the app bar should be the name of the contact or "&lt;no name&gt;" if the name is blank. Use `String.isEmpty()` to check for this.
        
        * Displays entry fields for all data for a contact
        
        * All data is saved as the user types it
        
        * Displays an "addresses" section at the bottom of the form, something like
        
        
                Addresses             +
            
                Home - 123 Sesame ... X
                Work - 11100 Johns... X
        
           * DO NOT use a Lazy Column for this list; just add a Card for each address directly to the Column 
           
           * Note - to make each address expand to push the "X" button all the way to the right, you can do something like

             ```kotlin
             Row(modifier = Modifier.fillMaxWidth()) {
                 Text(..., modifier = Modifier.weight(1f))
                 IconButton(..., modifier.size(48.dp))
             }
             ```
             Using a weight for only the Text will make it expand while the icon stays fixed.
        
        * The + button for the addresses should be green
        
        * The "X" buttons (use Icons.Filled.Delete - a trash can) to delete each address should be red
                                
        * Each address should be displayed on a card in the overall layout. Do not use a LazyColumn here (nested scrolling is pretty nasty for the user to navigate)
        
        * Address cards can be formatted however you want (you do not need an icon); however, they must appear raised with a rounded-corner outline
        
        * Address cards must have a delete icon (red trash can)
        
        * Tapping an address card opens the address edit screen for that address
        
        * Tapping the trash can on an address card deletes it without warning (we have not covered dialogs...)
        
        * Tapping the + icon next to "Addresses" creates and inserts a new address and opens the address edit screen
                        
    
    * Address Edit
    
        * Displays entry fields for all data for an address
        
        * All data is saved as the user types it
        
    
    * About
    
        * Displays a simple "about this application" type message. Can be whatever text you would like
        
        * Accessible from *any* screen except About via a button on the toolbar
        
        * Find an icon like an exclamation mark inside a circle to use for the about button
        

This is the basic flow of your application. 

```mermaid
graph
    About(About)

    ContactList(Contact List)
    ContactDisplay(Contact Display)
    ContactEdit(Contact Edit)
    AddressEdit(Address Edit)

    ContactList -- On create contact --> ContactEdit
    ContactList -- On tap contact --> ContactDisplay
    ContactDisplay -- On edit contact --> ContactEdit
    ContactEdit -- On tap or create address --> AddressEdit

    ContactList -- On about --> About
    ContactDisplay -- On about --> About
    ContactEdit -- On about --> About
    AddressEdit -- On about --> About
```

Notes:

* There should only be one activity in your submission

* You can use helper classes/superclasses as needed

* You can organize classes and top-level functions into whichever files you prefer

* You can copy as much code as needed from the examples, but be sure to add a comment at the top of the file (for large amounts copied) or around the chunks copied attributing it.

* Remember - all strings that the user sees must be in the strings.xml file or data in the database
	
<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folders and from the `app` folder
1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw3`
1. Go to the Blackboard course website
1. Click on the Assignment Submission link on the left navigation bar
1. Click on the Assignment 3 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!