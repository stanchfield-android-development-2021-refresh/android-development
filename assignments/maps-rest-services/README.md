# Assignment 5 - Maps and REST

## Setup

NOTE: Be sure to name this project HW5 and all packages starting with lastname.firstname.hw5.

You must implement this assignment in Kotlin.

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

## Introduction

In this assignment, you'll create an application to track an alien invasion. A RESTful web service is provided that tells you the current location of several alien ships. Your application will use this service to ascertain the ship locations and display them on your device using Google Maps.

Your application will fetch changes via the RESTful web service, polling for changes once per second. _Note this this a **very** bad way to implement this in real life. Ideally you would use something like Firebase Cloud Messaging to push updates to devices rather than poll. However, I thought it was more important for you to try communicating between an Activity and an Android service than set up Firebase Cloud Messaging._
   
**_The aliens are trying to tell us something! Figure out a way to see what they're saying!_**

| ![Sample Screen](image001.jpg) | 
| -- |
| *Sample Screen* |


## Data and Helper Classes

You will need to create the following  

   * A `UfoPosition` class that contains the following information
      * `int ship`
      * `double lat`
      * `double lon`

   * An `AlientAlert` class
      * Contains a `List<UFOPosition>`
   
You can annotate these to make them available for automatic conversion from JSON using Retrofit2.  

   * An `AlienAlerter` class
      * Exposes a `Flow<AlienAlert>` named `alerts`
      * Has a `startReporting()` function that starts a coroutine to perform the REST requests
         (**THERE SHOULD BE EXACTLY ONE COROUTINE HERE. DO NOT START A SEPARATE COROUTINE FOR EACH REPORTER OR FOR EACH REST CALL.**)
      * When `startReporting()` is called, once per second, make a request to `http://javadude.com/aliens/n.json` where 
        `n` is a number starting at 1, and should be incremented for each request. (Note that I'm _simulating_ REST requests via static JSON files on my static web site.) To see an example of the JSON, point your browser to http://javadude.com/aliens/1.json. I recommend you look at a few different 'n's here to see how the ships are tracked over time.
      * Use Retrofit2 to run your REST requests, similar to the REST example in class. 
      * If a request returns a 404 (NOT_FOUND) status code, stop polling. _Do not assume the number of reports that are available._
      * Parse the returned JSON into objects using Retrofit2's automatic JSON support. It will be a `List<UFOPosition>`.
      * Emit an `AlienAlert` to the `alerts` Flow.
      
      * Note: You will see Android complain about plain-text communication; I didn't want to cloud this assignment with HTTPS setup (which is the same as in any Java application). To get around this, add
      
         `android:usesCleartextTraffic="true"`

         to the application  tag in your AndroidManifest.xml

   * An `AliensViewModel` 
      * Holds an instance of the `AlienAlerter`
      * Has a `startAlienReporting()` function that tells the `AlienAlerter` to start `startReporting`
      * Converts the `AlienAlerter` Flow state from `AlienAlerter` to state for the UI
         * It's up to you to decide what that state looks like
         * You should use a `.map {...}` call on the `AlienAlerter.alerts` Flow to do the conversion. Expose this to the UI.
         * This state should contain info for
            * The UFOs that should currently be displayed
            * The lines to display on the map
            * Note that line lists should show all reported positions seen for each UFO. Each UFO has a separate line list
         * The state can be a single object containing all the data or separate objects for the list of UFOs and list of lines or some other structure.
         * The state should not be polyline or marker data, but can contain `LatLng`s
         * The UI should **not** keep track of the UFO state; it should only collect the state from the view model and display the results.
         * The UI should do very little processing of the data (aside from determining the `LatLngBounds`)

## The Activity

Notes:
   * You should be using Jetpack Compose as the basis for your User Interface, including a `GoogleMap`.

   * First create an Activity that only displays a map centered on lat=38.9073, lon=-77.0365. Test this to be sure you have properly set up the google maps API and properly registered an Android key. Do not write any other code until you have this working!

   * Get the UFO icon from [ic_ufo_flying.xml](ic_ufo_flying.xml). (Right-click the link to download; Note that I've tweaked the colors and size) 

     Attribution: Icon made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com)

   * To load the UFO as a BitmapDescriptor, copy the DrawableHelpers.kt file from the maps example project to your project. Then call
      ```kotlin
      context.loadBitmapDescriptor(R.drawable.ic_ufo_flying)
      ```
       as I demonstrated in class.

   * Wait for the map to be ready, then ask the view model to start reporting

   * Collect the state from the view model
   
   * For each collected state
      * Display active UFOs as markers on the map
      * Display lines as `Polyline`s on the map
   
   * If you're seeing a "U" where you think an "O" should appear in the alien's message, you're doing something wrong...

   * Lock your screen orientation on the activity screenOrientation to the &lt;activity&gt; tag in the manifest:
   
     ```xml   
     <activity
        android:name=".MainActivity"
        ...
        android:screenOrientation="portrait"
        tools:ignore="LockedOrientationActivity">
     ```
   
   * You _do not_ need to worry about saving/restoring data for configuration changes or if the user presses back or home while running the application. These would be necessary in a real application. In other words, it's ok if everything restarts if back/home is pressed and the application is re-entered.		  
   
   * Track the bounding box that will be large enough to contain all UFO positions that you have seen. Use `LatLngBounds` like in the example.

   * Update the camera when the `LatLngBounds` changes
      

<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folder and from the `app` folder
1. Zip the project directory. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw5`
1. Go to the Blackboard course website
1. Click on the Assignment Submission link on the left navigation bar
1. Click on the Assignment 5 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!