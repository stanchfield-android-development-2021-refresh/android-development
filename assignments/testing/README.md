# Assignment 6 - Testing

## Setup



You must implement this assignment in Kotlin.

> Note: Be sure the version numbers in your build files match those in the main [README](../../README.md) and [Sample Code Module](../../modules/sample-code).

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

## Introduction

In this assignment, you'll write three types of tests for a (buggy) calculator application

NOTE: DO NOT CHANGE THE NAME OF THE PROJECT, PACKAGES, ETC.

NOTE: MAKE ONLY MINIMAL CHANGES TO THE PROVIDED CODE, SUCH AS ADDING TEST TAGS TO COMPOSABLES TO ASSIST LOOKUP

You must implement this assignment using Kotlin.

You will not need a Service Locator or Dependency Injection for this assignment. (For a more complex application, Dependency Injection may make testing easier, or help isolate application layers)

Copy the provided calculator example from the `modules/testing` directory in the sample repository. You'll need to copy the `modules/libs.versions.toml` file to your copy of the project and adjust its location inside `ComnposeCalculator/settings.gradle`

Run the application to get a feel for what it does. Note that there are some intentional bugs...

Write the following types of tests:

* Test the CalculatorLogic class (a plain old Kotlin object) - test locally using JUnit
   
   * Test all functions, checking what happens. Check the displayed value after each called function. Perform operations like pressing 
     ```
     1 9 8 4 + 1 =
     ```
      
   * Be sure to cover **all** operations in some combination of tests so you can spot the errors (there are two intentional errors in the logic)
      
   * Do not fix the code; you'll deliver tests with the project that demonstrate the errors
      
* Test the CalculatorViewModel class - test locally using JUnit
  
   * Use Turbine to collect the displayed value and check it after each operation
     
   * Perform operations on the view model. Be sure to call all operations at least once		      
      
* Create a UI test - instrumented, using composeTestRule
  
   * Walk through three sequences of operations - perform more than one + - * / in each sequence
      
   * Be sure to demonstrate the error (check all of the buttons when running to see where the error appears - there is one intentional error in the UI)
      
* DO NOT write the tests _only_ for the intentional errors. Write tests that cover the other buttons/operations as well
  
* When writing your test code, remember the code is written in kotlin and take advantage of it
  
 * For example, if you want to do the same kind of test on a bunch of buttons, try something like

   ```kotlin     
   "C1234567890+-*/=.".forEach {
       pressAndCheckResult(it.toString())
   }
   ```

<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folders and from the `app` folder
1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw6`
1. Go to the Blackboard course website
1. Click on the Assignment Submission link on the left navigation bar
1. Click on the Assignment 6 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!