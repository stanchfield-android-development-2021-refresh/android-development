import kotlin.time.ExperimentalTime
import kotlinx.datetime.Month

sealed class ModuleId(val id: String)
object CourseLogistics: ModuleId("course-logistics")
object DevelopmentEnvironment: ModuleId("development-environment")
object SampleCode: ModuleId("sample-code")
object Activities: ModuleId("activities")
object KotlinPrimer: ModuleId("kotlin-primer")
object Room: ModuleId("room")
object ComposeBasics: ModuleId("compose-basics")
object ComposeLists: ModuleId("compose-lists")
object ComposeLayouts: ModuleId("compose-layouts")
object MoviesRefactoring: ModuleId("movies-refactoring")
object Workers: ModuleId("compose-notifications")
object ComposeGraphics1: ModuleId("compose-graphics-1")
object ComposeGraphics2: ModuleId("compose-graphics-2")
object ComposeToastAndSnackbarDialog: ModuleId("compose-toast-snackbar-dialog")
object Speech: ModuleId("speech")
object Nfc: ModuleId("nfc")
object Widgets: ModuleId("widgets")
object GoogleMap: ModuleId("google-map")
object Services: ModuleId("services")
object Rest: ModuleId("rest")
object Files: ModuleId("files")
object Testing: ModuleId("testing")
object Sensors: ModuleId("sensors")
object Versions: ModuleId("versions")
object Publishing: ModuleId("publishing")
object Resources: ModuleId("resources")

@ExperimentalTime
fun main() {
    semester(Term.Spring, 2022, Month.JANUARY, 26) {
        1.week {
            module(CourseLogistics, "Course Logistics") {
                video("yLGIrjAk1Qg", "Lecture: Course Logistics (Fall 2021)", 29.minutes + 51.seconds)
            }
            module(DevelopmentEnvironment, "Development Environment") {
                video("Cuk1hkPEWdQ", "Lecture: Development Environment (Fall 2021)", 3.minutes + 35.seconds)
                video("sxTBIIJAIug", "Example: Development Environment (Fall 2021)", 31.minutes + 14.seconds)
            }
            module(SampleCode, "Sample Code") {
                video("eOMvW9DxoIo", "Example: Sample Code (Fall 2021)", 15.minutes + 35.seconds)
            }
            module(Activities, "Activities") {
                video("sMr7xA5GQYE", "Lecture: Activities (Fall 2021)", 24.minutes + 30.seconds)
                video("fFewMJHoYF4", "Example: Activities (Fall 2021)", 46.minutes + 44.seconds)
            }
            assignment {
                id = "development-environment"
                title = "Development Environment"
                weeks = 1
                dependsOn(DevelopmentEnvironment)
            }
        }
        2.week {
            module(KotlinPrimer, "Kotlin Primer") {
                video("wIhwlYdO7cQ", "Example: Kotlin Primer (Fall 2021)", 1.hours + 13.minutes + 5.seconds)
            }
            module(Room, "Android Databases Using Room") {
                dependsOn(Activities)
                video("m7B0XDcvRjE", "Lecture: Room (Fall 2021)", 24.minutes + 21.seconds)
                video("i7Ix7p3AdPc", "Example: Room Movie Database (Fall 2021)", 1.hours + 25.minutes + 45.seconds)
            }
            assignment {
                id = "contact-database"
                title = "Contact Manager Database"
                weeks = 2
                dependsOn(Activities)
                dependsOn(Room)
            }
        }
        3.week {
            module(ComposeBasics, "Jetpack Compose Basics") {
                dependsOn(Activities)
                dependsOn(Room)
                video("DX9h4EdYtEs", "Lecture: Compose Basics (Fall 2021)", 26.minutes + 16.seconds)
                video("0l_MsqyGtA8", "Example: Compose Basics (Fall 2021)", 52.minutes + 41.seconds)
                video("qp0kprMzosE", "Example: Movies App, Part 1 (Fall 2021)", 1.hours + 32.minutes + 59.seconds)
            }
        }
        4.week {
            module(ComposeLists, "Jetpack Compose Lists") {
                dependsOn(Activities)
                dependsOn(Room)
                dependsOn(ComposeBasics)
                video("ZnmxNQjMw0g", "Example: Compose - Lists (Fall 2021)", 1.hours + 31.minutes + 4.seconds)
                video("bviUBfjxcyo", "Example: Compose - Side-by-Side UIs (Fall 2021)", 1.hours + 1.minutes + 24.seconds)
            }
            assignment {
                id = "contact-ui"
                title = "Contact Manager User Interface"
                weeks = 3
                dependsOn(Activities)
                dependsOn(Room)
                dependsOn(ComposeBasics)
                dependsOn(ComposeLists)
            }
        }
        5.week {
            module(ComposeLayouts, "Jetpack Compose Layouts") {
                dependsOn(Activities)
                dependsOn(Room)
                dependsOn(ComposeBasics)
                dependsOn(ComposeLists)
                video("V-rrPANU264", "Example: Basic and Custom Layouts (Fall 2021)", 1.hours + 11.minutes + 20.seconds)
                video("3XsCVqcdzVA", "Lecture: Slot APIs (Fall 2021)", 16.minutes + 47.seconds)
                video("uQIUpHP50Tc", "Example: Slot APIs (Fall 2021)", 10.minutes + 27.seconds)
                video("EWBCy0jWFvc", "(Bonus, Not Required) More detail on Template Method, Strategy and Null Object Patterns (Fall 2021)", 39.minutes + 0.seconds)
            }
            module(MoviesRefactoring, "Refactoring the Movies Example") {
                dependsOn(Activities)
                dependsOn(Room)
                dependsOn(ComposeBasics)
                dependsOn(ComposeLists)
                dependsOn(ComposeLayouts)
                video("wFhPd-n4i24", "Example: Refactoring the Movies Application (Fall 2021)", 18.minutes + 29.seconds)
            }
        }
        6.week {
            module(ComposeGraphics1, "Jetpack Compose Graphics 1") {
                dependsOn(Activities)
                dependsOn(Room)
                dependsOn(ComposeBasics)
                video("1t41a-X8Ewc", "Jetpack Compose Graphics: Introduction (Fall 2021)", 6.minutes + 50.seconds)
                video("BpVQus2r4qs", "Jetpack Compose Graphics: A Simple Gauge (Fall 2021)", 24.minutes + 32.seconds)
                video("5FtqH8dv0BM", "Jetpack Compose Graphics: An Analog Clock (Fall 2021)", 20.minutes + 0.seconds)
                video("kNLr8Oe8vnA", "Jetpack Compose Graphics: Graph Editor, Part 1 (Fall 2021)", 56.minutes + 33.seconds)
            }
        }
        7.week {
            module(ComposeGraphics2, "Jetpack Compose Graphics 2") {
                dependsOn(Activities)
                dependsOn(Room)
                dependsOn(ComposeBasics)
                dependsOn(ComposeGraphics1)
                video("4UZt1KOd8ow", "Jetpack Compose Graphics: Blinking and Internal Compiler Errors (Fall 2021)", 52.minutes + 55.seconds)
                video("SqscYM8G8KY", "Jetpack Compose Graphics: Dragging and Connecting (Fall 2021)", 1.hours + 4.minutes + 33.seconds)
            }
            assignment {
                id = "gem-matcher"
                title = "Gem Matcher Game"
                weeks = 4
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                dependsOn(ComposeGraphics1)
                dependsOn(ComposeGraphics2)
            }
        }
        8.week {
            module(ComposeToastAndSnackbarDialog, "Jetpack Compose Toast, Snackbar and Dialog") {
                dependsOn(Activities)
                dependsOn(Room)
                dependsOn(ComposeBasics)
                dependsOn(MoviesRefactoring)
                video("n5Mq-PwoXEM", "Jetpack Compose: Toast, Snackbar and Dialog Lecture (Fall 2021)", 16.minutes + 25.seconds)
                video("xQeYd6fcqOk", "Jetpack Compose: Toast, Snackbar and Dialog Example (Fall 2021)", 1.hours + 43.minutes + 2.seconds)
            }
        }

        breakWeek("Thanksgiving Break")

        9.week {
            module(GoogleMap, "Google Maps") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("VvNdc9eNcbc", "Google Maps: Overview (Fall 2021)", 9.minutes + 9.seconds)
                video("G_NDiQSHquo", "Google Maps: Basic Example (Fall 2021)", 35.minutes + 31.seconds)
                video("4Pmu7yVKYqY", "Google Maps: Car Finder (Fall 2021)", 33.minutes + 6.seconds)
            }
            module(Speech, "Speech") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("uTMbNvtmwZY", "Speech: Overview (Fall 2021)", 15.minutes + 35.seconds)
                video("YWtooRFWmZ8", "Speech: Example (Fall 2021)", 35.minutes + 48.seconds)
            }
        }
        10.week {
            module(Rest, "RESTful Web Services") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                dependsOn(ComposeToastAndSnackbarDialog)
                video("Fd0L5LbEwPU", "REST: Lecture (Fall 2021)", 7.minutes + 4.seconds)
                video("Ya7sCOzl0ss", "REST: Example (Fall 2021)", 18.minutes + 44.seconds)
            }
            module(Services, "Android Services") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("0mn-AJ-fcmw", "Services: Lecture (Fall 2021)", 28.minutes + 14.seconds)
                video("4br0VOPIzu8", "Services: Example (Fall 2021)", 35.minutes + 42.seconds)
                video("w_Bxk6cBAdE", "Services: Remote Example (Fall 2021)", 4.minutes + 52.seconds)
            }
            assignment {
                id = "maps-rest-services"
                title = "Maps, Rest and Services"
                weeks = 2
                projectNameText = { n ->
                    "NOTE: Be sure to name these projects HW${n}_Service and HW${n}_Client and all packages starting with lastname.firstname.hw$n."
                }
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                dependsOn(GoogleMap)
                dependsOn(Rest)
                dependsOn(Services)
            }
        }
        11.week {
            module(Nfc, "Near-Field Communication (NFC)") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("wXEjjljdNWI", "Near-Field Communication: Lecture (Spring 2016)", 19.minutes + 11.seconds)
                video("QzphwRdJ7r0", "Near-Field Communication: Reading Example (Spring 2016)", 16.minutes + 16.seconds)
                video("mUVdV9EmIQs", "Near-Field Communication: Writing Example (Spring 2016)", 15.minutes + 26.seconds)
            }
            module(Widgets, "Widgets") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("DY2Ij0lhDsE", "Widgets: Lecture (Spring 2016)", 10.minutes + 32.seconds)
                video("YtMB1HufaFY", "Widgets: Example (Spring 2016)", 32.minutes + 0.seconds)
            }
        }
        12.week {
            module(Testing, "Testing") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("6MdN6GSjcSc", "Testing: Lecture (Summer 2021)", 20.minutes + 31.seconds)
                video("k4aAgzQ4IyE", "Testing: Example, Part 1 (Summer 2021)", 1.hours + 2.minutes + 4.seconds)
                video("Pd5W-YQoDrA", "Testing: Example, Part 2 (Summer 2021)", 56.minutes + 31.seconds)
            }
            assignment {
                id = "testing"
                title = "Testing"
                weeks = 2
                projectNameText = { _ -> "" } // skip the "name this project XYZ" text
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                dependsOn(Testing)
            }
        }

        13.week {
            module(Files, "Files") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("8mR8lqTETtE", "Files: Lecture (Summer 2021)", 27.minutes + 29.seconds)
                video("4g3ScXm31tg", "Files: Example 1 - Private Files (Summer 2021)", 35.minutes + 4.seconds)
                video("EAJPl4xdAxY", "Files: Example 2 - SD Card Private Files and MediaStore (Summer 2021)", 35.minutes + 52.seconds)
                video("Fr_XW94jPIQ", "Files: Example 3 - Storage Access Framework (Summer 2021)", 21.minutes + 32.seconds)
            }
        }
        14.week {
            module(Sensors, "Sensors") {
                dependsOn(Activities)
                dependsOn(ComposeBasics)
                video("lHtOM9lsAmg", "Sensors: Lecture and Examples (Summer 2021)", 23.minutes + 3.seconds)
            }
            module(Versions, "Android Versions") {
                video("Y-f6X5iFxok", "Android Versions: Lecture (Summer 2021)", 9.minutes + 6.seconds)
            }
            module(Publishing, "Publishing Apps") {
                video("ouP17DyQsOM", "Publishing: Lecture (Summer 2021)", 28.minutes + 0.seconds)
            }
            module(Resources, "Further Resources") {
                video("TmagDwUR594", "Further Resources: Lecture (Summer 2021)", 3.minutes + 25.seconds)
            }
        }
    }
}
